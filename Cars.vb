﻿Imports System.Data.SqlClient

Public Class Cars
    Private Sub Cars_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try


            Me.Visible = False

            Load_Driver()
            CarDriverComboBox.DataSource = DriverDT
            CarDriverComboBox.DisplayMember = "DriverName"
            CarDriverComboBox.ValueMember = "DriverName"

            Load_cars()
            DataGridView1.DataSource = CarDT
            DataGridView1.Columns(0).HeaderText = "رقم اللوحه "
            DataGridView1.Columns(1).HeaderText = "اسم السائق"
            DataGridView1.Columns(2).HeaderText = "الحموله"
        Catch ex As Exception

        End Try


    End Sub





    Private Sub DataGridView1_SelectionChanged(sender As Object, e As EventArgs) Handles DataGridView1.SelectionChanged
        Try
            Dim pos As Integer = BindingContext(CarDT).Position

            PlatNBTextBox.Text = CarDT.Rows(pos).Item("PlatNB")
            CarDriverComboBox.Text = CarDT.Rows(pos).Item("CarDriver")
            CarCapacityTextBox.Text = CarDT.Rows(pos).Item("CarCapacity")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub SaveButton_Click(sender As Object, e As EventArgs) Handles SaveButton.Click

        Try




            If PlatNBTextBox.Text <> Nothing Then

                Dim CHKCarDA As New SqlDataAdapter
                Dim CHKCarDT As New DataTable
                CHKCarDA = New SqlDataAdapter("select * from Cars Where (PlatNB =N'" & PlatNBTextBox.Text & "') ", SQlManeCon)
                CHKCarDT.Clear()
                CHKCarDA.Fill(CHKCarDT)
                If CHKCarDT.Rows.Count = 0 Then
                    CarDT.Rows.Add()
                    Dim last As Integer = CarDT.Rows.Count - 1
                    CarDT.Rows(last).Item("PlatNB") = PlatNBTextBox.Text
                    CarDT.Rows(last).Item("CarDriver") = CarDriverComboBox.Text
                    CarDT.Rows(last).Item("CarCapacity") = CarCapacityTextBox.Text
                    Dim save As New SqlCommandBuilder(CarDA)
                    CarDA.Update(CarDT)
                    CarDT.AcceptChanges()
                    Load_cars()
                    Log_UserAct(Me.Text, "اضافه", "", PlatNBTextBox.Text)

                ElseIf CHKCarDT.Rows.Count > 0 Then
                    Dim pos As Integer = BindingContext(CarDT).Position
                    CarDT.Rows(pos).Item("PlatNB") = PlatNBTextBox.Text
                    CarDT.Rows(pos).Item("CarDriver") = CarDriverComboBox.Text
                    CarDT.Rows(pos).Item("CarCapacity") = CarCapacityTextBox.Text
                    Dim save1 As New SqlCommandBuilder(CarDA)
                    CarDA.Update(CarDT)
                    CarDT.AcceptChanges()
                    Load_cars()
                    Log_UserAct(Me.Text, "تعديل", "", PlatNBTextBox.Text)

                End If
            End If
        Catch ex As Exception
            log_error(Me.Text, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message)
            ' MsgBox(ex.Message & "log is saved in " & Application.StartupPath & "Log.txt")
        End Try
    End Sub

    Private Sub DeleteButton_Click(sender As Object, e As EventArgs) Handles DeleteButton.Click
        Try
            Dim pos As Integer = BindingContext(CarDT).Position
            CarDT.Rows(pos).Delete()
            Dim save As New SqlCommandBuilder(CarDA)
            CarDA.Update(CarDT)
            CarDT.AcceptChanges()
            Load_cars()
            Log_UserAct(Me.Text, "حذف", "", PlatNBTextBox.Text)

        Catch ex As Exception
            log_error(Me.Text, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message)
            ' MsgBox(ex.Message & "  log is saved in   " & Application.StartupPath & "/Log.txt  ")

        End Try
    End Sub

    Private Sub NewButton_Click(sender As Object, e As EventArgs) Handles NewButton.Click
        Try
            PlatNBTextBox.Text = ""
            CarDriverComboBox.Text = ""

            CarCapacityTextBox.Text = 0
        Catch ex As Exception

        End Try




    End Sub


End Class