﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ProductionLine
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim LineNameLabel As System.Windows.Forms.Label
        Dim LineCapacityLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ProductionLine))
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.LineNameTextBox = New System.Windows.Forms.TextBox()
        Me.LineCapacityTextBox = New System.Windows.Forms.TextBox()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton2 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton3 = New System.Windows.Forms.ToolStripButton()
        LineNameLabel = New System.Windows.Forms.Label()
        LineCapacityLabel = New System.Windows.Forms.Label()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ToolStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'LineNameLabel
        '
        LineNameLabel.AutoSize = True
        LineNameLabel.Location = New System.Drawing.Point(277, 42)
        LineNameLabel.Name = "LineNameLabel"
        LineNameLabel.Size = New System.Drawing.Size(61, 13)
        LineNameLabel.TabIndex = 2
        LineNameLabel.Text = "Line Name:"
        '
        'LineCapacityLabel
        '
        LineCapacityLabel.AutoSize = True
        LineCapacityLabel.Location = New System.Drawing.Point(93, 45)
        LineCapacityLabel.Name = "LineCapacityLabel"
        LineCapacityLabel.Size = New System.Drawing.Size(74, 13)
        LineCapacityLabel.TabIndex = 4
        LineCapacityLabel.Text = "Line Capacity:"
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(12, 68)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.Size = New System.Drawing.Size(328, 152)
        Me.DataGridView1.TabIndex = 0
        '
        'LineNameTextBox
        '
        Me.LineNameTextBox.Location = New System.Drawing.Point(171, 42)
        Me.LineNameTextBox.Name = "LineNameTextBox"
        Me.LineNameTextBox.Size = New System.Drawing.Size(100, 20)
        Me.LineNameTextBox.TabIndex = 3
        '
        'LineCapacityTextBox
        '
        Me.LineCapacityTextBox.Location = New System.Drawing.Point(12, 42)
        Me.LineCapacityTextBox.Name = "LineCapacityTextBox"
        Me.LineCapacityTextBox.Size = New System.Drawing.Size(75, 20)
        Me.LineCapacityTextBox.TabIndex = 5
        Me.LineCapacityTextBox.Text = "0"
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1, Me.ToolStripButton2, Me.ToolStripButton3})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(350, 25)
        Me.ToolStrip1.TabIndex = 6
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton1.Image = Global.OrderManger.My.Resources.Resources.floppy_disk_save_button_icon_65887
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton1.Text = "ToolStripButton1"
        '
        'ToolStripButton2
        '
        Me.ToolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton2.Image = Global.OrderManger.My.Resources.Resources._627249_delete3_512
        Me.ToolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton2.Name = "ToolStripButton2"
        Me.ToolStripButton2.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton2.Text = "ToolStripButton2"
        '
        'ToolStripButton3
        '
        Me.ToolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton3.Image = Global.OrderManger.My.Resources.Resources.orange_plus_sign_icon_32197
        Me.ToolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton3.Name = "ToolStripButton3"
        Me.ToolStripButton3.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton3.Text = "ToolStripButton3"
        '
        'ProductionLine
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(350, 232)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(LineNameLabel)
        Me.Controls.Add(Me.LineNameTextBox)
        Me.Controls.Add(LineCapacityLabel)
        Me.Controls.Add(Me.LineCapacityTextBox)
        Me.Controls.Add(Me.DataGridView1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "ProductionLine"
        Me.Opacity = 0R
        Me.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Text = "خطوط الانتاج"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents LineNameTextBox As TextBox
    Friend WithEvents LineCapacityTextBox As TextBox
    Friend WithEvents ToolStrip1 As ToolStrip
    Friend WithEvents ToolStripButton1 As ToolStripButton
    Friend WithEvents ToolStripButton2 As ToolStripButton
    Friend WithEvents ToolStripButton3 As ToolStripButton
End Class
