﻿Imports System.Data.SqlClient
Imports System.Threading




Public Class HomeScreen


    Private Sub OpenFormInTab(Frm As Form)
        Dim TabForm As New Form
        TabForm = Frm
        TabForm.MdiParent = Me
        TabForm.Opacity = 100

        TabForm.Show()

        TabForm.BringToFront()


    End Sub
    Public op As Boolean
    Private Sub CatsTile_ItemClick(sender As Object, e As DevExpress.XtraEditors.TileItemEventArgs)
        Cars.Show()
        Cars.BringToFront()
        Cars.Opacity = 100
    End Sub

    Private Sub driversTile_ItemClick(sender As Object, e As DevExpress.XtraEditors.TileItemEventArgs)
        Drivers.Show()
        Drivers.Opacity = 100
        Drivers.BringToFront()
    End Sub

    Private Sub TileItem1_ItemClick(sender As Object, e As DevExpress.XtraEditors.TileItemEventArgs)
        Products.Show()
        Products.Opacity = 100
        Products.BringToFront()
    End Sub

    Private Sub TileItem2_ItemClick(sender As Object, e As DevExpress.XtraEditors.TileItemEventArgs)
        ProductionLine.Show()
        ProductionLine.Opacity = 100
        ProductionLine.BringToFront()
    End Sub

    Private Sub TileItem3_ItemClick(sender As Object, e As DevExpress.XtraEditors.TileItemEventArgs)
        LoadDoneOrders()


        NewOrder.Show()
        NewOrder.BringToFront()

        NewOrder.Opacity = 100



    End Sub

    Private Sub TileItem4_ItemClick(sender As Object, e As DevExpress.XtraEditors.TileItemEventArgs)
        LoadDoneOrders()
        Orders.Show()
        Orders.Opacity = 100
        Orders.BringToFront()
    End Sub

    Private Sub TileItem6_ItemClick(sender As Object, e As DevExpress.XtraEditors.TileItemEventArgs)
        LoadDoneOrders()
        CarTable.Show()
        CarTable.Opacity = 100
        CarTable.BringToFront()
    End Sub

    Private Sub TileItem5_ItemClick(sender As Object, e As DevExpress.XtraEditors.TileItemEventArgs)
        LoadDoneOrders()
        ProductionTable.Show()
        ProductionTable.Opacity = 100
        ProductionTable.BringToFront()
    End Sub


    ' ===========================================


    Public Sub LoadDoneOrders()
        Try
            Dim InvoiceCodeDA As New SqlDataAdapter
            Dim InvoiceCodeDT As New DataTable
            Dim NotDeliverdDA As New SqlDataAdapter("Select CusCode, OrderID , Id , IsDeliverd ,InvoiceCode  from Orders where IsDeliverd = 'False' ", SQlManeCon)
            Dim NotDeliverdDT As New DataTable
            NotDeliverdDT.Clear()
            NotDeliverdDA.Fill(NotDeliverdDT)
            For i As Integer = NotDeliverdDT.Rows.Count - 1 To 0 Step -1
                InvoiceCodeDA = New SqlDataAdapter("SELECT  PurchaseOrderNo , InvoiceCode  FROM SL_Invoice where CustomerId =N'" & NotDeliverdDT.Rows(i).Item("CusCode") & "' and PurchaseOrderNo =N'" & NotDeliverdDT.Rows(i).Item("OrderID") & "'  ", SQLCon)
                InvoiceCodeDT.Clear()
                InvoiceCodeDA.Fill(InvoiceCodeDT)
                If InvoiceCodeDT.Rows.Count > 0 Then
                    NotDeliverdDT.Rows(i).Item("IsDeliverd") = CheckState.Checked
                    NotDeliverdDT.Rows(i).Item("InvoiceCode") = InvoiceCodeDT.Rows(0).Item("InvoiceCode")
                    Dim save As New SqlCommandBuilder(NotDeliverdDA)
                    NotDeliverdDA.Update(NotDeliverdDT)
                    NotDeliverdDT.AcceptChanges()
                End If
            Next

            MoveToDoneOrders()

        Catch ex As Exception

        End Try
    End Sub




    Private Sub TileItem8_ItemClick(sender As Object, e As DevExpress.XtraEditors.TileItemEventArgs)

        Form1.ShowDialog()


        DoneOrders.Show()

        DoneOrders.BringToFront()
        DoneOrders.Opacity = 100

    End Sub

    Private Sub HomeScreen_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'SplashScreen2.Show()




        ' ----------------- for users athorty 


        TextBox1.Text = CurrentUserName


        Dim CurrentUserDA As New SqlDataAdapter("Select * From Users Where UserName =N'" & CurrentUserName & "' ", SQlManeCon)

        Dim CurrentUserDT As New DataTable
        CurrentUserDT.Clear()
        CurrentUserDA.Fill(CurrentUserDT)

        SettingMW = CurrentUserDT.Rows(0).Item("SettingMW")
        If SettingMW = False Then
            SettingPage.Visible = False

        End If
        GroupW = CurrentUserDT.Rows(0).Item("GroupW")
        If GroupW = False Then
            OrdersGroupButton.Enabled = False

        End If
        DriverW = CurrentUserDT.Rows(0).Item("DriverW")
        If DriverW = False Then
            DriversButton.Enabled = False
        End If
        CarsW = CurrentUserDT.Rows(0).Item("CarsW")
        If CarsW = False Then
            CarsButton.Enabled = False
        End If
        ProductW = CurrentUserDT.Rows(0).Item("ProductW")
        If ProductW = False Then
            ProductsButton.Enabled = False
        End If
        ProductionLineW = CurrentUserDT.Rows(0).Item("ProductionLineW")
        If ProductionLineW = False Then
            ProLineTableButton.Enabled = False
        End If
        OrdersMW = CurrentUserDT.Rows(0).Item("OrdersMW")
        If OrdersMW = False Then
            OrdersPage.Visible = False
        End If
        CarTableW = CurrentUserDT.Rows(0).Item("CarTableW")
        If CarTableW = False Then
            CarsTableButton.Enabled = False
        End If
        NewOrderW = CurrentUserDT.Rows(0).Item("NewOrderW")
        If NewOrderW = False Then
            NewOrdersButton.Enabled = False
        End If
        OrdersW = CurrentUserDT.Rows(0).Item("OrdersW")
        If OrdersW = False Then
            OrdersButton.Enabled = False
        End If
        OrdersWEdit = CurrentUserDT.Rows(0).Item("OrdersWEdit")
        OrdersWDelete = CurrentUserDT.Rows(0).Item("OrdersWDelete")
        OrdersWPrint = CurrentUserDT.Rows(0).Item("OrdersWPrint")
        DoneOrdersW = CurrentUserDT.Rows(0).Item("DoneOrdersW")
        If DoneOrdersW = False Then
            DoneOrdersButton.Enabled = False
        End If
        'ProductionMW = CurrentUserDT.Rows(0).Item("ProductionMW")
        'If ProductionMW = False Then
        '    TileGroup3.Visible = False
        'End If
        ProductionTableW = CurrentUserDT.Rows(0).Item("ProductionTableW")
        If ProductionTableW = False Then
            ProLineTableButton.Enabled = False
        End If
        UserMW = CurrentUserDT.Rows(0).Item("UserMW")
        If UserMW = False Then
            UserHistoryButton.Enabled = False
            UsersMangemntButton.Enabled = False
        End If
        UserW = CurrentUserDT.Rows(0).Item("UserW")
        If UserW = False Then
            UsersMangemntButton.Enabled = False
        End If
        UserHSTW = CurrentUserDT.Rows(0).Item("UserHSTW")
        If UserHSTW = False Then
            UserHistoryButton.Enabled = False
        End If

        SignIn.Close()


        If My.Settings.ShowWN = True Then
            WhatsNew.Show()

        End If

        'Dim BackUpThread As New Thread(AddressOf BackUpTimer.Start)
        'BackUpThread.Start()
        BackUpTimer.Start()
    End Sub

    Private Sub TileItem7_ItemClick(sender As Object, e As DevExpress.XtraEditors.TileItemEventArgs)
        Groups.Show()
        Groups.BringToFront()

    End Sub

    Private Sub HomeScreen_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing


        Application.Exit()



    End Sub

    Private Sub TileItem9_ItemClick(sender As Object, e As DevExpress.XtraEditors.TileItemEventArgs)
        Users.ShowDialog()

    End Sub

    Private Sub TileItem10_ItemClick(sender As Object, e As DevExpress.XtraEditors.TileItemEventArgs)
        UsersHistory.Show()
        UsersHistory.BringToFront()

    End Sub

    Private Sub TileItem11_ItemClick(sender As Object, e As DevExpress.XtraEditors.TileItemEventArgs)
        PrintSetting.Show()
        PrintSetting.BringToFront()

    End Sub

    Private Sub PrintSettingButton_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles PrintSettingButton.ItemClick
        OpenFormInTab(PrintSetting)

    End Sub

    Private Sub UsersMangemntButton_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles UsersMangemntButton.ItemClick
        OpenFormInTab(Users)


    End Sub

    Private Sub OrdersButton_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles OrdersButton.ItemClick
        LoadDoneOrders()
        OpenFormInTab(Orders)
    End Sub

    Private Sub NewOrdersButton_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles NewOrdersButton.ItemClick
        LoadDoneOrders()
        OpenFormInTab(NewOrder)

    End Sub

    Private Sub CarsTableButton_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles CarsTableButton.ItemClick
        LoadDoneOrders()
        OpenFormInTab(CarTable)

    End Sub

    Private Sub ProLineTableButton_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles ProLineTableButton.ItemClick

        LoadDoneOrders()
        OpenFormInTab(ProductionTable)

    End Sub

    Private Sub DoneOrdersButton_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles DoneOrdersButton.ItemClick
        LoadDoneOrders()
        OpenFormInTab(DoneOrdersReport)

    End Sub

    Private Sub ClintTakeButton_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles ClintTakeButton.ItemClick
        OpenFormInTab(ClintTake)

    End Sub

    Private Sub UserHistoryButton_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles UserHistoryButton.ItemClick
        OpenFormInTab(UsersHistory)

    End Sub

    Private Sub CarsButton_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles CarsButton.ItemClick
        OpenFormInTab(Cars)
    End Sub

    Private Sub DriversButton_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles DriversButton.ItemClick
        OpenFormInTab(Drivers)

    End Sub

    Private Sub ProductionLineButton_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles ProductionLineButton.ItemClick
        OpenFormInTab(ProductionLine)

    End Sub

    Private Sub ProductsButton_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles ProductsButton.ItemClick
        OpenFormInTab(Products)

    End Sub

    Private Sub OrdersGroupButton_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles OrdersGroupButton.ItemClick
        OpenFormInTab(Groups)

    End Sub

    Private Sub BarButtonItem2_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem2.ItemClick
        NewInvoice.Show()

    End Sub


    Private Sub MoveToDoneOrders()




        Dim MyOrdersDA As New SqlDataAdapter("Select * from Orders where  IsDeliverd ='True'  and InvoiceCode is not Null ", SQlManeCon)
        Dim MyOrdersDT As New DataTable
        MyOrdersDT.Clear()
        MyOrdersDA.Fill(MyOrdersDT)


        Dim DoneOrdersDA As New SqlDataAdapter("SELECT * FROM DoneOrders where OrderID = 0  ", SQlManeCon)
        Dim DoneOrdersDT As New DataTable
        DoneOrdersDT.Clear()
        DoneOrdersDA.Fill(DoneOrdersDT)

        Dim DODetailsDA As New SqlDataAdapter("SELECT * FROM DoneOrdersDetails where OrderID = 0  ", SQlManeCon)
        Dim DODetailsDT As New DataTable
        DODetailsDT.Clear()
        DODetailsDA.Fill(DODetailsDT)
        ' To Get Order Numbers In a Grid 

        Dim GetSIDA As New SqlDataAdapter
        Dim GetSIDT As New DataTable
        Dim myrow() As Data.DataRow

        For i As Integer = 1 To MyOrdersDT.Rows.Count

            myrow = DoneOrdersDT.Select(" OrderID = '" & MyOrdersDT.Rows(i - 1).Item("OrderID") & "' ")

            If myrow.Count = 0 Then
                GetSIDA = New SqlDataAdapter("SELECT [SL_InvoiceId],[InvoiceCode],[InvoiceDate],[Net],[PurchaseOrderNo] FROM [ERP].[dbo].[SL_Invoice] where InvoiceCode = '" & MyOrdersDT.Rows(i - 1).Item("InvoiceCode") & "' and PurchaseOrderNo =  '" & MyOrdersDT.Rows(i - 1).Item("OrderID") & "'  ", SQLCon)
                GetSIDT.Clear()
                GetSIDA.Fill(GetSIDT)
                If GetSIDT.Rows.Count > 0 Then
                    DoneOrdersDT.Rows.Add()
                    Dim last As Integer = DoneOrdersDT.Rows.Count - 1
                    DoneOrdersDT.Rows(last).Item("OrderID") = MyOrdersDT.Rows(i - 1).Item("OrderID")
                    DoneOrdersDT.Rows(last).Item("OrderClint") = MyOrdersDT.Rows(i - 1).Item("OrderClint")
                    DoneOrdersDT.Rows(last).Item("DeliveryCar") = MyOrdersDT.Rows(i - 1).Item("DeliveryCar")
                    DoneOrdersDT.Rows(last).Item("DeliveryDriver") = MyOrdersDT.Rows(i - 1).Item("DeliveryDriver")
                    DoneOrdersDT.Rows(last).Item("OrderDate") = MyOrdersDT.Rows(i - 1).Item("OrderDate")
                    DoneOrdersDT.Rows(last).Item("DeliveryDate") = MyOrdersDT.Rows(i - 1).Item("DeliveryDate")
                    DoneOrdersDT.Rows(last).Item("TrueDeliveryDate") = GetSIDT.Rows(0).Item("InvoiceDate")
                    DoneOrdersDT.Rows(last).Item("InvoiceCode") = MyOrdersDT.Rows(i - 1).Item("InvoiceCode")
                    DoneOrdersDT.Rows(last).Item("OrderGroup") = MyOrdersDT.Rows(i - 1).Item("OrderGroup")
                    DoneOrdersDT.Rows(last).Item("OrderPrice") = GetSIDT.Rows(0).Item("Net")
                End If

            End If

        Next

        For i As Integer = 1 To DoneOrdersDT.Rows.Count

            GetSIDA = New SqlDataAdapter("SELECT  SL_Invoice.InvoiceCode ,SL_InvoiceDetail.ItemId,SL_InvoiceDetail.Qty ,SL_InvoiceDetail.TotalSellPrice 
            FROM ( SL_Invoice inner join SL_InvoiceDetail on SL_Invoice.SL_InvoiceId = SL_InvoiceDetail.SL_InvoiceId)
            where InvoiceCode = '" & DoneOrdersDT.Rows(i - 1).Item("InvoiceCode") & "' and PurchaseOrderNo =  '" & DoneOrdersDT.Rows(i - 1).Item("OrderID") & "'   ", SQLCon)
            GetSIDT.Clear()
            GetSIDA.Fill(GetSIDT)


            For x As Integer = 1 To GetSIDT.Rows.Count


                Dim ProName As String
                Dim TrueProInTon As Double
                Dim OrderedQu As Integer = 0
                '
                Dim da As New SqlDataAdapter("SELECT ProductName,ProductID , ProductUnitsInTon  FROM  Product where ProductID = '" & GetSIDT.Rows(x - 1).Item("ItemId") & "'  ", SQlManeCon)
                Dim dt As New DataTable
                dt.Clear()
                da.Fill(dt)
                If dt.Rows.Count > 0 Then

                    ProName = dt.Rows(0).Item("ProductName")
                    TrueProInTon = (GetSIDT.Rows(x - 1).Item("Qty") / dt.Rows(0).Item("ProductUnitsInTon"))
                    '
                    da = New SqlDataAdapter("SELECT Id , OrderProductID,OrderQuntity ,OrderID  FROM Orders where OrderProductID = '" & GetSIDT.Rows(x - 1).Item("ItemId") & "' and OrderID = '" & DoneOrdersDT.Rows(i - 1).Item("OrderID") & "'  ", SQlManeCon)
                    dt.Clear()
                    da.Fill(dt)
                    If dt.Rows.Count > 0 Then
                        OrderedQu = dt.Rows(0).Item("OrderQuntity")
                        dt.Rows(0).Delete()
                        Dim savedt As New SqlCommandBuilder(da)
                        da.Update(dt)
                        dt.AcceptChanges()
                    End If
                    DODetailsDT.Rows.Add()
                    Dim lastpos As Integer = DODetailsDT.Rows.Count - 1
                    DODetailsDT.Rows(lastpos).Item("OrderID") = DoneOrdersDT.Rows(i - 1).Item("OrderID")
                    DODetailsDT.Rows(lastpos).Item("OrderProduct") = ProName
                    DODetailsDT.Rows(lastpos).Item("OrderProduct_ID") = GetSIDT.Rows(x - 1).Item("ItemId")
                    DODetailsDT.Rows(lastpos).Item("OrderQuntity") = OrderedQu
                    DODetailsDT.Rows(lastpos).Item("TrueOrderQuntity") = GetSIDT.Rows(x - 1).Item("Qty")
                    DODetailsDT.Rows(lastpos).Item("TrueOrderQuntityInTon") = TrueProInTon
                    DODetailsDT.Rows(lastpos).Item("TotalPrice") = GetSIDT.Rows(x - 1).Item("TotalSellPrice")
                End If
            Next

            Dim Leftda As New SqlDataAdapter("SELECT Id, OrderProductID,OrderQuntity ,OrderID ,OrderProduct ,OrderQuntity , OrderInton  FROM Orders where  OrderID = '" & DoneOrdersDT.Rows(i - 1).Item("OrderID") & "'  ", SQlManeCon)
            Dim Leftdt As New DataTable
            Leftdt.Clear()
            Leftda.Fill(Leftdt)
            If Leftdt.Rows.Count > 0 Then

                For y As Integer = 1 To Leftdt.Rows.Count

                    DODetailsDT.Rows.Add()
                    Dim lastpos As Integer = DODetailsDT.Rows.Count - 1
                    DODetailsDT.Rows(lastpos).Item("OrderID") = Leftdt.Rows(y - 1).Item("OrderID")
                    DODetailsDT.Rows(lastpos).Item("OrderProduct") = Leftdt.Rows(y - 1).Item("OrderProduct")
                    DODetailsDT.Rows(lastpos).Item("OrderProduct_ID") = Leftdt.Rows(y - 1).Item("OrderProductID")
                    DODetailsDT.Rows(lastpos).Item("OrderQuntity") = Leftdt.Rows(y - 1).Item("OrderQuntity")
                    DODetailsDT.Rows(lastpos).Item("TrueOrderQuntity") = 0
                    DODetailsDT.Rows(lastpos).Item("TrueOrderQuntityInTon") = Leftdt.Rows(y - 1).Item("OrderInton")
                    DODetailsDT.Rows(lastpos).Item("TotalPrice") = 0
                Next
                For y As Integer = 1 To Leftdt.Rows.Count
                    Leftdt.Rows(y - 1).Delete()
                Next




                Dim savedt As New SqlCommandBuilder(Leftda)
                Leftda.Update(Leftdt)
                Leftdt.AcceptChanges()

            End If

        Next

        Dim SaveDoneOrdersDT As New SqlCommandBuilder(DoneOrdersDA)
        DoneOrdersDA.Update(DoneOrdersDT)
        DoneOrdersDT.AcceptChanges()

        Dim SaveDODetailsDT As New SqlCommandBuilder(DODetailsDA)
        DODetailsDA.Update(DODetailsDT)
        DODetailsDT.AcceptChanges()

    End Sub
    Public Sub DoBackUp()
        Dim BackUpName As String = "OrderManger" & Now.ToString("yyyy_MM_dd") & ".bak"
        Dim cmd As New SqlCommand
        cmd.Connection = SQlManeCon
        cmd.CommandText = " BACKUP DATABASE OrderMangerDB TO DISK =  '" & My.Settings.BackUpPath & "\" & BackUpName & "'"
        SQlManeCon.Open()
        cmd.ExecuteNonQuery()
        SQlManeCon.Close()
        My.Settings.LastBackUp = Now
        My.Settings.Save()
    End Sub

    Private Sub BackUpTimer_Tick(sender As Object, e As EventArgs) Handles BackUpTimer.Tick
        If My.Settings.DoBackUp = True Then
            If My.Settings.LastBackUp <> Nothing Then
                If DateDiff(DateInterval.Day, My.Settings.LastBackUp, Now) >= My.Settings.BackUpDuration Then
                    Invoke(Sub() DoBackUp())
                End If
            Else
                Invoke(Sub() DoBackUp())
            End If

        Else
            BackUpTimer.Stop()
        End If
    End Sub

    Private Sub BarButtonItem3_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem3.ItemClick
        BackUpSetting.ShowDialog()
    End Sub
End Class