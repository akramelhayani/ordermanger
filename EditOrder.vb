﻿Imports System.Data.SqlClient

Public Class EditOrder
    Private Sub EditOrder_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Visible = False
        GroupBox1.Enabled = False
        Load_Product()



        OrderProductComboBox.DataSource = ProductDT
        OrderProductComboBox.DisplayMember = "ProductName"
        OrderProductComboBox.ValueMember = "Productionline"

        Timer1.Start()




    End Sub


    Dim EditOrderDA As New SqlDataAdapter
    Dim EditOrderDt As New DataTable
    Dim orderInTon As Double




    Private Sub Load_EditOrder()
        EditOrderDA = New SqlDataAdapter("Select * From Orders Where OrderId = '" & OrderIDTextBox.Text & "' ", SQlManeCon)
        EditOrderDt.Clear()
        EditOrderDA.Fill(EditOrderDt)

    End Sub



    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick

        Load_EditOrder()



        DataGridView1.DataSource = EditOrderDt
        DataGridView1.Columns(0).Visible = False
        DataGridView1.Columns(1).Visible = False
        DataGridView1.Columns(3).Visible = False
        DataGridView1.Columns(5).Visible = False
        DataGridView1.Columns(6).Visible = False
        DataGridView1.Columns(7).Visible = False
        DataGridView1.Columns(8).Visible = False
        DataGridView1.Columns(9).Visible = False
        DataGridView1.Columns(10).Visible = False
        DataGridView1.Columns(11).Visible = False
        DataGridView1.Columns(12).Visible = False
        DataGridView1.Columns(13).Visible = False
        DataGridView1.Columns(14).Visible = False
        DataGridView1.Columns(2).HeaderText = "الصنف"
        DataGridView1.Columns(4).HeaderText = "الكميه"



        Label4.Text = EditOrderDt.Compute("SUM(OrderInton)", String.Empty)


        Timer1.Stop()

    End Sub



    Private Sub DataGridView1_SelectionChanged(sender As Object, e As EventArgs) Handles DataGridView1.SelectionChanged


        Try
            OrderProductComboBox.Enabled = False
            Dim pos As Integer = BindingContext(EditOrderDt).Position

            OrderProductComboBox.Text = EditOrderDt.Rows(pos).Item("OrderProduct")
            OrderQuntityTextBox.Text = EditOrderDt.Rows(pos).Item("OrderQuntity")

            Button1.Enabled = False

        Catch ex As Exception

        End Try



    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click

        Try
            ProductLineTextBox.Text = OrderProductComboBox.SelectedValue
        Catch ex As Exception
        End Try
        Dim da As New SqlDataAdapter
        Dim dt As New DataTable
        da = New SqlDataAdapter("Select * from Product Where ProductName =N'" & OrderProductComboBox.Text & "' ", SQlManeCon)
        dt.Clear()
        da.Fill(dt)
        Try
            orderInTon = (OrderQuntityTextBox.Text / dt.Rows(0).Item("ProductUnitsInTon"))
            TextBox1.Text = orderInTon
        Catch ex As Exception
        End Try
        Dim pos As Integer = BindingContext(EditOrderDt).Position


        EditOrderDt.Rows(pos).Item("OrderQuntity") = OrderQuntityTextBox.Text
        EditOrderDt.Rows(pos).Item("OrderInton") = TextBox1.Text
        Dim save As New SqlCommandBuilder(EditOrderDA)
        EditOrderDA.Update(EditOrderDt)
        EditOrderDt.AcceptChanges()
        Load_EditOrder()

        Label4.Text = EditOrderDt.Compute("SUM(OrderInton)", String.Empty)
        Log_UserAct(Me.Text, "تعديل صنف في طلبيه", OrderIDTextBox.Text, OrderProductComboBox.Text)


    End Sub

    Private Sub NewButton_Click(sender As Object, e As EventArgs) Handles NewButton.Click
        OrderProductComboBox.Enabled = True
        OrderQuntityTextBox.Text = String.Empty
        Button1.Enabled = True

        Log_UserAct(Me.Text, "آضافه صنف في طلبيه", OrderIDTextBox.Text, OrderProductComboBox.Text)


    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

        Try
            ProductLineTextBox.Text = OrderProductComboBox.SelectedValue
        Catch ex As Exception
        End Try
        Dim da As New SqlDataAdapter
        Dim dt As New DataTable
        da = New SqlDataAdapter("Select * from Product Where ProductName =N'" & OrderProductComboBox.Text & "' ", SQlManeCon)
        dt.Clear()
        da.Fill(dt)
        Try
            orderInTon = (OrderQuntityTextBox.Text / dt.Rows(0).Item("ProductUnitsInTon"))
            TextBox1.Text = orderInTon
        Catch ex As Exception
        End Try
        EditOrderDt.Rows.Add()
        Dim last As Integer = EditOrderDt.Rows.Count - 1
        EditOrderDt.Rows(last).Item("OrderID") = OrderIDTextBox.Text
        EditOrderDt.Rows(last).Item("OrderClint") = OrderClintComboBox.Text
        EditOrderDt.Rows(last).Item("OrderProduct") = OrderProductComboBox.Text
        EditOrderDt.Rows(last).Item("OrderProductID") = dt.Rows(0).Item("ProductID")
        EditOrderDt.Rows(last).Item("Orderquntity") = OrderQuntityTextBox.Text
        EditOrderDt.Rows(last).Item("ProductLine") = ProductLineTextBox.Text
        EditOrderDt.Rows(last).Item("DeliveryCar") = DelivaryCarComboBox.Text
        EditOrderDt.Rows(last).Item("DeliveryDriver") = DelivaryDriverComboBox.Text
        EditOrderDt.Rows(last).Item("OrderDate") = OrderDateDateTimePicker.Value
        EditOrderDt.Rows(last).Item("DeliveryDate") = DeliveryDateDateTimePicker.Value
        EditOrderDt.Rows(last).Item("ProductionDate") = ProductionDateDateTimePicker.Value
        EditOrderDt.Rows(last).Item("IsDeliverd") = CheckState.Unchecked
        EditOrderDt.Rows(last).Item("OrderInton") = TextBox1.Text
        EditOrderDt.Rows(last).Item("CusCode") = TextBox2.Text
        Dim save As New SqlCommandBuilder(EditOrderDA)
        EditOrderDA.Update(EditOrderDt)
        EditOrderDt.AcceptChanges()
        Load_EditOrder()

        Label4.Text = EditOrderDt.Compute("SUM(OrderInton)", String.Empty)

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click

        Dim pos As Integer = BindingContext(EditOrderDt).Position
        EditOrderDt.Rows(pos).Delete()
        Dim save As New SqlCommandBuilder(EditOrderDA)
        EditOrderDA.Update(EditOrderDt)
        EditOrderDt.AcceptChanges()
        Load_EditOrder()
        Log_UserAct(Me.Text, "حذف صنف من طلبيه", OrderIDTextBox.Text, OrderProductComboBox.Text)

        Try
            Label4.Text = EditOrderDt.Compute("SUM(OrderInton)", String.Empty)

        Catch ex As Exception

        End Try

    End Sub


    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        GroupBox1.Enabled = True
        Load_Driver()
        Load_cars()
        DelivaryCarComboBox.DataSource = CarDT
        DelivaryCarComboBox.DisplayMember = "PlatNB"
        DelivaryCarComboBox.ValueMember = "CarDriver"
        DelivaryDriverComboBox.DataSource = DriverDT
        DelivaryDriverComboBox.DisplayMember = "DriverName"
        DelivaryDriverComboBox.ValueMember = "DriverName"
        SimpleButton1.Visible = False

    End Sub

    Private Sub SimpleButton2_Click(sender As Object, e As EventArgs) Handles SimpleButton2.Click

        GroupBox2.Enabled = True
        SimpleButton2.Visible = False


    End Sub


    Private Sub DelivaryCarComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DelivaryCarComboBox.SelectedIndexChanged
        Try
            Try
                DelivaryDriverComboBox.Text = DelivaryCarComboBox.SelectedValue
            Catch ex As Exception

            End Try

            Dim da As New SqlDataAdapter
            Dim dt As New DataTable

            da = New SqlDataAdapter("Select * from orders where  deliverydate = '" & DeliveryDateDateTimePicker.Value.Year & "-" & DeliveryDateDateTimePicker.Value.Month & "-" & DeliveryDateDateTimePicker.Value.Day & "' and deliverycar =N'" & DelivaryCarComboBox.Text & "'  ", SQlManeCon)
            dt.Clear()
            da.Fill(dt)
            If dt.Rows.Count > 0 Then
                Try
                    Label2.Text = dt.Compute("SUM(OrderInTon)", String.Empty)
                Catch ex As Exception
                    Label2.Text = 0
                End Try
            Else
                Label2.Text = 0
            End If


            Dim MaxCCDA As New SqlDataAdapter
            Dim MaxCCDT As New DataTable

            Try
                MaxCCDA = New SqlDataAdapter("Select * from Cars where PlatNB =N'" & DelivaryCarComboBox.Text & "' ", SQlManeCon)
                MaxCCDT.Clear()
                MaxCCDA.Fill(MaxCCDT)
                Label9.Text = MaxCCDT.Rows(0).Item("CarCapacity")

            Catch ex As Exception

            End Try

        Catch ex As Exception
            log_error(Me.Text, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message)
            '   MsgBox("لقد حدث خطأً ما " & ex.Message & "  تم حفظ الخطأ في ملف سجل الاخطاء الموجود في   " & Application.StartupPath & "/Log.txt  ")

        End Try

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click


        For i As Integer = DataGridView1.Rows.Count - 1 To 0 Step -1
            'Load_Orders()
            'EditOrderDt.Rows.Add()
            'Dim last As Integer = EditOrderDt.Rows.Count - 1


            EditOrderDt.Rows(i).Item("OrderQuntity") = DataGridView1.Rows(i).DataBoundItem("OrderQuntity")

            EditOrderDt.Rows(i).Item("DeliveryCar") = DelivaryCarComboBox.Text
            EditOrderDt.Rows(i).Item("DeliveryDriver") = DelivaryDriverComboBox.Text

            EditOrderDt.Rows(i).Item("DeliveryDate") = DeliveryDateDateTimePicker.Value.Date
            EditOrderDt.Rows(i).Item("ProductionDate") = ProductionDateDateTimePicker.Value.Date
            EditOrderDt.Rows(i).Item("IsDeliverd") = CheckState.Unchecked
            EditOrderDt.Rows(i).Item("OrderInton") = DataGridView1.Rows(i).DataBoundItem("OrderInton")
            EditOrderDt.Rows(i).Item("OrderGroup") = GroupCompoBox.Text


            Dim save As New SqlCommandBuilder(EditOrderDA)
            EditOrderDA.Update(EditOrderDt)
            EditOrderDt.AcceptChanges()
            Load_EditOrder()
        Next
        Log_UserAct(Me.Text, "تعديل", OrderIDTextBox.Text, OrderClintComboBox.Text)


    End Sub


    Private Sub Label2_TextChanged(sender As Object, e As EventArgs) Handles Label2.TextChanged, Label4.TextChanged, Label9.TextChanged, Label6.TextChanged


        Dim load1 As Double
        Dim load2 As Double
        Dim lb6 As Double
        Dim lb9 As Double

        Try
            load1 = Convert.ToDouble(Label2.Text)
            load2 = Convert.ToDouble(Label4.Text)
            Label6.Text = load1 + load2

            lb6 = Convert.ToDouble(Label6.Text)
            lb9 = Convert.ToDouble(Label9.Text)





            ArcScaleComponent1.MaxValue = lb9
            ArcScaleComponent1.Value = lb6


            'ArcScaleRangeBarComponent1.Value = (lb6 / lb9) * 100

            If ArcScaleRangeBarComponent1.Value > 100 Then
                StateImageIndicatorComponent1.StateIndex = 1
                GaugeControl1.ColorScheme.Color = Color.Red

            Else
                StateImageIndicatorComponent1.StateIndex = 0
                GaugeControl1.ColorScheme.Color = Color.Green

            End If


        Catch ex As Exception

        End Try




    End Sub

    Private Sub SimpleButton3_Click(sender As Object, e As EventArgs) Handles SimpleButton3.Click
        LOAD_Groups()
        GroupCompoBox.DataSource = GroupDT
        GroupCompoBox.DisplayMember = "GroupName"
    End Sub

    Private Sub SimpleButton4_Click(sender As Object, e As EventArgs) Handles SimpleButton4.Click

        SimpleButton4.Visible = False
        Load_clients()
        OrderClintComboBox.DataSource = SQLDT
        OrderClintComboBox.DisplayMember = "CusNameAr"
        OrderClintComboBox.ValueMember = "CustomerId"
        OrderClintComboBox.Enabled = True

    End Sub
End Class