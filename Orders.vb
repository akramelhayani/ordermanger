﻿
Imports System.Data.SqlClient
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks
Imports DevExpress.Utils
Imports DevExpress.XtraGrid.Views.Grid.ViewInfo
Imports System.Drawing.Printing

Public Class Orders
    Private Sub Orders_Load(sender As Object, e As EventArgs) Handles MyBase.Load



        If OrdersWDelete = False Then
            SimpleButton2.Enabled = False
        End If
        If OrdersWPrint = False Then
            Bar1.Visible = False
        End If


        LOAD_Groups()
        ComboBox1.DataSource = GroupDT
        ComboBox1.DisplayMember = "GroupName"


        Try
            DateTimePicker1.Value = Today
            DateTimePicker2.Value = Today

            If CheckButton1.Checked = True And CheckButton2.Checked = False Then
                DateTimePicker1.Enabled = True
                OrderDA = New SqlDataAdapter("Select * from Orders where DeliveryDate between '" & DateTimePicker1.Value.Year & "-" & DateTimePicker1.Value.Month & "-" & DateTimePicker1.Value.Day & "' and  '" & DateTimePicker2.Value.Year & "-" & DateTimePicker2.Value.Month & "-" & DateTimePicker2.Value.Day & "' and IsDeliverd ='False' ", SQlManeCon)
                OrderDT.Clear()
                OrderDA.Fill(OrderDT)
                PivotView()
                DateTimePicker1.Enabled = True
                DateTimePicker2.Enabled = True
                ComboBox1.Enabled = False


            End If

            If CheckButton2.Checked = True And CheckButton1.Checked = False Then
                DateTimePicker1.Enabled = True
                OrderDA = New SqlDataAdapter("Select * from Orders where OrderGroup =N'" & ComboBox1.Text & "' and IsDeliverd ='False' ", SQlManeCon)
                OrderDT.Clear()
                OrderDA.Fill(OrderDT)
                PivotView()
                DateTimePicker1.Enabled = False
                DateTimePicker2.Enabled = False
                ComboBox1.Enabled = True

            End If
            If CheckButton1.Checked = True AndAlso CheckButton2.Checked = True Then
                DateTimePicker1.Enabled = True
                OrderDA = New SqlDataAdapter("Select * from Orders where OrderGroup =N'" & ComboBox1.Text & "' and DeliveryDate between '" & DateTimePicker1.Value.Year & "-" & DateTimePicker1.Value.Month & "-" & DateTimePicker1.Value.Day & "' and  '" & DateTimePicker2.Value.Year & "-" & DateTimePicker2.Value.Month & "-" & DateTimePicker2.Value.Day & "' and IsDeliverd ='False' ", SQlManeCon)
                OrderDT.Clear()
                OrderDA.Fill(OrderDT)
                PivotView()
                DateTimePicker1.Enabled = True
                DateTimePicker2.Enabled = True
                ComboBox1.Enabled = True

            End If
            If CheckButton1.Checked = False AndAlso CheckButton2.Checked = False Then

                DateTimePicker1.Value = Today
                DateTimePicker1.Enabled = False
                Load_Orders()
                PivotView()
                DateTimePicker1.Enabled = False
                DateTimePicker2.Enabled = False
                ComboBox1.Enabled = False

            End If
            GridControl1.BeginUpdate()
            Try
                GridView1.Columns.Clear()
                GridControl1.DataSource = Nothing
                GridControl1.DataSource = OrderDT

            Finally
                GridControl1.EndUpdate()

            End Try

            With GridView1


                .Columns("IsDeliverd").Visible = False
                .Columns("ProductLine").Visible = False
                .Columns("OrderProductID").Visible = False
                .Columns("ProductionDate").Visible = False
                .Columns("OrderInton").Visible = False
                .Columns("CusCode").Visible = False
                .Columns("Id").Visible = False
                .Columns("InvoiceCode").Visible = False


                .Columns("DeliveryDate").Caption = "موعد الاستلام"
                .Columns("DeliveryDate").DisplayFormat.FormatType = FormatType.Custom
                .Columns("DeliveryDate").DisplayFormat.FormatString = "yyyy-MM-dd"




                .Columns("OrderID").Caption = "م"
                .Columns("OrderID").BestFit()
                .Columns("OrderID").MinWidth = 50


                .Columns("OrderClint").Caption = "العميل"
                .Columns("OrderClint").MinWidth = 100




                .Columns("OrderProduct").Caption = "الصنف"
                .Columns("OrderProduct").MinWidth = 80
                .Columns("OrderProduct").MaxWidth = 120


                .Columns("OrderQuntity").Caption = "الكميه"
                .Columns("OrderQuntity").MaxWidth = 50
                .Columns("OrderProduct").MinWidth = 40
                .Columns("OrderID").BestFit()



                .Columns("ProductLine").Caption = "خط الانتاج"
                .Columns("DeliveryCar").Caption = "السياره"
                .Columns("OrderClint").MinWidth = 80


                .Columns("DeliveryDriver").Caption = "السائق"
                .Columns("DeliveryDriver").Visible = False



                .Columns("OrderDate").Caption = "تاريخ الطلب"
                .Columns("OrderDate").DisplayFormat.FormatType = FormatType.Custom
                .Columns("OrderDate").DisplayFormat.FormatString = "yyyy-MM-dd    | hh:mm tt |"
                .Columns("OrderDate").MaxWidth = 160
                .Columns("OrderDate").MinWidth = 120
                .Columns("OrderDate").BestFit()



                GridView1.Columns("OrderGroup").Caption = "الفئه"
                GridView1.Columns("OrderGroup").MaxWidth = 80
                GridView1.Columns("OrderGroup").BestFit()







                GridView1.Columns(0).OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True
                GridView1.Columns(1).OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True

                GridView1.Columns(2).OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False
                GridView1.Columns(4).OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False
                GridView1.Columns(5).OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False

                GridView1.Columns(8).OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False
                GridView1.Columns(9).OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False
                GridView1.Columns(10).OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False

                GridView1.Columns(11).OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False
            End With
        Catch ex As Exception
            log_error(Me.Text, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message)
            ' MsgBox("لقد حدث خطأً ما " & ex.Message & "  تم حفظ الخطأ في ملف سجل الاخطاء الموجود في   " & Application.StartupPath & "/Log.txt  ")

        End Try
    End Sub



    ' Inserts a PageInfoBrick into the top margin to display the time.
    Private Sub composLink_CreateMarginalHeaderArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)
        'e.Graph.DrawPageInfo(PageInfo.DateTime, "{0:hhhh:mmmm:ssss}",
        'Color.Black, New RectangleF(0, 0, 200, 50), BorderSide.None)
    End Sub

    ' Creates a text header for the first grid.
    Private Sub LinkGrid1Report_CreateDetailArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)

        Try


            Dim tb As TextBrick = New TextBrick()



            If CheckButton1.Checked = True Then
                tb.Text = "جدول الطلبيات من تاريخ   " & DateTimePicker1.Value.Date.ToString & "  حتي تاريخ  " & DateTimePicker2.Value.Date & ""
            Else
                tb.Text = " جدول الطلبيات"
            End If
            tb.Font = New Font("Times New Roman", 15, FontStyle.Bold)
            tb.Rect = New RectangleF(0, 0, e.Graph.ClientPageSize.Width, 35)
            tb.BorderWidth = 2
            tb.BackColor = Color.LightGray
            tb.HorzAlignment = DevExpress.Utils.HorzAlignment.Center

            tb.VertAlignment = DevExpress.Utils.VertAlignment.Center

            e.Graph.DrawBrick(tb)
        Catch ex As Exception
            log_error(Me.Text, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message)
            '  MsgBox(ex.Message & "log is saved in " & Application.StartupPath & "Log.txt")
        End Try
    End Sub

    '    ' Creates an interval between the grids and fills it with color.
    Private Sub LinkMainReport_CreateDetailArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)
        If CheckButton2.Checked = True Then
            Dim tb As TextBrick = New TextBrick With {
                .Rect = New RectangleF(0, 0, e.Graph.ClientPageSize.Width, 50),
                .Text = ComboBox1.Text,
                .Font = New Font("Times New Roman", 15, FontStyle.Bold),
                .BackColor = Color.Gray,
                .HorzAlignment = DevExpress.Utils.HorzAlignment.Center,
                .VertAlignment = DevExpress.Utils.VertAlignment.Center
            }

            e.Graph.DrawBrick(tb)
        End If

    End Sub

    ' Creates a text header for the second grid.
    Private Sub LinkGrid2Report_CreateDetailArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)
        Dim tb As TextBrick = New TextBrick()


        tb.Rect = New RectangleF(0, 0, e.Graph.ClientPageSize.Width, 10)

        e.Graph.DrawBrick(tb)
    End Sub


    Private Sub CheckButton1_CheckedChanged(sender As Object, e As EventArgs) Handles CheckButton1.CheckedChanged, CheckButton2.CheckedChanged
        Try
            If CheckButton1.Checked = True And CheckButton2.Checked = False Then
                DateTimePicker1.Enabled = True
                OrderDA = New SqlDataAdapter("Select * from Orders where DeliveryDate between '" & DateTimePicker1.Value.Year & "-" & DateTimePicker1.Value.Month & "-" & DateTimePicker1.Value.Day & "' and  '" & DateTimePicker2.Value.Year & "-" & DateTimePicker2.Value.Month & "-" & DateTimePicker2.Value.Day & "' and IsDeliverd ='False' ", SQlManeCon)
                OrderDT.Clear()
                OrderDA.Fill(OrderDT)
                PivotView()
                DateTimePicker1.Enabled = True
                DateTimePicker2.Enabled = True
                ComboBox1.Enabled = False


            End If

            If CheckButton2.Checked = True And CheckButton1.Checked = False Then
                DateTimePicker1.Enabled = True
                OrderDA = New SqlDataAdapter("Select * from Orders where OrderGroup =N'" & ComboBox1.Text & "' and IsDeliverd ='False' ", SQlManeCon)
                OrderDT.Clear()
                OrderDA.Fill(OrderDT)
                PivotView()
                DateTimePicker1.Enabled = False
                DateTimePicker2.Enabled = False
                ComboBox1.Enabled = True

            End If
            If CheckButton1.Checked = True AndAlso CheckButton2.Checked = True Then
                DateTimePicker1.Enabled = True
                OrderDA = New SqlDataAdapter("Select * from Orders where OrderGroup =N'" & ComboBox1.Text & "' and DeliveryDate between '" & DateTimePicker1.Value.Year & "-" & DateTimePicker1.Value.Month & "-" & DateTimePicker1.Value.Day & "' and  '" & DateTimePicker2.Value.Year & "-" & DateTimePicker2.Value.Month & "-" & DateTimePicker2.Value.Day & "' and IsDeliverd ='False' ", SQlManeCon)
                OrderDT.Clear()
                OrderDA.Fill(OrderDT)
                PivotView()
                DateTimePicker1.Enabled = True
                DateTimePicker2.Enabled = True
                ComboBox1.Enabled = True

            End If
            If CheckButton1.Checked = False AndAlso CheckButton2.Checked = False Then

                DateTimePicker1.Value = Today
                DateTimePicker1.Enabled = False
                Load_Orders()
                PivotView()
                DateTimePicker1.Enabled = False
                DateTimePicker2.Enabled = False
                ComboBox1.Enabled = False

            End If
        Catch ex As Exception
            log_error(Me.Text, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message)
            'MsgBox("لقد حدث خطأً ما " & ex.Message & "  تم حفظ الخطأ في ملف سجل الاخطاء الموجود في   " & Application.StartupPath & "/Log.txt  ")

        End Try
    End Sub

    Private Sub DateTimePicker1_ValueChanged(sender As Object, e As EventArgs) Handles DateTimePicker1.ValueChanged, DateTimePicker2.ValueChanged


        Try
            If CheckButton1.Checked = True And CheckButton2.Checked = False Then
                DateTimePicker1.Enabled = True
                OrderDA = New SqlDataAdapter("Select * from Orders where DeliveryDate between '" & DateTimePicker1.Value.Year & "-" & DateTimePicker1.Value.Month & "-" & DateTimePicker1.Value.Day & "' and  '" & DateTimePicker2.Value.Year & "-" & DateTimePicker2.Value.Month & "-" & DateTimePicker2.Value.Day & "' and IsDeliverd ='False' ", SQlManeCon)
                OrderDT.Clear()
                OrderDA.Fill(OrderDT)
                PivotView()
                DateTimePicker1.Enabled = True
                DateTimePicker2.Enabled = True
                ComboBox1.Enabled = False


            End If

            If CheckButton2.Checked = True And CheckButton1.Checked = False Then
                DateTimePicker1.Enabled = True
                OrderDA = New SqlDataAdapter("Select * from Orders where OrderGroup =N'" & ComboBox1.Text & "' and IsDeliverd ='False' ", SQlManeCon)
                OrderDT.Clear()
                OrderDA.Fill(OrderDT)
                PivotView()
                DateTimePicker1.Enabled = False
                DateTimePicker2.Enabled = False
                ComboBox1.Enabled = True

            End If
            If CheckButton1.Checked = True AndAlso CheckButton2.Checked = True Then
                DateTimePicker1.Enabled = True
                OrderDA = New SqlDataAdapter("Select * from Orders where OrderGroup =N'" & ComboBox1.Text & "' and DeliveryDate between '" & DateTimePicker1.Value.Year & "-" & DateTimePicker1.Value.Month & "-" & DateTimePicker1.Value.Day & "' and  '" & DateTimePicker2.Value.Year & "-" & DateTimePicker2.Value.Month & "-" & DateTimePicker2.Value.Day & "' and IsDeliverd ='False' ", SQlManeCon)
                OrderDT.Clear()
                OrderDA.Fill(OrderDT)
                PivotView()
                DateTimePicker1.Enabled = True
                DateTimePicker2.Enabled = True
                ComboBox1.Enabled = True

            End If
            If CheckButton1.Checked = False AndAlso CheckButton2.Checked = False Then

                DateTimePicker1.Value = Today
                DateTimePicker1.Enabled = False
                Load_Orders()
                PivotView()
                DateTimePicker1.Enabled = False
                DateTimePicker2.Enabled = False
                ComboBox1.Enabled = False

            End If
        Catch ex As Exception
            log_error(Me.Text, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message)
            'MsgBox("لقد حدث خطأً ما " & ex.Message & "  تم حفظ الخطأ في ملف سجل الاخطاء الموجود في   " & Application.StartupPath & "/Log.txt  ")

        End Try


    End Sub




    Private Sub SimpleButton2_Click(sender As Object, e As EventArgs) Handles SimpleButton2.Click


        If MsgBox(" هل تريد حذف الطلبيه ", MsgBoxStyle.YesNo, "تأكيد الحذف") = MsgBoxResult.Yes Then


            Try
                If MsgBox("هل انت متآكد من حذف هذه الطلبيه", MsgBoxStyle.YesNo, "تأكيد الحذف") = MsgBoxResult.Yes Then
                    Dim str As String
                    str = GridView1.GetFocusedRowCellDisplayText("OrderID")
                    Dim tmporderda As New SqlDataAdapter("Select * from ordershead Where OrderID like '" & str & "' ", SQlManeCon)
                    Dim tmporderdt As New DataTable
                    tmporderdt.Clear()
                    tmporderda.Fill(tmporderdt)
                    For i As Integer = tmporderdt.Rows.Count - 1 To 0 Step -1
                        tmporderdt.Rows(i).Delete()
                    Next
                    Dim save As New SqlCommandBuilder(tmporderda)
                    tmporderda.Update(tmporderdt)
                    tmporderdt.AcceptChanges()
                    CheckButton1_CheckedChanged(sender, e)
                    Log_UserAct(Me.Text, "حذف", GridView1.GetFocusedRowCellValue("OrderID"), GridView1.GetFocusedRowCellValue("OrderClint"))


                End If

            Catch ex As Exception
                log_error(Me.Text, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message)
                'MsgBox("لقد حدث خطأً ما " & ex.Message & "  تم حفظ الخطأ في ملف سجل الاخطاء الموجود في   " & Application.StartupPath & "/Log.txt  ")

            End Try
        End If

    End Sub

    Private Sub GridControl1_DoubleClick(sender As Object, e As EventArgs) Handles GridControl1.DoubleClick



        If OrdersWEdit = True Then

            EditOrder.OrderIDTextBox.Text = GridView1.GetFocusedRowCellValue("OrderID")
            EditOrder.OrderClintComboBox.Text = GridView1.GetFocusedRowCellValue("OrderClint")
            EditOrder.OrderDateDateTimePicker.Value = GridView1.GetFocusedRowCellValue("OrderDate")
            EditOrder.DelivaryCarComboBox.Text = GridView1.GetFocusedRowCellDisplayText("DeliveryCar")
            EditOrder.DelivaryDriverComboBox.Text = GridView1.GetFocusedRowCellDisplayText("DeliveryDriver")
            EditOrder.ProductionDateDateTimePicker.Value = GridView1.GetFocusedRowCellValue("ProductionDate")
            EditOrder.DeliveryDateDateTimePicker.Value = GridView1.GetFocusedRowCellValue("DeliveryDate")
            EditOrder.GroupCompoBox.Text = GridView1.GetFocusedRowCellDisplayText("OrderGroup")
            EditOrder.TextBox2.Text = GridView1.GetFocusedRowCellValue("CusCode")
            EditOrder.ShowDialog()
            EditOrder.BringToFront()
            EditOrder.Opacity = 100
            '====== Next Is Handeld in EditeOrder Form 


        End If



    End Sub
    Dim sumorders As New DataTable
    Private Sub PivotView()
        Try


            ProductDA = New SqlDataAdapter("Select * from Product order by sort DESC ", SQlManeCon)
            ProductDT.Clear()
            ProductDA.Fill(ProductDT)

            sumorders.Clear()
            sumorders.Columns.Clear()
            sumorders.Columns.Add("ت", GetType(Integer))

            sumorders.Columns.Add("م", GetType(Integer))

            sumorders.Columns.Add("العميل", GetType(String))

            sumorders.Columns.Add("السيارة", GetType(String))
            '
            For i As Integer = ProductDT.Rows.Count - 1 To 0 Step -1

                Dim chkitemROW() As Data.DataRow
                chkitemROW = OrderDT.Select(" OrderProduct = '" & ProductDT.Rows(i).Item("ProductName") & "' ")

                If chkitemROW.Count > 0 Then
                    sumorders.Columns.Add(ProductDT.Rows(i).Item("ProductName"), GetType(String))
                End If

            Next

            sumorders.Columns.Add("ا.ح.طن", GetType(Double))

            'For i As Integer = ProductDT.Rows.Count - 1 To 0 Step -1
            '    sumorders.Columns.Add(ProductDT.Rows(i).Item("ProductName"), GetType(String))
            'Next



            Dim myrow() As Data.DataRow

            For i As Integer = 0 To OrderDT.Rows.Count - 1 Step +1
                myrow = sumorders.Select(" م = '" & OrderDT.Rows(i).Item("OrderID") & "'")
                If myrow.Count = 0 Then
                    sumorders.Rows.Add()
                    Dim last As Integer = sumorders.Rows.Count - 1
                    sumorders.Rows(last).Item("ت") = sumorders.Rows.Count - 1
                    sumorders.Rows(last).Item("م") = OrderDT.Rows(i).Item("OrderID")
                    sumorders.Rows(last).Item("العميل") = OrderDT.Rows(i).Item("OrderClint")
                    sumorders.Rows(last).Item("السيارة") = OrderDT.Rows(i).Item("DeliveryCar")
                    sumorders.Rows(last).Item("" & OrderDT.Rows(i).Item("OrderProduct") & "") = OrderDT.Rows(i).Item("OrderQuntity")
                    sumorders.Rows(last).Item("ا.ح.طن") = OrderDT.Rows(i).Item("OrderInton")
                End If
                If myrow.Count <> 0 Then

                    myrow(0)("" & OrderDT.Rows(i).Item("OrderProduct") & "") = OrderDT.Rows(i).Item("OrderQuntity")
                    myrow(0)("ا.ح.طن") += OrderDT.Rows(i).Item("OrderInton")
                End If
            Next


            GridControl2.BeginUpdate()
            GridView2.Columns.Clear()
            GridControl2.DataSource = Nothing
            GridControl2.DataSource = sumorders
            GridControl2.EndUpdate()

            GridView2.Columns(0).Visible = False


            For i As Integer = sumorders.Columns.Count - 1 To 4 Step -1
                Try
                    GridView2.Columns(i).Summary.Add(DevExpress.Data.SummaryItemType.Sum, "" & sumorders.Columns(i).ToString & "", "  {0}")

                Catch ex As Exception

                End Try
            Next

            Try
                GridView2.Columns(3).Summary.Add(DevExpress.Data.SummaryItemType.Sum, "العميل", " الاجماليات")

            Catch ex As Exception

            End Try

            For i As Int32 = 0 To GridView2.Columns.Count - 1

                GridView2.Columns(i).BestFit()
                GridView2.Columns(i).AppearanceCell.TextOptions.HAlignment = HorzAlignment.Center
                GridView2.Columns(i).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
            Next
            GridView2.Columns(1).AppearanceCell.TextOptions.HAlignment = HorzAlignment.Near
            GridView2.Columns(1).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Near
            GridView2.Appearance.FooterPanel.TextOptions.HAlignment = HorzAlignment.Center
            GridView2.Appearance.FooterPanel.Options.UseTextOptions = True

            GridView2.ClearSorting()
            GridView2.Columns("ت").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending

        Catch ex As Exception

        End Try
    End Sub



    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox1.SelectedIndexChanged
        If CheckButton1.Checked = True And CheckButton2.Checked = False Then
            DateTimePicker1.Enabled = True
            OrderDA = New SqlDataAdapter("Select * from Orders where DeliveryDate between '" & DateTimePicker1.Value.Year & "-" & DateTimePicker1.Value.Month & "-" & DateTimePicker1.Value.Day & "' and  '" & DateTimePicker2.Value.Year & "-" & DateTimePicker2.Value.Month & "-" & DateTimePicker2.Value.Day & "' and IsDeliverd ='False' ", SQlManeCon)
            OrderDT.Clear()
            OrderDA.Fill(OrderDT)
            PivotView()
            DateTimePicker1.Enabled = True
            DateTimePicker2.Enabled = True
            ComboBox1.Enabled = False


        End If

        If CheckButton2.Checked = True And CheckButton1.Checked = False Then
            DateTimePicker1.Enabled = True
            OrderDA = New SqlDataAdapter("Select * from Orders where OrderGroup =N'" & ComboBox1.Text & "' and IsDeliverd ='False' ", SQlManeCon)
            OrderDT.Clear()
            OrderDA.Fill(OrderDT)
            PivotView()
            DateTimePicker1.Enabled = False
            DateTimePicker2.Enabled = False
            ComboBox1.Enabled = True

        End If
        If CheckButton1.Checked = True AndAlso CheckButton2.Checked = True Then
            DateTimePicker1.Enabled = True
            OrderDA = New SqlDataAdapter("Select * from Orders where OrderGroup =N'" & ComboBox1.Text & "' and DeliveryDate between '" & DateTimePicker1.Value.Year & "-" & DateTimePicker1.Value.Month & "-" & DateTimePicker1.Value.Day & "' and  '" & DateTimePicker2.Value.Year & "-" & DateTimePicker2.Value.Month & "-" & DateTimePicker2.Value.Day & "' and IsDeliverd ='False' ", SQlManeCon)
            OrderDT.Clear()
            OrderDA.Fill(OrderDT)
            PivotView()
            DateTimePicker1.Enabled = True
            DateTimePicker2.Enabled = True
            ComboBox1.Enabled = True

        End If
        If CheckButton1.Checked = False AndAlso CheckButton2.Checked = False Then

            DateTimePicker1.Value = Today
            DateTimePicker1.Enabled = False
            Load_Orders()
            PivotView()
            DateTimePicker1.Enabled = False
            DateTimePicker2.Enabled = False
            ComboBox1.Enabled = False

        End If
    End Sub

    Public Sub SimpleButton4_Click(sender As Object, e As EventArgs) Handles SimpleButton4.Click
        If CheckButton1.Checked = True And CheckButton2.Checked = False Then
            DateTimePicker1.Enabled = True
            OrderDA = New SqlDataAdapter("Select * from Orders where DeliveryDate between '" & DateTimePicker1.Value.Year & "-" & DateTimePicker1.Value.Month & "-" & DateTimePicker1.Value.Day & "' and  '" & DateTimePicker2.Value.Year & "-" & DateTimePicker2.Value.Month & "-" & DateTimePicker2.Value.Day & "' and IsDeliverd ='False' ", SQlManeCon)
            OrderDT.Clear()
            OrderDA.Fill(OrderDT)
            PivotView()
            DateTimePicker1.Enabled = True
            DateTimePicker2.Enabled = True
            ComboBox1.Enabled = False

        End If

        If CheckButton2.Checked = True And CheckButton1.Checked = False Then
            DateTimePicker1.Enabled = True
            OrderDA = New SqlDataAdapter("Select * from Orders where OrderGroup =N'" & ComboBox1.Text & "' and IsDeliverd ='False' ", SQlManeCon)
            OrderDT.Clear()
            OrderDA.Fill(OrderDT)
            PivotView()
            DateTimePicker1.Enabled = False
            DateTimePicker2.Enabled = False
            ComboBox1.Enabled = True

        End If
        If CheckButton1.Checked = True AndAlso CheckButton2.Checked = True Then
            DateTimePicker1.Enabled = True
            OrderDA = New SqlDataAdapter("Select * from Orders where OrderGroup =N'" & ComboBox1.Text & "' and DeliveryDate between '" & DateTimePicker1.Value.Year & "-" & DateTimePicker1.Value.Month & "-" & DateTimePicker1.Value.Day & "' and  '" & DateTimePicker2.Value.Year & "-" & DateTimePicker2.Value.Month & "-" & DateTimePicker2.Value.Day & "' and IsDeliverd ='False' ", SQlManeCon)
            OrderDT.Clear()
            OrderDA.Fill(OrderDT)
            PivotView()
            DateTimePicker1.Enabled = True
            DateTimePicker2.Enabled = True
            ComboBox1.Enabled = True

        End If
        If CheckButton1.Checked = False AndAlso CheckButton2.Checked = False Then

            DateTimePicker1.Value = Today
            DateTimePicker1.Enabled = False
            Load_Orders()
            PivotView()
            DateTimePicker1.Enabled = False
            DateTimePicker2.Enabled = False
            ComboBox1.Enabled = False

        End If
    End Sub

    '''''''''''
    Private Sub btnMoveUp_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnMoveUp.Click
        If GridView2.GetFocusedRowCellValue("ت") > 0 Then
            MoveUp()
        End If

    End Sub
    Private Sub btnMoveDown_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnMoveDown.Click
        If GridView2.GetFocusedRowCellValue("ت") < GridView2.RowCount - 1 Then
            MoveDown()
        End If

    End Sub
    Private Sub SimpleButton5_Click(sender As Object, e As EventArgs) Handles SimpleButton5.Click

        Do Until GridView2.GetFocusedRowCellValue("ت") = (GridView2.RowCount - 1)
            MoveDown()
        Loop
        'For i As Integer = Math.Abs((GridView2.RowCount - 1) - GridView2.GetFocusedRowCellValue("ت")) To 1 Step -1
        '    MoveDown()
        'Next
    End Sub
    Private Sub MoveUp()
        Dim pos As Integer = GridView2.GetFocusedRowCellValue("ت")
        GridView2.SetRowCellValue(pos - 1, "ت", pos)
        GridView2.SetFocusedRowCellValue("ت", pos - 1)
        GridView2.ClearSorting()
        GridView2.Columns("ت").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
    End Sub
    Private Sub MoveDown()
        Dim pos As Integer = GridView2.GetFocusedRowCellValue("ت")
        GridView2.SetRowCellValue(pos + 1, "ت", pos)
        GridView2.SetFocusedRowCellValue("ت", pos + 1)
        GridView2.ClearSorting()
        GridView2.Columns("ت").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
    End Sub
    Private Sub SimpleButton6_Click(sender As Object, e As EventArgs) Handles SimpleButton6.Click

        Do Until GridView2.GetFocusedRowCellValue("ت") = 0
            MoveUp()
        Loop



    End Sub


    Private Sub PrintGrid(Optional ByVal Q As Boolean = False, Optional ByVal G As Integer = 0)

        Dim composLink As CompositeLink = New CompositeLink(New PrintingSystem())
        AddHandler composLink.CreateMarginalHeaderArea, AddressOf composLink_CreateMarginalHeaderArea
        Dim pcLink1 As PrintableComponentLink = New PrintableComponentLink()
        Dim pcLink2 As PrintableComponentLink = New PrintableComponentLink()
        Dim linkMainReport As Link = New Link()
        AddHandler linkMainReport.CreateDetailArea, AddressOf LinkMainReport_CreateDetailArea
        Dim linkGrid1Report As Link = New Link()
        AddHandler linkGrid1Report.CreateDetailArea, AddressOf LinkGrid1Report_CreateDetailArea
        Dim linkGrid2Report As Link = New Link()
        AddHandler linkGrid2Report.CreateDetailArea, AddressOf LinkGrid2Report_CreateDetailArea
        ' Assign the controls to the printing links.
        pcLink1.Component = Me.GridControl1
        pcLink2.Component = Me.GridControl2
        ' Populate the collection of links in the composite link.
        ' The order of operations corresponds to the document structure.
        composLink.Links.Add(linkMainReport)
        composLink.Links.Add(linkGrid1Report)
        If G = 1 Then
            For i As Integer = 0 To BarEditItem1.EditValue


                composLink.Links.Add(pcLink2)
                composLink.Links.Add(linkGrid2Report)
            Next

        ElseIf G = 2 Then

            composLink.Links.Add(pcLink1)
        ElseIf G = 0 Then

            composLink.Links.Add(pcLink2)
            composLink.Links.Add(linkGrid2Report)
            composLink.Links.Add(linkGrid1Report)
            composLink.Links.Add(pcLink1)
        End If

        composLink.PaperKind = My.Settings.PaperSize
        composLink.Landscape = My.Settings.LandScape
        composLink.Margins.Left = My.Settings.MLeft
        composLink.Margins.Right = My.Settings.MRight
        composLink.Margins.Bottom = My.Settings.MBottom
        composLink.Margins.Top = My.Settings.MTop

        If Q = False Then
            composLink.ShowPreviewDialog()
        Else

            composLink.Print(My.Settings.DefultPrinter)

        End If


    End Sub

    Private Sub BarButtonItem1_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem1.ItemClick
        PrintGrid(True, 1)

    End Sub

    Private Sub BarButtonItem2_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem2.ItemClick
        PrintGrid(False, 1)
    End Sub

    Private Sub BarButtonItem3_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem3.ItemClick
        PrintGrid(True, 2)
    End Sub

    Private Sub BarButtonItem4_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem4.ItemClick
        PrintGrid(False, 2)

    End Sub

    Private Sub BarButtonItem5_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem5.ItemClick
        PrintGrid(True, 0)

    End Sub

    Private Sub BarButtonItem6_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem6.ItemClick
        PrintGrid(False, 0)

    End Sub

    Private Sub MakeSL_IN_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles MakeSL_IN.ItemClick

        NewInvoice.Show()
        NewInvoice.OrderNumberlookUp.Text = GridView2.GetFocusedRowCellValue("م")



    End Sub
End Class