﻿Imports System.Data.SqlClient
Public Class Drivers
    Private Sub DriverBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs) Handles DriverBindingNavigatorSaveItem.Click


        Try


            If DriverNameTextBox.Text <> Nothing Then

                Dim CHKDriverDA As New SqlDataAdapter
                Dim CHKDriverDT As New DataTable
                CHKDriverDA = New SqlDataAdapter("select * from Drivers Where driverName like '" & DriverNameTextBox.Text & "' ", SQlManeCon)
                CHKDriverDT.Clear()
                CHKDriverDA.Fill(CHKDriverDT)
                If CHKDriverDT.Rows.Count = 0 Then
                    DriverDT.Rows.Add()
                    Dim last As Integer = DriverDT.Rows.Count - 1
                    DriverDT.Rows(last).Item("DriverName") = DriverNameTextBox.Text
                    Dim save As New SqlCommandBuilder(DriverDA)
                    DriverDA.Update(DriverDT)
                    DriverDT.AcceptChanges()
                    Load_Driver()
                    Log_UserAct(Me.Text, "اضافه", "", DriverNameTextBox.Text)

                ElseIf CHKDriverDT.Rows.Count > 0 Then
                    Dim pos As Integer = BindingContext(DriverDT).Position
                    DriverDT.Rows(pos).Item("Drivername") = DriverNameTextBox.Text
                    Dim save1 As New SqlCommandBuilder(DriverDA)
                    DriverDA.Update(DriverDT)
                    DriverDT.AcceptChanges()
                    Load_Driver()

                End If
            End If
        Catch ex As Exception
            log_error(Me.Text, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message)
            ' MsgBox("لقد حدث خطأً ما " & ex.Message & "  تم حفظ الخطأ في ملف سجل الاخطاء الموجود في   " & Application.StartupPath & "/Log.txt  ")

        End Try
    End Sub

    Private Sub Drivers_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Visible = False
        Try
            Load_Driver()
            DataGridView1.DataSource = DriverDT
            DataGridView1.Columns(0).HeaderText = "اسم السائق"
            BindingNavigatorDeleteItem.Enabled = True
            BindingNavigatorAddNewItem.Enabled = True

        Catch ex As Exception
            log_error(Me.Text, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message)
            '  MsgBox("لقد حدث خطأً ما " & ex.Message & "  تم حفظ الخطأ في ملف سجل الاخطاء الموجود في   " & Application.StartupPath & "/Log.txt  ")

        End Try


    End Sub


    Private Sub DataGridView1_SelectionChanged(sender As Object, e As EventArgs) Handles DataGridView1.SelectionChanged
        Try
            Dim pos As Integer = BindingContext(DriverDT).Position
            DriverNameTextBox.Text = DriverDT.Rows(pos).Item("Drivername")
        Catch


        End Try

    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(sender As Object, e As EventArgs) Handles BindingNavigatorDeleteItem.Click

        Try
            Dim pos As Integer = BindingContext(DriverDT).Position
            DriverDT.Rows(pos).Delete()
            Dim save As New SqlCommandBuilder(DriverDA)
            DriverDA.Update(DriverDT)
            DriverDT.AcceptChanges()
            Load_Driver()
            Log_UserAct(Me.Text, "حذف", "", DriverNameTextBox.Text)

        Catch ex As Exception
            log_error(Me.Text, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message)
            ' MsgBox("لقد حدث خطأً ما " & ex.Message & "  تم حفظ الخطأ في ملف سجل الاخطاء الموجود في   " & Application.StartupPath & "/Log.txt  ")

        End Try


    End Sub

    Private Sub BindingNavigatorAddNewItem_Click(sender As Object, e As EventArgs) Handles BindingNavigatorAddNewItem.Click
        DriverNameTextBox.Text = ""

    End Sub

End Class