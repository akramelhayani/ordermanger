﻿Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks
Imports System

Imports System.Data.SqlClient
Imports DevExpress.XtraEditors.Controls
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports DevExpress.Utils

Public Class CarTable
    Private Sub CarTable_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            DateTimePicker1.Value = Today
            Load_cars()
            ComboBox1.DataSource = CarDT


            ComboBox1.DisplayMember = "PlatNB"


            CheckedComboBoxEdit1.Properties.DataSource = CarDT
            CheckedComboBoxEdit1.Properties.DisplayMember = "PlatNB"

            ptDT.Clear()
            ptDA.Fill(ptDT)
            GridControl1.BeginUpdate()
            Try
                GridView1.Columns.Clear()
                GridControl1.DataSource = Nothing
                GridControl1.DataSource = ptDT

            Finally
                GridControl1.EndUpdate()

            End Try
            GridView1.Columns("Product").Caption = "الصنف"
            GridView1.Columns("Quntity").Caption = "الكميه"

        Catch ex As Exception
            log_error(Me.Text, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message)
            '  MsgBox("لقد حدث خطأً ما " & ex.Message & "  تم حفظ الخطأ في ملف سجل الاخطاء الموجود في   " & Application.StartupPath & "/Log.txt  ")
        End Try

    End Sub

    Dim ptDA As New SqlDataAdapter("Select * from TimpProTable", SQlManeCon)
    Dim ptDT As New DataTable

    Dim timpproda As New SqlDataAdapter
    Dim timpprodt As New DataTable



    Private Sub SimpleButton2_Click(sender As Object, e As EventArgs) Handles SimpleButton2.Click


        Try


            Dim composLink As CompositeLink = New CompositeLink(New PrintingSystem())
            AddHandler composLink.CreateMarginalHeaderArea, AddressOf composLink_CreateMarginalHeaderArea
            Dim pcLink1 As PrintableComponentLink = New PrintableComponentLink()
            Dim pcLink2 As PrintableComponentLink = New PrintableComponentLink()
            Dim linkMainReport As Link = New Link()
            AddHandler linkMainReport.CreateDetailArea, AddressOf linkMainReport_CreateDetailArea
            Dim linkGrid1Report As Link = New Link()
            AddHandler linkGrid1Report.CreateDetailArea, AddressOf linkGrid1Report_CreateDetailArea
            Dim linkGrid2Report As Link = New Link()
            AddHandler linkGrid2Report.CreateDetailArea, AddressOf linkGrid2Report_CreateDetailArea
            ' Assign the controls to the printing links.
            pcLink1.Component = Me.GridControl1
            pcLink2.Component = Me.GridControl2
            ' Populate the collection of links in the composite link.
            ' The order of operations corresponds to the document structure.
            composLink.Links.Add(linkGrid1Report)
            composLink.Links.Add(pcLink1)
            composLink.Links.Add(linkMainReport)
            composLink.Links.Add(linkGrid2Report)
            composLink.Links.Add(pcLink2)

            composLink.PaperKind = My.Settings.PaperSize
            composLink.Landscape = My.Settings.LandScape
            composLink.Margins.Left = My.Settings.MLeft
            composLink.Margins.Right = My.Settings.MRight
            composLink.Margins.Bottom = My.Settings.MBottom
            composLink.Margins.Top = My.Settings.MTop

            ' Create the report and show the preview window.
            composLink.ShowPreviewDialog()



        Catch ex As Exception
            log_error(Me.Text, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message)
            '  MsgBox(ex.Message & "log is saved in " & Application.StartupPath & "Log.txt")
        End Try



    End Sub

    ' Inserts a PageInfoBrick into the top margin to display the time.
    Private Sub composLink_CreateMarginalHeaderArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)
        'e.Graph.DrawPageInfo(PageInfo.DateTime, "{0:hhhh:mmmm:ssss}",
        'Color.Black, New RectangleF(0, 0, 200, 50), BorderSide.None)
    End Sub

    ' Creates a text header for the first grid.
    Private Sub linkGrid1Report_CreateDetailArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)

        Try


            Dim tb As TextBrick = New TextBrick()
            tb.Text = "  حموله سياره رقم " & ComboBox1.Text & "  بتاريخ  " & DateTimePicker1.Value.Date
            tb.Font = New Font("Times New Roman", 15, FontStyle.Bold)
            tb.Rect = New RectangleF(0, 0, e.Graph.ClientPageSize.Width, 25)
            tb.BorderWidth = 2
            tb.BackColor = Color.LightGray
            tb.HorzAlignment = DevExpress.Utils.HorzAlignment.Center

            tb.VertAlignment = DevExpress.Utils.VertAlignment.Center

            e.Graph.DrawBrick(tb)
        Catch ex As Exception
            log_error(Me.Text, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message)
            '  MsgBox(ex.Message & "log is saved in " & Application.StartupPath & "Log.txt")
        End Try
    End Sub

    '    ' Creates an interval between the grids and fills it with color.
    Private Sub linkMainReport_CreateDetailArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)
        'Dim tb As TextBrick = New TextBrick()
        'tb.Rect = New RectangleF(0, 0, e.Graph.ClientPageSize.Width, 50)
        'tb.BackColor = Color.Gray
        'e.Graph.DrawBrick(tb)
    End Sub

    ' Creates a text header for the second grid.
    Private Sub linkGrid2Report_CreateDetailArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)
        'Dim tb As TextBrick = New TextBrick()
        'tb.Text = "Suppliers"
        'tb.Font = New Font("Arial", 15)
        'tb.Rect = New RectangleF(0, 0, 300, 25)
        'tb.BorderWidth = 0
        'tb.BackColor = Color.Transparent
        'tb.HorzAlignment = DevExpress.Utils.HorzAlignment.Near
        'e.Graph.DrawBrick(tb)
    End Sub


    Private Sub SimpleButton3_Click(sender As Object, e As EventArgs) Handles SimpleButton3.Click
        Try


            Load_Product()
            ptDT.Clear()
            For i As Integer = ProductDT.Rows.Count - 1 To 0 Step -1
                timpproda = New SqlDataAdapter("Select * from Orders where deliveryCar =N'" & ComboBox1.Text & "' and OrderProduct =N'" & ProductDT.Rows(i).Item("ProductName") & "' and deliveryDate = '" & DateTimePicker1.Value.Year & "-" & DateTimePicker1.Value.Month & "-" & DateTimePicker1.Value.Day & "' and IsDeliverd ='False' ", SQlManeCon)
                timpprodt.Clear()
                timpproda.Fill(timpprodt)

                If timpprodt.Rows.Count > 0 Then
                    ptDT.Rows.Add()
                    Dim last As Integer = ptDT.Rows.Count - 1
                    ptDT.Rows(last).Item(0) = ProductDT.Rows(i).Item("ProductName")
                    ptDT.Rows(last).Item(1) = timpprodt.Compute("Sum(OrderQuntity)", String.Empty)
                End If
            Next



            OrderDA = New SqlDataAdapter("Select * from Orders where  deliveryCar =N'" & ComboBox1.Text & "' and DeliveryDate = '" & DateTimePicker1.Value.Year & "-" & DateTimePicker1.Value.Month & "-" & DateTimePicker1.Value.Day & "' and IsDeliverd ='False' ", SQlManeCon)
            OrderDT.Clear()
            OrderDA.Fill(OrderDT)
            GridControl2.BeginUpdate()
            Try
                GridView2.Columns.Clear()
                GridControl2.DataSource = Nothing
                GridControl2.DataSource = OrderDT
            Finally
                GridControl2.EndUpdate()
            End Try
            GridView1.Columns(1).Summary.Clear()

            GridView1.Columns(1).Summary.Add(DevExpress.Data.SummaryItemType.Sum, "الكميه", " اجمالي الحموله بالطن " & OrderDT.Compute("SUM(OrderInTon)", String.Empty) & String.Empty)

            GridView2.Columns("IsDeliverd").Visible = False
            GridView2.Columns("ProductLine").Visible = False
            GridView2.Columns("ProductionDate").Visible = False
            GridView2.Columns("OrderInton").Visible = False
            GridView2.Columns("DeliveryDate").Caption = "موعد الاستلام"
            'GridView2.Columns("OrderID").Visible = False
            GridView2.Columns("OrderID").Caption = "م"
            GridView2.Columns("OrderID").BestFit()

            GridView2.Columns("OrderClint").Caption = "العميل"
            GridView2.Columns("OrderClint").Width = 120
            GridView2.Columns("OrderProduct").Caption = "الصنف"
            GridView2.Columns("OrderQuntity").Caption = "الكميه"
            GridView2.Columns("OrderQuntity").Width = 54
            GridView2.Columns("CusCode").Visible = False
            GridView2.Columns("Id").Visible = False
            GridView2.Columns("InvoiceCode").Visible = False
            GridView2.Columns("OrderGroup").Caption = "الفئه"
            GridView2.Columns("DeliveryCar").Caption = "السياره"
            GridView2.Columns("DeliveryDriver").Caption = "السائق"
            GridView2.Columns("OrderDate").Caption = "تاريخ الطلب"
            GridView2.Columns("OrderDate").DisplayFormat.FormatType = FormatType.Custom
            GridView2.Columns("OrderDate").DisplayFormat.FormatString = "yyyy-MM-dd hh:mm"
            GridView2.Columns(0).OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True
            GridView2.Columns(1).OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True
            GridView2.Columns(2).OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False
            GridView2.Columns(3).OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False
            GridView2.Columns(4).OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False
            GridView2.Columns(7).OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False
            GridView2.Columns(8).OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False
            GridView2.Columns(9).OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False
            GridView2.Columns(10).OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False
            GridView2.Columns(11).OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False
            GridView2.Columns(12).OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False


        Catch ex As Exception
            log_error(Me.Text, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message)
            'MsgBox("لقد حدث خطأً ما " & ex.Message & "  تم حفظ الخطأ في ملف سجل الاخطاء الموجود في   " & Application.StartupPath & "/Log.txt  ")
        End Try
    End Sub

    Private Sub DateTimePicker1_ValueChanged(sender As Object, e As EventArgs) Handles DateTimePicker1.ValueChanged
        Try
            SimpleButton3_Click(sender, e)
        Catch ex As Exception
            log_error(Me.Text, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message)
            ' MsgBox("لقد حدث خطأً ما " & ex.Message & "  تم حفظ الخطأ في ملف سجل الاخطاء الموجود في   " & Application.StartupPath & "/Log.txt  ")
        End Try


    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox1.SelectedIndexChanged
        Try
            SimpleButton3_Click(sender, e)
        Catch ex As Exception
            log_error(Me.Text, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message)
            ' MsgBox("لقد حدث خطأً ما " & ex.Message & "  تم حفظ الخطأ في ملف سجل الاخطاء الموجود في   " & Application.StartupPath & "/Log.txt  ")
        End Try


    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        Try
            Dim MultCarDA As New SqlDataAdapter
            Dim MultCarDt As New DataTable
            MultCarDt.Clear()

            For Each item As CheckedListBoxItem In CheckedComboBoxEdit1.Properties.Items
                If item.CheckState = CheckState.Checked Then

                    MultCarDA = New SqlDataAdapter("Select * from Orders where  deliveryCar =N'" & item.ToString & "' and DeliveryDate = '" & DateTimePicker1.Value.Year & "-" & DateTimePicker1.Value.Month & "-" & DateTimePicker1.Value.Day & "' and IsDeliverd ='False' ", SQlManeCon)
                    MultCarDA.Fill(MultCarDt)

                End If
            Next
            '-======================================================================
            Dim ProductDA1 As New SqlDataAdapter("Select * from Product order by sort DESC ", SQlManeCon)
            Dim ProductDT1 As New DataTable

            ProductDT1.Clear()
            ProductDA1.Fill(ProductDT1)

            'GridView1.Columns("OrderID").Caption = "م"
            'GridView1.Columns("OrderID").BestFit()


            Dim sumorders As New DataTable

            sumorders.Columns.Add("السيارة", GetType(String))

            sumorders.Columns.Add("م", GetType(Integer))

            sumorders.Columns.Add("العميل", GetType(String))

            '
            For i As Integer = ProductDT.Rows.Count - 1 To 0 Step -1

                Dim chkitemROW() As Data.DataRow
                chkitemROW = MultCarDt.Select(" OrderProduct = '" & ProductDT.Rows(i).Item("ProductName") & "' ")

                If chkitemROW.Count > 0 Then
                    sumorders.Columns.Add(ProductDT.Rows(i).Item("ProductName"), GetType(String))
                End If

            Next

            sumorders.Columns.Add("ا.ح.طن", GetType(Double))



            For i As Integer = MultCarDt.Rows.Count - 1 To 0 Step -1
                Dim myrow() As Data.DataRow
                myrow = sumorders.Select(" م = '" & MultCarDt.Rows(i).Item("OrderID") & "'")

                If myrow.Count = 0 Then
                    sumorders.Rows.Add()
                    Dim last As Integer = sumorders.Rows.Count - 1
                    sumorders.Rows(last).Item("العميل") = MultCarDt.Rows(i).Item("OrderClint")
                    sumorders.Rows(last).Item("م") = MultCarDt.Rows(i).Item("OrderID")
                    sumorders.Rows(last).Item("السيارة") = MultCarDt.Rows(i).Item("DeliveryCar")
                    sumorders.Rows(last).Item("" & MultCarDt.Rows(i).Item("OrderProduct") & "") = MultCarDt.Rows(i).Item("OrderQuntity")
                    sumorders.Rows(last).Item("ا.ح.طن") = MultCarDt.Rows(i).Item("OrderInton")
                End If
                If myrow.Count <> 0 Then
                    myrow(0)("" & MultCarDt.Rows(i).Item("OrderProduct") & "") = MultCarDt.Rows(i).Item("OrderQuntity")
                    myrow(0)("ا.ح.طن") += MultCarDt.Rows(i).Item("OrderInton")

                End If
            Next


            GridControl3.BeginUpdate()
            GridView3.Columns.Clear()
            GridControl3.DataSource = Nothing
            GridControl3.DataSource = sumorders

            GridControl3.EndUpdate()


            For i As Integer = sumorders.Columns.Count - 1 To 3 Step -1
                GridView3.Columns(i).Summary.Add(DevExpress.Data.SummaryItemType.Sum, "" & sumorders.Columns(i).ToString & "", "  ={0}")
            Next
            GridView3.Columns(2).Summary.Add(DevExpress.Data.SummaryItemType.Sum, "العميل", " الاجماليات")

            GridView3.BeginSort()
            Try
                GridView3.ClearGrouping()
                GridView3.Columns("السيارة").GroupIndex = 0

            Finally
                GridView3.EndSort()
            End Try


            GridView3.OptionsView.GroupFooterShowMode = GroupFooterShowMode.VisibleAlways
            Dim tmp1da As New SqlDataAdapter
            Dim tmp1dt As New DataTable

            For i As Integer = sumorders.Columns.Count - 2 To 3 Step -1

                tmp1da = New SqlDataAdapter(" select * from Product Where ProductName =N'" & sumorders.Columns(i).ColumnName & "' ", SQlManeCon)
                tmp1dt.Clear()
                tmp1da.Fill(tmp1dt)

                ' Create and setup the second summary item.
                Dim sum2 As GridGroupSummaryItem = New GridGroupSummaryItem()
                sum2.FieldName = "" & sumorders.Columns(i).ColumnName & ""
                sum2.SummaryType = DevExpress.Data.SummaryItemType.Sum
                sum2.DisplayFormat = " " & tmp1dt.Rows(0).Item("ProductUnit") & " {0} "



                sum2.ShowInGroupColumnFooter = GridView3.Columns(i)



                GridView3.GroupSummary.Add(sum2)

                'Dim a As String
                'Dim b As String
                'a = sumorders.Columns(i).ColumnName
                'b = Replace(a, " ", "_")

                'MsgBox(Convert.ToString(sumorders.Compute("SUM(ارنب_كبير)", String.Empty)))
                ''MsgBox("(" & sumorders.Columns(i).ColumnName & ")")

            Next

            '==========================================


            Dim Sum As GridGroupSummaryItem = New GridGroupSummaryItem
            Sum.FieldName = ("ا.ح.طن")
            Sum.ShowInGroupColumnFooter = GridView3.Columns("ا.ح.طن")
            Sum.SummaryType = DevExpress.Data.SummaryItemType.Sum
            Sum.DisplayFormat = " = {0} طن"
            GridView3.GroupSummary.Add(Sum)

            GridView3.ExpandAllGroups()


            GridView3.Columns("م").Caption = "م"
            GridView3.Columns("م").BestFit()
            GridView3.Columns("م").Width = 100




            For i As Int32 = 0 To GridView3.Columns.Count - 1

                GridView3.Columns(i).BestFit()
                GridView3.Columns(i).AppearanceCell.TextOptions.HAlignment = HorzAlignment.Center
                GridView3.Columns(i).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center

            Next
            GridView3.Columns(1).AppearanceCell.TextOptions.HAlignment = HorzAlignment.Near
            GridView3.Columns(1).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Near
            GridView3.Appearance.FooterPanel.TextOptions.HAlignment = HorzAlignment.Center
            GridView3.Appearance.FooterPanel.Options.UseTextOptions = True

        Catch ex As Exception
            'MsgBox(ex.Message)

        End Try

    End Sub

    Private Sub SimpleButton4_Click(sender As Object, e As EventArgs) Handles SimpleButton4.Click

        If SimpleButton4.Text = "ضم" Then
            GridView3.CollapseAllGroups()
            SimpleButton4.Text = "فرد"
        Else
            GridView3.ExpandAllGroups()
            SimpleButton4.Text = "ضم"

        End If

    End Sub

    Private Sub SimpleButton5_Click(sender As Object, e As EventArgs) Handles SimpleButton5.Click


        Try


            Dim composLink As CompositeLink = New CompositeLink(New PrintingSystem())
            AddHandler composLink.CreateMarginalHeaderArea, AddressOf composLink_CreateMarginalHeaderArea


            Dim pcLink1 As PrintableComponentLink = New PrintableComponentLink()

            Dim linkGrid1Report3 As Link = New Link()
            AddHandler linkGrid1Report3.CreateDetailArea, AddressOf linkGrid1Report3_CreateDetailArea
            ' Assign the controls to the printing links.
            pcLink1.Component = Me.GridControl3

            ' Populate the collection of links in the composite link.
            ' The order of operations corresponds to the document structure.
            composLink.Links.Add(linkGrid1Report3)
            composLink.Links.Add(pcLink1)


            composLink.PaperKind = My.Settings.PaperSize
            composLink.Landscape = My.Settings.LandScape
            composLink.Margins.Left = My.Settings.MLeft
            composLink.Margins.Right = My.Settings.MRight
            composLink.Margins.Bottom = My.Settings.MBottom
            composLink.Margins.Top = My.Settings.MTop


            ' Create the report and show the preview window.
            composLink.ShowPreviewDialog()

        Catch ex As Exception
            log_error(Me.Text, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message)
            '  MsgBox(ex.Message & "log is saved in " & Application.StartupPath & "Log.txt")
        End Try




    End Sub


    Private Sub linkGrid1Report3_CreateDetailArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)

        Try


            Dim tb As TextBrick = New TextBrick()
            tb.Text = "  حموله السيارات " & CheckedComboBoxEdit1.Text & "  بتاريخ  " & DateTimePicker1.Value.Date '.ToString
            tb.Font = New Font("Times New Roman", 12, FontStyle.Bold)

            tb.Rect = New RectangleF(0, 0, e.Graph.ClientPageSize.Width, 80)
            tb.BorderWidth = 2

            tb.BackColor = Color.LightGray
            tb.HorzAlignment = DevExpress.Utils.HorzAlignment.Center

            tb.VertAlignment = DevExpress.Utils.VertAlignment.Center

            e.Graph.DrawBrick(tb)
        Catch ex As Exception
            log_error(Me.Text, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message)
            '  MsgBox(ex.Message & "log is saved in " & Application.StartupPath & "Log.txt")
        End Try
    End Sub

    Private Sub SimpleButton6_Click(sender As Object, e As EventArgs) Handles SimpleButton6.Click


        GridView3.ShowPrintPreview()

    End Sub

    Private Sub CheckedComboBoxEdit1_EditValueChanged(sender As Object, e As EventArgs) Handles CheckedComboBoxEdit1.EditValueChanged

        SimpleButton1_Click(sender, e)


    End Sub
End Class