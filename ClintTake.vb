﻿Imports System.Data.SqlClient
Imports DevExpress.Utils
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks

Public Class ClintTake

    Dim ClintOrdersDA As New SqlDataAdapter
    Dim ClintOrdersDT As New DataTable
    Dim ClintOrdersDetailsDA As New SqlDataAdapter
    Dim ClintOrdersDetailsDT As New DataTable
    Dim PivotGrid As New DataTable

    Private Sub LoadClintsList()
        Load_clients()
        OrderClintComboBox.DataSource = SQLDT
        OrderClintComboBox.DisplayMember = "CusNameAr"
        OrderClintComboBox.ValueMember = "CustomerId"
        Dim autocomplet As New AutoCompleteStringCollection
        For i As Integer = 0 To SQLDT.Rows.Count - 1
            autocomplet.Add(SQLDT.Rows(i)("CusNameAr"))
        Next
        OrderClintComboBox.AutoCompleteCustomSource = autocomplet
        OrderClintComboBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        OrderClintComboBox.AutoCompleteSource = AutoCompleteSource.CustomSource
        OrderClintComboBox.Text = "عميل عام"
    End Sub
    Private Sub SearchForClintOrder()
        'ClintOrdersDA
        ClintOrdersDA = New SqlDataAdapter("SELECT OrderID,OrderDate,TrueDeliveryDate,InvoiceCode,OrderPrice 
        FROM DoneOrders where OrderClint = N'" & OrderClintComboBox.Text & "' and TrueDeliveryDate between '" & DateEdit1.DateTime.Year & "-" & DateEdit1.DateTime.Month & "-" & DateEdit1.DateTime.Day & "' and '" & DateEdit2.DateTime.Year & "-" & DateEdit2.DateTime.Month & "-" & DateEdit2.DateTime.Day & "'
        ", SQlManeCon)
        ClintOrdersDT.Clear()
        ClintOrdersDA.Fill(ClintOrdersDT)

        ClintOrdersDetailsDA = New SqlDataAdapter("SELECT 
                                                         DoneOrdersDetails.OrderID
                                                        ,DoneOrdersDetails.OrderProduct
                                                        ,DoneOrdersDetails.OrderQuntity
                                                        ,DoneOrdersDetails.TrueOrderQuntity
                                                        ,DoneOrdersDetails.TrueOrderQuntityInTon
                                                        FROM ( DoneOrders inner join  DoneOrdersDetails on DoneOrdersDetails.OrderID =  DoneOrders.OrderID )
                                                        where OrderClint = N'" & OrderClintComboBox.Text & "' and TrueDeliveryDate between '" & DateEdit1.DateTime.Year & "-" & DateEdit1.DateTime.Month & "-" & DateEdit1.DateTime.Day & "' and '" & DateEdit2.DateTime.Year & "-" & DateEdit2.DateTime.Month & "-" & DateEdit2.DateTime.Day & "' and  TrueOrderQuntity <> 0 
                                                        ", SQlManeCon)
        ClintOrdersDetailsDT.Clear()
        ClintOrdersDetailsDA.Fill(ClintOrdersDetailsDT)

        PivotGrid.Clear()
        PivotGrid.Columns.Clear()
        PivotGrid.Columns.Add("م", GetType(Integer))
        PivotGrid.Columns.Add("كود الفاتوره", GetType(Integer))
        PivotGrid.Columns.Add("تاريخ الطلب", GetType(DateTime))
        PivotGrid.Columns.Add("تاريخ الفاتوره", GetType(DateTime))
        PivotGrid.Columns.Add("اجمالي الفاتوره", GetType(Double))

        ProductDA = New SqlDataAdapter("Select * from Product order by sort DESC ", SQlManeCon)
        ProductDT.Clear()
        ProductDA.Fill(ProductDT)

        For i As Integer = ProductDT.Rows.Count - 1 To 0 Step -1
            Dim chkitemROW() As Data.DataRow
            chkitemROW = ClintOrdersDetailsDT.Select(" OrderProduct = '" & ProductDT.Rows(i).Item("ProductName") & "' ")
            If chkitemROW.Count > 0 Then
                PivotGrid.Columns.Add(ProductDT.Rows(i).Item("ProductName"), GetType(String))
            End If
        Next
        PivotGrid.Columns.Add("ا.ح.ط", GetType(Double))

        For i As Integer = ClintOrdersDT.Rows.Count - 1 To 0 Step -1

            PivotGrid.Rows.Add()
            Dim last As Integer = PivotGrid.Rows.Count - 1
            PivotGrid.Rows(last).Item("م") = ClintOrdersDT.Rows(i).Item("OrderID")
            PivotGrid.Rows(last).Item("كود الفاتوره") = ClintOrdersDT.Rows(i).Item("InvoiceCode")
            PivotGrid.Rows(last).Item("تاريخ الطلب") = ClintOrdersDT.Rows(i).Item("OrderDate")
            PivotGrid.Rows(last).Item("تاريخ الفاتوره") = ClintOrdersDT.Rows(i).Item("TrueDeliveryDate")
            PivotGrid.Rows(last).Item("اجمالي الفاتوره") = ClintOrdersDT.Rows(i).Item("OrderPrice")
            PivotGrid.Rows(last).Item("ا.ح.ط") = 0
        Next

        Dim myrow

        For i As Integer = ClintOrdersDetailsDT.Rows.Count - 1 To 0 Step -1
            myrow = PivotGrid.Select(" م = '" & ClintOrdersDetailsDT.Rows(i).Item("OrderID") & "'")
            myrow(0)("" & ClintOrdersDetailsDT.Rows(i).Item("OrderProduct") & "") = ClintOrdersDetailsDT.Rows(i).Item("TrueOrderQuntity")
            myrow(0)("ا.ح.ط") += ClintOrdersDetailsDT.Rows(i).Item("TrueOrderQuntityInTon")
        Next







    End Sub
    Private Sub ArrangeGrid()


        For i As Integer = GridView1.Columns.Count - 1 To 4 Step -1
            Try
                GridView1.Columns(i).Summary.Add(DevExpress.Data.SummaryItemType.Sum, "" & GridView1.Columns(i).ToString & "", "  {0}")

            Catch ex As Exception

            End Try
        Next

        Try
            GridView1.Columns(3).Summary.Add(DevExpress.Data.SummaryItemType.Sum, "العميل", " الاجماليات")

        Catch ex As Exception

        End Try

        For i As Int32 = 0 To GridView1.Columns.Count - 1

            GridView1.Columns(i).BestFit()
            GridView1.Columns(i).AppearanceCell.TextOptions.HAlignment = HorzAlignment.Center
            GridView1.Columns(i).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next
        GridView1.Columns(1).AppearanceCell.TextOptions.HAlignment = HorzAlignment.Near
        GridView1.Columns(1).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Near
        GridView1.Appearance.FooterPanel.TextOptions.HAlignment = HorzAlignment.Center
        GridView1.Appearance.FooterPanel.Options.UseTextOptions = True
        GridView1.Columns(2).DisplayFormat.FormatType = FormatType.Custom
        GridView1.Columns(2).DisplayFormat.FormatString = "yyyy-MM-dd hh:mm:ss tt "
        GridView1.Columns(3).DisplayFormat.FormatType = FormatType.Custom
        GridView1.Columns(3).DisplayFormat.FormatString = "yyyy-MM-dd hh:mm:ss tt "




    End Sub


    Private Sub ClintTake_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        LoadClintsList()


        DateEdit1.DateTime = " " & Now.Year & "-" & Now.AddMonths(-1).Month & "-1 "

        DateEdit2.EditValue = " " & Now.Year & "-" & Now.Month & "-1 "


    End Sub

    Private Sub SearchButton_Click(sender As Object, e As EventArgs) Handles SearchButton.Click

        SearchForClintOrder()
        GridControl1.DataSource = Nothing
        GridView1.Columns.Clear()
        GridControl1.DataSource = PivotGrid
        GridControl1.EndUpdate()
        GridView1.OptionsView.GroupFooterShowMode = GroupFooterShowMode.VisibleAlways
        ArrangeGrid()

    End Sub

    Private Sub PrintButton_Click(sender As Object, e As EventArgs) Handles PrintButton.Click
        PrintGrid()

    End Sub
    Private Sub PrintGrid()

        Dim composLink As CompositeLink = New CompositeLink(New PrintingSystem())

        Dim pcLink1 As PrintableComponentLink = New PrintableComponentLink()

        Dim linkGrid2Report As Link = New Link()
        AddHandler linkGrid2Report.CreateDetailArea, AddressOf LinkGrid2Report_CreateDetailArea
        ' Assign the controls to the printing links.
        pcLink1.Component = Me.GridControl1

        ' Populate the collection of links in the composite link.
        ' The order of operations corresponds to the document structure.


        composLink.Links.Add(linkGrid2Report)
        composLink.Links.Add(pcLink1)

        composLink.PaperKind = My.Settings.PaperSize
        composLink.Landscape = My.Settings.LandScape
        composLink.Margins.Left = My.Settings.MLeft
        composLink.Margins.Right = My.Settings.MRight
        composLink.Margins.Bottom = My.Settings.MBottom
        composLink.Margins.Top = My.Settings.MTop

        composLink.ShowPreviewDialog()



    End Sub
    Private Sub LinkGrid2Report_CreateDetailArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)
        Dim tb As TextBrick = New TextBrick()
        With tb
            .Text = "  تقرير مسحوبات العميل   " & OrderClintComboBox.Text & "  " & " خلال الفتره من  " & DateEdit1.EditValue & "  الي  " & DateEdit2.EditValue
            .Rect = New RectangleF(0, 0, e.Graph.ClientPageSize.Width, 50)
            .Font = New Font("Times New Roman", 15, FontStyle.Bold)
            .BackColor = Color.Gray
            .HorzAlignment = DevExpress.Utils.HorzAlignment.Center
            .VertAlignment = DevExpress.Utils.VertAlignment.Center
            .BorderWidth = 2
            .BorderStyle = BrickBorderStyle.Outset
            .BorderColor = Color.Black

        End With
        e.Graph.DrawBrick(tb)

    End Sub

End Class