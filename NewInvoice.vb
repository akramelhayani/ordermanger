﻿Imports System.Data.SqlClient
Imports DevExpress.XtraEditors.Repository

Public Class NewInvoice
    Dim ItemsTmpDT As New DataTable
    Dim CusCode As Integer
    Private Sub LoadDrawers()
        Dim da As New SqlDataAdapter("SELECT DrawerNameAr ,AccountId FROM ACC_Drawer", SQLCon)
        Dim dt As New DataTable
        dt.Clear()
        da.Fill(dt)
        dt.Columns(0).Caption = "اسم الخزينه"
        dt.Columns(1).Caption = "رقم الخزينه"
        da = New SqlDataAdapter("SELECT  BankName,AccountId FROM ACC_Bank", SQLCon)
        Dim dt1 As New DataTable
        dt1.Clear()
        da.Fill(dt1)
        For i As Integer = 0 To dt1.Rows.Count - 1
            dt.Rows.Add()
            dt.Rows(dt.Rows.Count - 1).Item(0) = dt1.Rows(i).Item(0)
            dt.Rows(dt.Rows.Count - 1).Item(1) = dt1.Rows(i).Item(1)
        Next
        With LookUpEdit1.Properties
            .DataSource = dt
            .DisplayMember = "DrawerNameAr"
            .ValueMember = "AccountId"

        End With
        LookUpEdit1.EditValue = 9



    End Sub
    Private Sub LoadStores()
        Dim da As New SqlDataAdapter("SELECT  StoreId  , StoreNameAr  , SellAccount  FROM  IC_Store ", SQLCon)
        Dim dt As New DataTable
        dt.Clear()
        da.Fill(dt)
        dt.Columns(0).Caption = "كود"
        dt.Columns(1).Caption = "اسم المخزن"
        dt.Columns(2).Caption = "حساب المبيعات"

        With LookUpEdit2.Properties
            .DataSource = dt
            .DisplayMember = "StoreNameAr"
            .ValueMember = "StoreId"
        End With
        LookUpEdit2.EditValue = 6



    End Sub
    Private Sub MakeTmpDT()
        Dim ItemID As New DataColumn
        Dim ItemName As New DataColumn
        Dim ItemUnit As New DataColumn
        Dim ItemQty As New DataColumn
        Dim ItemPrice As New DataColumn
        Dim TotalPrice As New DataColumn
        With ItemID
            .ColumnName = "ItemID"
            .Caption = "كود"
            .DataType = GetType(Integer)
        End With
        With ItemName
            .ColumnName = "ItemName"
            .Caption = "الصنف"
            .DataType = GetType(String)
        End With
        With ItemUnit
            .ColumnName = "ItemUnit"
            .Caption = "الوحده"
            .DataType = GetType(String)
        End With
        With ItemQty
            .ColumnName = "ItemQty"
            .Caption = "الكمية"
            .DataType = GetType(Double)
            .DefaultValue = 1
        End With
        With ItemPrice
            .ColumnName = "ItemPrice"
            .Caption = "السعر"
            .DataType = GetType(Double)
        End With
        With TotalPrice
            .ColumnName = "TotalPrice"
            .Caption = "الاجمالي"
            .DataType = GetType(Double)
        End With
        With ItemsTmpDT
            .Clear()
            .Columns.Clear()
            .Columns.Add(ItemID)
            .Columns.Add(ItemName)
            .Columns.Add(ItemUnit)
            .Columns.Add(ItemPrice)
            .Columns.Add(ItemQty)
            .Columns.Add(TotalPrice)


        End With
    End Sub
    Private Sub LoadRepoItem()
        Dim da As New SqlDataAdapter("select ProductName , ProductID from product", SQlManeCon)
        Dim dt As New DataTable
        dt.Clear()
        da.Fill(dt)
        dt.Columns(0).Caption = "الصنف"
        dt.Columns(1).Caption = "كود"

        Dim ItemRepo As New RepositoryItemLookUpEdit
        ItemRepo.Name = "ItemsRepoEdit"
        ItemRepo.DataSource = dt
        ItemRepo.DisplayMember = "ProductName"
        ItemRepo.ValueMember = "ProductID"
        GridView1.Columns("ItemName").ColumnEdit = ItemRepo
        GridControl1.RepositoryItems.Add(ItemRepo)

    End Sub
    Private Sub LoadOrders()

        'Dim da As New SqlDataAdapter(" SELECT  OrderID , OrderClint , DeliveryCar , DeliveryDriver , CusCode FROM Orders where IsDeliverd = 0 ", SQlManeCon)
        Dim da As New SqlDataAdapter(" SELECT  OrderID , OrderClint FROM Orders where IsDeliverd = 0 ", SQlManeCon)
        Dim dt As New DataTable
        dt.Clear()
        da.Fill(dt)
        Dim OrdersDT As New DataTable
        OrdersDT.Columns.Add("OrderID", GetType(String))
        OrdersDT.Columns.Add("OrderClint", GetType(String))
        OrdersDT.Columns("OrderID").Caption = "رقم  الطلبيه"
        OrdersDT.Columns("OrderClint").Caption = "العميل"
        'OrdersDT.Columns.Add("DeliveryCar", GetType(String))
        'OrdersDT.Columns.Add("DeliveryDriver", GetType(String))
        'OrdersDT.Columns.Add("CusCode", GetType(String))
        For i As Integer = dt.Rows.Count - 1 To 0 Step -1
            Dim chkROW() As Data.DataRow
            chkROW = OrdersDT.Select(" OrderID = '" & dt.Rows(i).Item("OrderID") & "' ")
            If chkROW.Count = 0 Then
                OrdersDT.Rows.Add()
                Dim last As Integer = OrdersDT.Rows.Count - 1
                OrdersDT.Rows(last).Item("OrderID") = dt.Rows(i).Item("OrderID")
                OrdersDT.Rows(last).Item("OrderClint") = dt.Rows(i).Item("OrderClint")
                'OrdersDT.Rows(last).Item("DeliveryCar") = dt.Rows(i).Item("DeliveryCar")
                'OrdersDT.Rows(last).Item("DeliveryDriver") = dt.Rows(i).Item("DeliveryDriver")
                'OrdersDT.Rows(last).Item("CusCode") = dt.Rows(i).Item("CusCode")
            End If
        Next
        With OrderNumberlookUp.Properties
            .DataSource = OrdersDT
            .DisplayMember = "OrderID"

        End With





    End Sub
    Private Function MakeInvoiceCode()

        Dim da As New SqlDataAdapter("Select IDENT_CURRENT('SL_Invoice')", SQLCon)
        Dim dt As New DataTable
        dt.Clear()
        da.Fill(dt)
        Return dt(0)(0) + 1


    End Function
    Private Function MakeJurnalCode()

        Dim da As New SqlDataAdapter("Select IDENT_CURRENT('ACC_Journal')", SQLCon)
        Dim dt As New DataTable
        dt.Clear()
        da.Fill(dt)
        Return dt(0)(0) + 1

    End Function
    Public Sub ConvertToSL_Invoice(OID As Integer)
        Dim da As New SqlDataAdapter("SELECT [OrderID]
      ,[OrderClint]
      ,[OrderProduct]
      ,[OrderProductID]
      ,[OrderQuntity]
      ,[DeliveryCar]
      ,[DeliveryDriver]
      ,[CusCode]
  FROM [OrderMangerDB].[dbo].[Orders] where OrderID = '" & OID & "' ", SQlManeCon)

        Dim dt As New DataTable
        dt.Clear()
        da.Fill(dt)
        TextBox2.Text = dt.Rows(0).Item("OrderClint")
        CusCode = dt.Rows(0).Item("CusCode")
        TextBox1.Text = MakeInvoiceCode()
        TextEdit3.Text = dt.Rows(0).Item("DeliveryDriver")
        TextEdit4.Text = dt.Rows(0).Item("DeliveryCar")
        ItemsTmpDT.Clear()

        For i As Integer = 0 To dt.Rows.Count - 1
            ItemsTmpDT.Rows.Add()
            Dim last As Integer = ItemsTmpDT.Rows.Count - 1
            ItemsTmpDT.Rows(last).Item("ItemID") = dt.Rows(i).Item("OrderProductID")
            ItemsTmpDT.Rows(last).Item("ItemName") = dt.Rows(i).Item("OrderProductID")
            ItemsTmpDT.Rows(last).Item("ItemQty") = dt.Rows(i).Item("OrderQuntity")

        Next
        ChkItemsGrid()
        TextEdit1.Text = ItemsTmpDT.Compute("SUM(TotalPrice)", String.Empty)
        SpinEdit2.EditValue = TextEdit1.Text








    End Sub
    Private Sub ChkItemsGrid()
        For i As Integer = 0 To ItemsTmpDT.Rows.Count - 1
            Dim da As New SqlDataAdapter(" SELECT  MediumUOMPrice FROM IC_Item where ItemId = '" & ItemsTmpDT.Rows(i).Item("ItemName") & "' ", SQLCon)
            Dim dt As New DataTable
            dt.Clear()
            da.Fill(dt)
            ItemsTmpDT.Rows(i).Item("ItemPrice") = dt(0)(0)
            da = New SqlDataAdapter("SELECT ProductUnit From Product Where ProductID = '" & ItemsTmpDT.Rows(i).Item("ItemName") & "' ", SQlManeCon)
            dt.Clear()
            dt.Columns.Clear()
            da.Fill(dt)
            ItemsTmpDT.Rows(i).Item("ItemUnit") = dt(0)(0)
            ItemsTmpDT.Rows(i).Item("TotalPrice") = ItemsTmpDT.Rows(i).Item("ItemQty") * ItemsTmpDT.Rows(i).Item("ItemPrice")
        Next




    End Sub
    Private Sub NewInvoice_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        MakeTmpDT()
        GridControl1.DataSource = ItemsTmpDT
        GridView1.Columns("ItemID").OptionsColumn.AllowEdit = False
        GridView1.Columns("ItemUnit").OptionsColumn.AllowEdit = False
        GridView1.Columns("TotalPrice").OptionsColumn.AllowEdit = False

        LoadRepoItem()
        LoadOrders()
        DateEdit1.EditValue = Now
        LoadDrawers()
        LoadStores()


    End Sub
    Private Sub GridView1_CellValueChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles GridView1.CellValueChanged

        Select Case e.Column.FieldName
            Case "ItemName"
                GridView1.SetFocusedRowCellValue("ItemID", GridView1.GetFocusedRowCellValue("ItemName"))

            Case "ItemID"
                Dim da As New SqlDataAdapter(" SELECT  MediumUOMPrice FROM IC_Item where ItemId = '" & GridView1.GetFocusedRowCellValue("ItemName") & "' ", SQLCon)
                Dim dt As New DataTable
                dt.Clear()
                da.Fill(dt)
                GridView1.SetFocusedRowCellValue("ItemPrice", dt(0)(0))
                da = New SqlDataAdapter("SELECT ProductUnit From Product Where ProductID = '" & GridView1.GetFocusedRowCellValue("ItemName") & "' ", SQlManeCon)
                dt.Clear()
                dt.Columns.Clear()
                da.Fill(dt)
                GridView1.SetFocusedRowCellValue("ItemUnit", dt(0)(0))


            Case "ItemPrice"
                Try
                    GridView1.SetFocusedRowCellValue("TotalPrice", GridView1.GetFocusedRowCellValue("ItemQty") * GridView1.GetFocusedRowCellValue("ItemPrice"))
                Catch
                End Try
            Case "ItemQty"
                Try
                    GridView1.SetFocusedRowCellValue("TotalPrice", GridView1.GetFocusedRowCellValue("ItemQty") * GridView1.GetFocusedRowCellValue("ItemPrice"))
                Catch
                End Try
            Case "TotalPrice"
                TextEdit1.Text = ItemsTmpDT.Compute("SUM(TotalPrice)", String.Empty)
                SpinEdit2.EditValue = TextEdit1.Text




        End Select






    End Sub
    Private Sub OrderNumberlookUp_EditValueChanged(sender As Object, e As EventArgs) Handles OrderNumberlookUp.EditValueChanged
        ConvertToSL_Invoice(OrderNumberlookUp.Text)
        SaveButton1.Enabled = True

    End Sub
    Private Sub SpinEdit2_EditValueChanged(sender As Object, e As EventArgs) Handles SpinEdit2.EditValueChanged, TextEdit1.EditValueChanged
        SpinEdit1.EditValue = TextEdit1.Text - SpinEdit2.EditValue
    End Sub
    Private Sub SaveSl_Invoice()
        SQLCon.Open()

        Dim InvoiceCode As Integer = MakeInvoiceCode()
        Dim PayMethod As Boolean = 0
        Dim JornalId As Integer = MakeJurnalCode()


        If SpinEdit1.EditValue = 0 Then
            PayMethod = 1
        End If
        If InvoiceCode <> TextBox1.Text Then
            Invoke(Sub() MsgBox("تم تغير كود الفاتوره ليصبح   " & InvoiceCode & " ", MsgBoxStyle.Information, "تنويه "))
        End If

        Dim cmd As New SqlCommand
        cmd.Connection = SQLCon
        cmd.CommandText = "Insert Into ACC_Journal 
              (
               JCode
              ,JNotes
              ,ProcessId
              ,SourceId
              ,InsertUser
              ,InsertDate
              ,StoreId
              ,JNumber)
values (       
             '" & JornalId & "',  
             ' فاتوره مبيعات رقم " & InvoiceCode & "  الي عميل  " & TextBox2.Text & "  بواسطة مدير الطلبات ',  
             '2',  
             '" & InvoiceCode & "',  
             '" & My.Settings.UserID & "',  
             '" & DateEdit1.DateTime.ToString("yyyy-MM-dd hh:mm:ss tt") & "',  
             '" & LookUpEdit2.EditValue & "',  
             '" & InvoiceCode & "'   
                  
)

"
        cmd.ExecuteNonQuery()

        cmd.CommandText = "Insert Into SL_Invoice (
       
       InvoiceCode
      ,StoreId
      ,CustomerId
      ,InvoiceDate
      ,Net
      ,Paid
      ,Remains
      ,PayMethod
      ,UserId
      ,JornalId
      ,DrawerAccountId
      ,PurchaseOrderNo
      ,DueDate
      ,AttnMr
      ,TotalCostPrice
      ,DriverName
      ,VehicleNumber
      , Notes)
Values ( '" & InvoiceCode & "',
         '" & LookUpEdit2.EditValue & "',
         '" & CusCode & "',
         '" & DateEdit1.DateTime.ToString("yyyy-MM-dd hh:mm:ss tt") & "',
         '" & TextEdit1.Text & "',
         '" & SpinEdit2.EditValue & "',
         '" & SpinEdit1.EditValue & "',
         '" & PayMethod & "',
         '" & My.Settings.UserID & "',
         '" & JornalId & "',
         '" & LookUpEdit1.EditValue & "',
         '" & OrderNumberlookUp.Text & "',
         '" & DateEdit1.DateTime.ToString("yyyy-MM-dd hh:mm:ss tt") & "',
         '" & TextBox2.Text & "',
         '0',
         '" & TextEdit3.Text & "',
         '" & TextEdit4.Text & "',
         'تم انشاء الفاتوره بواسطه مدير الطلبات '
)"
        cmd.ExecuteNonQuery()

        cmd.CommandText = " Insert Into ACC_JournalDetail  ( 
             JournalId
           , AccountId
           , Debit
           , Credit
           , Notes
           , DueDate) 
values (  '" & JornalId & "',
          '" & GetCusAccID() & "',
          '" & TextEdit1.Text & "',
          '0',
         ' فاتوره مبيعات رقم " & InvoiceCode & "  الي عميل  " & TextBox2.Text & "  بواسطة مدير الطلبات ',
          '" & DateEdit1.DateTime.ToString("yyyy-MM-dd hh:mm:ss tt") & "'
)"

        cmd.ExecuteNonQuery()

        cmd.CommandText = " Insert Into ACC_JournalDetail  ( 
             JournalId
           , AccountId
           , Debit
           , Credit
           , Notes
           , DueDate) 
values (  '" & JornalId & "',
          '" & GetCusAccID() & "',
          '0',
          '" & SpinEdit2.EditValue & "',
         ' فاتوره مبيعات رقم " & InvoiceCode & "  الي عميل  " & TextBox2.Text & "  بواسطة مدير الطلبات  سداد ',
          '" & DateEdit1.DateTime.ToString("yyyy-MM-dd hh:mm:ss tt") & "'
)"

        cmd.ExecuteNonQuery()

        cmd.CommandText = " Insert Into ACC_JournalDetail  ( 
             JournalId
           , AccountId
           , Debit
           , Credit
           , Notes
           , DueDate) 
values (  '" & JornalId & "',
          '" & GetStoreSellAccID() & "',
          '0',
          '" & TextEdit1.Text & "',
         ' فاتوره مبيعات رقم " & InvoiceCode & "  الي عميل  " & TextBox2.Text & "  بواسطة مدير الطلبات ',
          '" & DateEdit1.DateTime.ToString("yyyy-MM-dd hh:mm:ss tt") & "'
)"

        cmd.ExecuteNonQuery()
        cmd.CommandText = " Insert Into ACC_JournalDetail  ( 
             JournalId
           , AccountId
           , Debit
           , Credit
           , Notes
           , DueDate) 
values (  '" & JornalId & "',
          '" & LookUpEdit1.EditValue & "',
          '" & SpinEdit2.EditValue & "',
          '0',
         ' فاتوره مبيعات رقم " & InvoiceCode & "  الي عميل  " & TextBox2.Text & "  بواسطة مدير الطلبات  سداد ',
          '" & DateEdit1.DateTime.ToString("yyyy-MM-dd hh:mm:ss tt") & "'
)"
        cmd.ExecuteNonQuery()


        For i As Integer = 0 To ItemsTmpDT.Rows.Count - 1


            Dim GetUOMda As New SqlDataAdapter("SELECT  MediumUOM ,MediumUOMFactor FROM IC_Item  where ItemId  = '" & ItemsTmpDT.Rows(i).Item("ItemID") & "'", SQLCon)
            Dim GetUOMdt As New DataTable
            GetUOMdt.Clear()
            GetUOMda.Fill(GetUOMdt)

            cmd.CommandText = " Insert Into SL_InvoiceDetail 
        (   SL_InvoiceId
            ,ItemId
            ,UOMIndex
            ,UOMId
            ,Qty
            ,SellPrice
            ,TotalSellPrice ) 
Values  (  '" & InvoiceCode & "',
           '" & ItemsTmpDT.Rows(i).Item("ItemID") & "',
           '1',
           '" & GetUOMdt.Rows(0).Item("MediumUOM") & "',
           '" & ItemsTmpDT.Rows(i).Item("ItemQty") & "',
           '" & ItemsTmpDT.Rows(i).Item("ItemPrice") & "',
           '" & ItemsTmpDT.Rows(i).Item("TotalPrice") & "'
        )"

            cmd.ExecuteNonQuery()

        Next

        Dim da As New SqlDataAdapter("SELECT SL_InvoiceDetailId  ,ItemId  ,Qty ,TotalSellPrice FROM  SL_InvoiceDetail  where SL_InvoiceId = '" & InvoiceCode & "' ", SQLCon)
        Dim dt As New DataTable
        dt.Clear()
        da.Fill(dt)
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim getFactorDA As New SqlDataAdapter("SELECT MediumUOMFactor FROM IC_Item  where ItemId  = '" & dt.Rows(i).Item("ItemId") & "'", SQLCon)
            Dim getFactorDT As New DataTable
            getFactorDT.Clear()
            getFactorDA.Fill(getFactorDT)
            Dim Totalqty As Double = (dt.Rows(i).Item("Qty") * getFactorDT.Rows(0).Item(0))
            cmd.CommandText = " Insert Into IC_ItemStore 
        (
                  ProcessId
                , SourceId
                , ItemId
                , StoreId
                , Qty
                , IsInTrns
                , InsertTime
                , SellPrice 
                ,PurchasePrice
        )
   Values (    '2', 
               '" & dt.Rows(i).Item("SL_InvoiceDetailId") & "', 
               '" & dt.Rows(i).Item("ItemId") & "', 
               '" & LookUpEdit2.EditValue & "', 
               '" & Totalqty & "', 
               '0', 
               '" & DateEdit1.DateTime.ToString("yyyy-MM-dd hh:mm:ss tt") & "', 
               '" & dt.Rows(i).Item("TotalSellPrice") & "',
               '0'
          )  "

            cmd.ExecuteNonQuery()




        Next


        cmd.CommandText = "Insert Into ST_UserLog ( 
               ScreenId
              ,ProcessId
              ,Code
              ,NotesAr
              ,UserId
              ,ActionDate

) Values ( '15',
           '2',
           '" & InvoiceCode & "',
           'تحويل الطلبيه رقم  '" & OrderNumberlookUp.Text & "'  الي فاتوره برقم '" & InvoiceCode & "' ',
           '" & My.Settings.UserID & "'
           '" & DateEdit1.DateTime.ToString("yyyy-MM-dd hh:mm:ss tt") & "'
) 

"







        SQLCon.Close()

    End Sub
    Private Function GetCusAccID()

        Dim da As New SqlDataAdapter("SELECT  AccountId  FROM  SL_Customer where CustomerId = '" & CusCode & "' ", SQLCon)
        Dim dt As New DataTable
        da.Fill(dt)
        Return dt(0)(0)

    End Function
    Private Function GetStoreSellAccID()

        Dim dr As DataRowView
        dr = LookUpEdit2.GetSelectedDataRow

        Return dr.Row("SellAccount")

    End Function
    Private Sub SaveButton1_Click(sender As Object, e As EventArgs) Handles SaveButton1.Click
        SaveSl_Invoice()
        MsgBox(" تم انشاء الفاتوره بنجاح")
        HomeScreen.LoadDoneOrders()
        LoadOrders()
        Orders.SimpleButton4_Click(sender, e)
        SaveButton1.Enabled = False




    End Sub
End Class