﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Products
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim ProductNameLabel As System.Windows.Forms.Label
        Dim ProductionLineLabel As System.Windows.Forms.Label
        Dim ProductUnitLabel As System.Windows.Forms.Label
        Dim ProductUnitsInTonLabel As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Products))
        Me.ProductNameTextBox = New System.Windows.Forms.TextBox()
        Me.ProductionLineTextBox = New System.Windows.Forms.ComboBox()
        Me.ProductUnitTextBox = New System.Windows.Forms.TextBox()
        Me.ProductUnitsInTonTextBox = New System.Windows.Forms.TextBox()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton2 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton3 = New System.Windows.Forms.ToolStripButton()
        Me.NumericUpDown1 = New System.Windows.Forms.NumericUpDown()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        ProductNameLabel = New System.Windows.Forms.Label()
        ProductionLineLabel = New System.Windows.Forms.Label()
        ProductUnitLabel = New System.Windows.Forms.Label()
        ProductUnitsInTonLabel = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        Label2 = New System.Windows.Forms.Label()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ToolStrip1.SuspendLayout()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ProductNameLabel
        '
        ProductNameLabel.AutoSize = True
        ProductNameLabel.Location = New System.Drawing.Point(608, 38)
        ProductNameLabel.Name = "ProductNameLabel"
        ProductNameLabel.Size = New System.Drawing.Size(35, 13)
        ProductNameLabel.TabIndex = 1
        ProductNameLabel.Text = "الصنف"
        '
        'ProductionLineLabel
        '
        ProductionLineLabel.AutoSize = True
        ProductionLineLabel.Location = New System.Drawing.Point(590, 64)
        ProductionLineLabel.Name = "ProductionLineLabel"
        ProductionLineLabel.Size = New System.Drawing.Size(53, 13)
        ProductionLineLabel.TabIndex = 3
        ProductionLineLabel.Text = "خط الانتاج"
        '
        'ProductUnitLabel
        '
        ProductUnitLabel.AutoSize = True
        ProductUnitLabel.Location = New System.Drawing.Point(606, 90)
        ProductUnitLabel.Name = "ProductUnitLabel"
        ProductUnitLabel.Size = New System.Drawing.Size(37, 13)
        ProductUnitLabel.TabIndex = 5
        ProductUnitLabel.Text = "الوحده"
        '
        'ProductUnitsInTonLabel
        '
        ProductUnitsInTonLabel.AutoSize = True
        ProductUnitsInTonLabel.Location = New System.Drawing.Point(539, 116)
        ProductUnitsInTonLabel.Name = "ProductUnitsInTonLabel"
        ProductUnitsInTonLabel.Size = New System.Drawing.Size(104, 13)
        ProductUnitsInTonLabel.TabIndex = 7
        ProductUnitsInTonLabel.Text = "عدد الوحدات في الطن"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Location = New System.Drawing.Point(607, 146)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(38, 13)
        Label1.TabIndex = 1
        Label1.Text = "الترتيب"
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.Location = New System.Drawing.Point(537, 186)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(106, 13)
        Label2.TabIndex = 1
        Label2.Text = "اسم الصنف ف البرنامج"
        '
        'ProductNameTextBox
        '
        Me.ProductNameTextBox.Location = New System.Drawing.Point(433, 35)
        Me.ProductNameTextBox.Name = "ProductNameTextBox"
        Me.ProductNameTextBox.Size = New System.Drawing.Size(148, 20)
        Me.ProductNameTextBox.TabIndex = 2
        '
        'ProductionLineTextBox
        '
        Me.ProductionLineTextBox.Location = New System.Drawing.Point(433, 61)
        Me.ProductionLineTextBox.Name = "ProductionLineTextBox"
        Me.ProductionLineTextBox.Size = New System.Drawing.Size(148, 21)
        Me.ProductionLineTextBox.TabIndex = 4
        '
        'ProductUnitTextBox
        '
        Me.ProductUnitTextBox.Location = New System.Drawing.Point(433, 87)
        Me.ProductUnitTextBox.Name = "ProductUnitTextBox"
        Me.ProductUnitTextBox.Size = New System.Drawing.Size(148, 20)
        Me.ProductUnitTextBox.TabIndex = 6
        '
        'ProductUnitsInTonTextBox
        '
        Me.ProductUnitsInTonTextBox.Location = New System.Drawing.Point(433, 113)
        Me.ProductUnitsInTonTextBox.Name = "ProductUnitsInTonTextBox"
        Me.ProductUnitsInTonTextBox.Size = New System.Drawing.Size(100, 20)
        Me.ProductUnitsInTonTextBox.TabIndex = 8
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(12, 35)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.Size = New System.Drawing.Size(415, 245)
        Me.DataGridView1.TabIndex = 9
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1, Me.ToolStripButton2, Me.ToolStripButton3})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(655, 25)
        Me.ToolStrip1.TabIndex = 10
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton1.Image = Global.OrderManger.My.Resources.Resources.floppy_disk_save_button_icon_65887
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton1.Text = "حفظ"
        '
        'ToolStripButton2
        '
        Me.ToolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton2.Image = Global.OrderManger.My.Resources.Resources._627249_delete3_512
        Me.ToolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton2.Name = "ToolStripButton2"
        Me.ToolStripButton2.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton2.Text = "ToolStripButton2"
        '
        'ToolStripButton3
        '
        Me.ToolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton3.Image = Global.OrderManger.My.Resources.Resources.orange_plus_sign_icon_32197
        Me.ToolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton3.Name = "ToolStripButton3"
        Me.ToolStripButton3.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton3.Text = "ToolStripButton3"
        '
        'NumericUpDown1
        '
        Me.NumericUpDown1.Location = New System.Drawing.Point(433, 139)
        Me.NumericUpDown1.Name = "NumericUpDown1"
        Me.NumericUpDown1.Size = New System.Drawing.Size(100, 20)
        Me.NumericUpDown1.TabIndex = 11
        '
        'ComboBox1
        '
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(433, 202)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(207, 21)
        Me.ComboBox1.TabIndex = 12
        '
        'Products
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(655, 290)
        Me.Controls.Add(Me.ComboBox1)
        Me.Controls.Add(Me.NumericUpDown1)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Label2)
        Me.Controls.Add(Label1)
        Me.Controls.Add(ProductNameLabel)
        Me.Controls.Add(Me.ProductNameTextBox)
        Me.Controls.Add(ProductionLineLabel)
        Me.Controls.Add(Me.ProductionLineTextBox)
        Me.Controls.Add(ProductUnitLabel)
        Me.Controls.Add(Me.ProductUnitTextBox)
        Me.Controls.Add(ProductUnitsInTonLabel)
        Me.Controls.Add(Me.ProductUnitsInTonTextBox)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Products"
        Me.Opacity = 0R
        Me.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Text = "الاصناف"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ProductNameTextBox As TextBox
    Friend WithEvents ProductionLineTextBox As ComboBox
    Friend WithEvents ProductUnitTextBox As TextBox
    Friend WithEvents ProductUnitsInTonTextBox As TextBox
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents ToolStrip1 As ToolStrip
    Friend WithEvents ToolStripButton1 As ToolStripButton
    Friend WithEvents ToolStripButton2 As ToolStripButton
    Friend WithEvents ToolStripButton3 As ToolStripButton

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Friend WithEvents NumericUpDown1 As NumericUpDown
    Friend WithEvents ComboBox1 As ComboBox
End Class
