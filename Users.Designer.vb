﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Users
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim UserNameLabel As System.Windows.Forms.Label
        Dim UserPassWordLabel As System.Windows.Forms.Label
        Me.NavigationPane1 = New DevExpress.XtraBars.Navigation.NavigationPane()
        Me.NavigationPage3 = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.ProductionLineWCheckEdit = New DevExpress.XtraEditors.CheckEdit()
        Me.ProductWCheckEdit = New DevExpress.XtraEditors.CheckEdit()
        Me.CarsWCheckEdit = New DevExpress.XtraEditors.CheckEdit()
        Me.DriverWCheckEdit = New DevExpress.XtraEditors.CheckEdit()
        Me.GroupWCheckEdit = New DevExpress.XtraEditors.CheckEdit()
        Me.NavigationPage4 = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.OrdersWPrintCheckEdit = New DevExpress.XtraEditors.CheckEdit()
        Me.OrdersWDeleteCheckEdit = New DevExpress.XtraEditors.CheckEdit()
        Me.OrdersWEditCheckEdit = New DevExpress.XtraEditors.CheckEdit()
        Me.CarTableWCheckEdit = New DevExpress.XtraEditors.CheckEdit()
        Me.NewOrderWCheckEdit = New DevExpress.XtraEditors.CheckEdit()
        Me.OrdersWCheckEdit = New DevExpress.XtraEditors.CheckEdit()
        Me.DoneOrdersWCheckEdit = New DevExpress.XtraEditors.CheckEdit()
        Me.NavigationPage1 = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.ProductionTableWCheckEdit = New DevExpress.XtraEditors.CheckEdit()
        Me.NavigationPage2 = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.UserWCheckEdit = New DevExpress.XtraEditors.CheckEdit()
        Me.UserHSTWCheckEdit = New DevExpress.XtraEditors.CheckEdit()
        Me.SettingMWCheckEdit = New DevExpress.XtraEditors.CheckEdit()
        Me.OrdersMWCheckEdit = New DevExpress.XtraEditors.CheckEdit()
        Me.ProductionMWCheckEdit = New DevExpress.XtraEditors.CheckEdit()
        Me.UserMWCheckEdit = New DevExpress.XtraEditors.CheckEdit()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton3 = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.UserNameTextEdit = New System.Windows.Forms.TextBox()
        Me.UserPassWordTextEdit = New System.Windows.Forms.TextBox()
        UserNameLabel = New System.Windows.Forms.Label()
        UserPassWordLabel = New System.Windows.Forms.Label()
        CType(Me.NavigationPane1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.NavigationPane1.SuspendLayout()
        Me.NavigationPage3.SuspendLayout()
        CType(Me.ProductionLineWCheckEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProductWCheckEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CarsWCheckEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DriverWCheckEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupWCheckEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.NavigationPage4.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.OrdersWPrintCheckEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.OrdersWDeleteCheckEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.OrdersWEditCheckEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CarTableWCheckEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewOrderWCheckEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.OrdersWCheckEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DoneOrdersWCheckEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.NavigationPage1.SuspendLayout()
        CType(Me.ProductionTableWCheckEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.NavigationPage2.SuspendLayout()
        CType(Me.UserWCheckEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UserHSTWCheckEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SettingMWCheckEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.OrdersMWCheckEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProductionMWCheckEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UserMWCheckEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'UserNameLabel
        '
        UserNameLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        UserNameLabel.AutoSize = True
        UserNameLabel.Location = New System.Drawing.Point(499, 13)
        UserNameLabel.Name = "UserNameLabel"
        UserNameLabel.Size = New System.Drawing.Size(53, 14)
        UserNameLabel.TabIndex = 3
        UserNameLabel.Text = "اسم المستخدم"
        '
        'UserPassWordLabel
        '
        UserPassWordLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        UserPassWordLabel.AutoSize = True
        UserPassWordLabel.Location = New System.Drawing.Point(509, 53)
        UserPassWordLabel.Name = "UserPassWordLabel"
        UserPassWordLabel.Size = New System.Drawing.Size(39, 14)
        UserPassWordLabel.TabIndex = 5
        UserPassWordLabel.Text = "كلمه السر"
        '
        'NavigationPane1
        '
        Me.NavigationPane1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.NavigationPane1.Controls.Add(Me.NavigationPage3)
        Me.NavigationPane1.Controls.Add(Me.NavigationPage4)
        Me.NavigationPane1.Controls.Add(Me.NavigationPage1)
        Me.NavigationPane1.Controls.Add(Me.NavigationPage2)
        Me.NavigationPane1.Location = New System.Drawing.Point(12, 19)
        Me.NavigationPane1.Name = "NavigationPane1"
        Me.NavigationPane1.PageProperties.ShowExpandButton = False
        Me.NavigationPane1.Pages.AddRange(New DevExpress.XtraBars.Navigation.NavigationPageBase() {Me.NavigationPage3, Me.NavigationPage4, Me.NavigationPage1, Me.NavigationPage2})
        Me.NavigationPane1.RegularSize = New System.Drawing.Size(499, 267)
        Me.NavigationPane1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.NavigationPane1.SelectedPage = Me.NavigationPage4
        Me.NavigationPane1.Size = New System.Drawing.Size(499, 267)
        Me.NavigationPane1.TabIndex = 2
        '
        'NavigationPage3
        '
        Me.NavigationPage3.Caption = "الاعدادات "
        Me.NavigationPage3.Controls.Add(Me.ProductionLineWCheckEdit)
        Me.NavigationPage3.Controls.Add(Me.ProductWCheckEdit)
        Me.NavigationPage3.Controls.Add(Me.CarsWCheckEdit)
        Me.NavigationPage3.Controls.Add(Me.DriverWCheckEdit)
        Me.NavigationPage3.Controls.Add(Me.GroupWCheckEdit)
        Me.NavigationPage3.Name = "NavigationPage3"
        Me.NavigationPage3.Size = New System.Drawing.Size(405, 207)
        '
        'ProductionLineWCheckEdit
        '
        Me.ProductionLineWCheckEdit.Location = New System.Drawing.Point(247, 169)
        Me.ProductionLineWCheckEdit.Name = "ProductionLineWCheckEdit"
        Me.ProductionLineWCheckEdit.Properties.Caption = "خطوط الانتاج"
        Me.ProductionLineWCheckEdit.Size = New System.Drawing.Size(122, 19)
        Me.ProductionLineWCheckEdit.TabIndex = 18
        '
        'ProductWCheckEdit
        '
        Me.ProductWCheckEdit.Location = New System.Drawing.Point(247, 131)
        Me.ProductWCheckEdit.Name = "ProductWCheckEdit"
        Me.ProductWCheckEdit.Properties.Caption = "الاصناف"
        Me.ProductWCheckEdit.Size = New System.Drawing.Size(122, 19)
        Me.ProductWCheckEdit.TabIndex = 16
        '
        'CarsWCheckEdit
        '
        Me.CarsWCheckEdit.Location = New System.Drawing.Point(247, 93)
        Me.CarsWCheckEdit.Name = "CarsWCheckEdit"
        Me.CarsWCheckEdit.Properties.Caption = "السيارات"
        Me.CarsWCheckEdit.Size = New System.Drawing.Size(122, 19)
        Me.CarsWCheckEdit.TabIndex = 14
        '
        'DriverWCheckEdit
        '
        Me.DriverWCheckEdit.Location = New System.Drawing.Point(247, 55)
        Me.DriverWCheckEdit.Name = "DriverWCheckEdit"
        Me.DriverWCheckEdit.Properties.Caption = "السائقون"
        Me.DriverWCheckEdit.Size = New System.Drawing.Size(122, 19)
        Me.DriverWCheckEdit.TabIndex = 12
        '
        'GroupWCheckEdit
        '
        Me.GroupWCheckEdit.Location = New System.Drawing.Point(247, 17)
        Me.GroupWCheckEdit.Name = "GroupWCheckEdit"
        Me.GroupWCheckEdit.Properties.Caption = "المجموعات"
        Me.GroupWCheckEdit.Size = New System.Drawing.Size(122, 19)
        Me.GroupWCheckEdit.TabIndex = 10
        '
        'NavigationPage4
        '
        Me.NavigationPage4.Caption = "الطلبيات"
        Me.NavigationPage4.Controls.Add(Me.GroupBox1)
        Me.NavigationPage4.Controls.Add(Me.CarTableWCheckEdit)
        Me.NavigationPage4.Controls.Add(Me.NewOrderWCheckEdit)
        Me.NavigationPage4.Controls.Add(Me.OrdersWCheckEdit)
        Me.NavigationPage4.Controls.Add(Me.DoneOrdersWCheckEdit)
        Me.NavigationPage4.Name = "NavigationPage4"
        Me.NavigationPage4.Size = New System.Drawing.Size(405, 207)
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.OrdersWPrintCheckEdit)
        Me.GroupBox1.Controls.Add(Me.OrdersWDeleteCheckEdit)
        Me.GroupBox1.Controls.Add(Me.OrdersWEditCheckEdit)
        Me.GroupBox1.Location = New System.Drawing.Point(79, 96)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(198, 40)
        Me.GroupBox1.TabIndex = 35
        Me.GroupBox1.TabStop = False
        '
        'OrdersWPrintCheckEdit
        '
        Me.OrdersWPrintCheckEdit.Location = New System.Drawing.Point(24, 14)
        Me.OrdersWPrintCheckEdit.Name = "OrdersWPrintCheckEdit"
        Me.OrdersWPrintCheckEdit.Properties.Caption = "طباعه"
        Me.OrdersWPrintCheckEdit.Size = New System.Drawing.Size(52, 19)
        Me.OrdersWPrintCheckEdit.TabIndex = 32
        '
        'OrdersWDeleteCheckEdit
        '
        Me.OrdersWDeleteCheckEdit.Location = New System.Drawing.Point(82, 14)
        Me.OrdersWDeleteCheckEdit.Name = "OrdersWDeleteCheckEdit"
        Me.OrdersWDeleteCheckEdit.Properties.Caption = "حذف"
        Me.OrdersWDeleteCheckEdit.Size = New System.Drawing.Size(49, 19)
        Me.OrdersWDeleteCheckEdit.TabIndex = 30
        '
        'OrdersWEditCheckEdit
        '
        Me.OrdersWEditCheckEdit.Location = New System.Drawing.Point(137, 14)
        Me.OrdersWEditCheckEdit.Name = "OrdersWEditCheckEdit"
        Me.OrdersWEditCheckEdit.Properties.Caption = "تعديل"
        Me.OrdersWEditCheckEdit.Size = New System.Drawing.Size(58, 19)
        Me.OrdersWEditCheckEdit.TabIndex = 28
        '
        'CarTableWCheckEdit
        '
        Me.CarTableWCheckEdit.Location = New System.Drawing.Point(232, 29)
        Me.CarTableWCheckEdit.Name = "CarTableWCheckEdit"
        Me.CarTableWCheckEdit.Properties.Caption = "جدول تحمبل السيارات"
        Me.CarTableWCheckEdit.Size = New System.Drawing.Size(131, 19)
        Me.CarTableWCheckEdit.TabIndex = 22
        '
        'NewOrderWCheckEdit
        '
        Me.NewOrderWCheckEdit.Location = New System.Drawing.Point(283, 70)
        Me.NewOrderWCheckEdit.Name = "NewOrderWCheckEdit"
        Me.NewOrderWCheckEdit.Properties.Caption = "طلبه جديده"
        Me.NewOrderWCheckEdit.Size = New System.Drawing.Size(80, 19)
        Me.NewOrderWCheckEdit.TabIndex = 24
        '
        'OrdersWCheckEdit
        '
        Me.OrdersWCheckEdit.Location = New System.Drawing.Point(283, 110)
        Me.OrdersWCheckEdit.Name = "OrdersWCheckEdit"
        Me.OrdersWCheckEdit.Properties.Caption = "الطلبيات"
        Me.OrdersWCheckEdit.Size = New System.Drawing.Size(80, 19)
        Me.OrdersWCheckEdit.TabIndex = 26
        '
        'DoneOrdersWCheckEdit
        '
        Me.DoneOrdersWCheckEdit.Location = New System.Drawing.Point(248, 154)
        Me.DoneOrdersWCheckEdit.Name = "DoneOrdersWCheckEdit"
        Me.DoneOrdersWCheckEdit.Properties.Caption = "الطلبيات المستلمه"
        Me.DoneOrdersWCheckEdit.Size = New System.Drawing.Size(115, 19)
        Me.DoneOrdersWCheckEdit.TabIndex = 34
        '
        'NavigationPage1
        '
        Me.NavigationPage1.Caption = "الانتاج"
        Me.NavigationPage1.Controls.Add(Me.ProductionTableWCheckEdit)
        Me.NavigationPage1.Name = "NavigationPage1"
        Me.NavigationPage1.Size = New System.Drawing.Size(405, 207)
        '
        'ProductionTableWCheckEdit
        '
        Me.ProductionTableWCheckEdit.Location = New System.Drawing.Point(217, 20)
        Me.ProductionTableWCheckEdit.Name = "ProductionTableWCheckEdit"
        Me.ProductionTableWCheckEdit.Properties.Caption = "جدول الانتاج اليومي"
        Me.ProductionTableWCheckEdit.Size = New System.Drawing.Size(154, 19)
        Me.ProductionTableWCheckEdit.TabIndex = 38
        '
        'NavigationPage2
        '
        Me.NavigationPage2.Caption = "المستخدمين"
        Me.NavigationPage2.Controls.Add(Me.UserWCheckEdit)
        Me.NavigationPage2.Controls.Add(Me.UserHSTWCheckEdit)
        Me.NavigationPage2.Name = "NavigationPage2"
        Me.NavigationPage2.Size = New System.Drawing.Size(405, 207)
        '
        'UserWCheckEdit
        '
        Me.UserWCheckEdit.Location = New System.Drawing.Point(269, 20)
        Me.UserWCheckEdit.Name = "UserWCheckEdit"
        Me.UserWCheckEdit.Properties.Caption = "المستخدمين"
        Me.UserWCheckEdit.Size = New System.Drawing.Size(100, 19)
        Me.UserWCheckEdit.TabIndex = 42
        '
        'UserHSTWCheckEdit
        '
        Me.UserHSTWCheckEdit.Location = New System.Drawing.Point(237, 47)
        Me.UserHSTWCheckEdit.Name = "UserHSTWCheckEdit"
        Me.UserHSTWCheckEdit.Properties.Caption = "سجل المستخدمين"
        Me.UserHSTWCheckEdit.Size = New System.Drawing.Size(132, 19)
        Me.UserHSTWCheckEdit.TabIndex = 44
        '
        'SettingMWCheckEdit
        '
        Me.SettingMWCheckEdit.Location = New System.Drawing.Point(517, 21)
        Me.SettingMWCheckEdit.Name = "SettingMWCheckEdit"
        Me.SettingMWCheckEdit.Properties.Caption = "CheckEdit1"
        Me.SettingMWCheckEdit.Size = New System.Drawing.Size(21, 19)
        Me.SettingMWCheckEdit.TabIndex = 8
        '
        'OrdersMWCheckEdit
        '
        Me.OrdersMWCheckEdit.Location = New System.Drawing.Point(521, 49)
        Me.OrdersMWCheckEdit.Name = "OrdersMWCheckEdit"
        Me.OrdersMWCheckEdit.Properties.Caption = "CheckEdit1"
        Me.OrdersMWCheckEdit.Size = New System.Drawing.Size(17, 19)
        Me.OrdersMWCheckEdit.TabIndex = 20
        '
        'ProductionMWCheckEdit
        '
        Me.ProductionMWCheckEdit.Location = New System.Drawing.Point(521, 76)
        Me.ProductionMWCheckEdit.Name = "ProductionMWCheckEdit"
        Me.ProductionMWCheckEdit.Properties.Caption = "CheckEdit1"
        Me.ProductionMWCheckEdit.Size = New System.Drawing.Size(17, 19)
        Me.ProductionMWCheckEdit.TabIndex = 36
        '
        'UserMWCheckEdit
        '
        Me.UserMWCheckEdit.Location = New System.Drawing.Point(521, 103)
        Me.UserMWCheckEdit.Name = "UserMWCheckEdit"
        Me.UserMWCheckEdit.Properties.Caption = "CheckEdit1"
        Me.UserMWCheckEdit.Size = New System.Drawing.Size(17, 19)
        Me.UserMWCheckEdit.TabIndex = 40
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(22, 12)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.Size = New System.Drawing.Size(401, 161)
        Me.DataGridView1.TabIndex = 41
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SimpleButton2.Location = New System.Drawing.Point(429, 130)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(57, 25)
        Me.SimpleButton2.TabIndex = 42
        Me.SimpleButton2.Text = "حذف"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SimpleButton1.Location = New System.Drawing.Point(429, 100)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(127, 25)
        Me.SimpleButton1.TabIndex = 42
        Me.SimpleButton1.Text = "حفظ"
        '
        'SimpleButton3
        '
        Me.SimpleButton3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SimpleButton3.Location = New System.Drawing.Point(499, 130)
        Me.SimpleButton3.Name = "SimpleButton3"
        Me.SimpleButton3.Size = New System.Drawing.Size(57, 25)
        Me.SimpleButton3.TabIndex = 42
        Me.SimpleButton3.Text = "جديد"
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox2.Controls.Add(Me.SettingMWCheckEdit)
        Me.GroupBox2.Controls.Add(Me.OrdersMWCheckEdit)
        Me.GroupBox2.Controls.Add(Me.ProductionMWCheckEdit)
        Me.GroupBox2.Controls.Add(Me.UserMWCheckEdit)
        Me.GroupBox2.Controls.Add(Me.NavigationPane1)
        Me.GroupBox2.Location = New System.Drawing.Point(10, 162)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(549, 299)
        Me.GroupBox2.TabIndex = 43
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "الصلاحيات"
        '
        'UserNameTextEdit
        '
        Me.UserNameTextEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.UserNameTextEdit.Location = New System.Drawing.Point(429, 30)
        Me.UserNameTextEdit.Name = "UserNameTextEdit"
        Me.UserNameTextEdit.Size = New System.Drawing.Size(130, 20)
        Me.UserNameTextEdit.TabIndex = 44
        '
        'UserPassWordTextEdit
        '
        Me.UserPassWordTextEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.UserPassWordTextEdit.Location = New System.Drawing.Point(429, 72)
        Me.UserPassWordTextEdit.Name = "UserPassWordTextEdit"
        Me.UserPassWordTextEdit.Size = New System.Drawing.Size(130, 20)
        Me.UserPassWordTextEdit.TabIndex = 45
        Me.UserPassWordTextEdit.UseSystemPasswordChar = True
        '
        'Users
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(596, 465)
        Me.Controls.Add(Me.UserPassWordTextEdit)
        Me.Controls.Add(Me.UserNameTextEdit)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.SimpleButton3)
        Me.Controls.Add(Me.SimpleButton1)
        Me.Controls.Add(Me.SimpleButton2)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(UserNameLabel)
        Me.Controls.Add(UserPassWordLabel)
        Me.Font = New System.Drawing.Font("Times New Roman", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Name = "Users"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Text = "آداره المستخدمين"
        CType(Me.NavigationPane1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.NavigationPane1.ResumeLayout(False)
        Me.NavigationPage3.ResumeLayout(False)
        CType(Me.ProductionLineWCheckEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProductWCheckEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CarsWCheckEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DriverWCheckEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupWCheckEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.NavigationPage4.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.OrdersWPrintCheckEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.OrdersWDeleteCheckEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.OrdersWEditCheckEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CarTableWCheckEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewOrderWCheckEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.OrdersWCheckEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DoneOrdersWCheckEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.NavigationPage1.ResumeLayout(False)
        CType(Me.ProductionTableWCheckEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.NavigationPage2.ResumeLayout(False)
        CType(Me.UserWCheckEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UserHSTWCheckEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SettingMWCheckEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.OrdersMWCheckEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProductionMWCheckEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UserMWCheckEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents NavigationPane1 As DevExpress.XtraBars.Navigation.NavigationPane
    Friend WithEvents NavigationPage3 As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents NavigationPage4 As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents NavigationPage1 As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents NavigationPage2 As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents ProductionLineWCheckEdit As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents ProductWCheckEdit As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CarsWCheckEdit As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents DriverWCheckEdit As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents GroupWCheckEdit As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CarTableWCheckEdit As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents NewOrderWCheckEdit As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents OrdersWCheckEdit As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents OrdersWEditCheckEdit As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents OrdersWDeleteCheckEdit As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents OrdersWPrintCheckEdit As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents DoneOrdersWCheckEdit As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents ProductionTableWCheckEdit As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents UserWCheckEdit As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents UserHSTWCheckEdit As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents SettingMWCheckEdit As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents OrdersMWCheckEdit As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents ProductionMWCheckEdit As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents UserMWCheckEdit As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton3 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents UserNameTextEdit As TextBox
    Friend WithEvents UserPassWordTextEdit As TextBox
End Class
