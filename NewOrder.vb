﻿Imports System.Data.SqlClient
Public Class NewOrder
    Public TMPOrederDA As New SqlDataAdapter
    Public TMPOrederDT As New DataTable
    Dim orderInTon As Double
    Dim max As Date

    Private Sub NewOrder_Load(sender As Object, e As EventArgs) Handles MyBase.Load


        Me.Opacity = 100
        LOAD_Groups()
        GroupCompoBox.DataSource = GroupDT
        GroupCompoBox.DisplayMember = "GroupName"

        OrderQuntityTextBox.Text = ""

        Load_cars()
        Load_Driver()
        Load_Product()
        Code_Order()
        OrderIDTextBox.Text = NewOrderID
        DelivaryCarComboBox.DataSource = CarDT
        DelivaryCarComboBox.DisplayMember = "PlatNB"
        DelivaryCarComboBox.ValueMember = "CarDriver"
        DelivaryDriverComboBox.DataSource = DriverDT
        DelivaryDriverComboBox.DisplayMember = "DriverName"
        DelivaryDriverComboBox.ValueMember = "DriverName"
        OrderProductComboBox.DataSource = ProductDT
        OrderProductComboBox.DisplayMember = "ProductName"
        OrderProductComboBox.ValueMember = "Productionline"

        TMPOrederDA = New SqlDataAdapter("Select OrderProduct,Orderquntity,ProductLine,OrderInton,DeliveryDate from Orders Where OrderID = '" & OrderIDTextBox.Text & "' ", SQlManeCon)
        TMPOrederDT.Clear()
        TMPOrederDA.Fill(TMPOrederDT)
        DataGridView1.DataSource = TMPOrederDT
        DataGridView1.Columns("OrderProduct").HeaderText = "الصنف"
        DataGridView1.Columns("Orderquntity").HeaderText = "الكميه"
        DataGridView1.Columns("ProductLine").Visible = False
        DataGridView1.Columns("OrderInton").Visible = False
        DataGridView1.Columns("DeliveryDate").Visible = False

        OrderDateDateTimePicker.Value = Now
        Load_clients()
        OrderClintComboBox.DataSource = SQLDT
        OrderClintComboBox.DisplayMember = "CusNameAr"
        OrderClintComboBox.ValueMember = "CustomerId"

        'Dim autocomplet As New AutoCompleteStringCollection
        'For i As Integer = 0 To SQLDT.Rows.Count - 1
        '    autocomplet.Add(SQLDT.Rows(i)("CusNameAr"))
        'Next
        'OrderClintComboBox.AutoCompleteCustomSource = autocomplet
        'OrderClintComboBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        'OrderClintComboBox.AutoCompleteSource = AutoCompleteSource.CustomSource
        OrderClintComboBox.Text = "عميل عام"
        OrderProductComboBox_SelectedIndexChanged(sender, e)
    End Sub

    Private Sub DelivaryCarComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DelivaryCarComboBox.SelectedIndexChanged
        Try


            Try
                DelivaryDriverComboBox.Text = DelivaryCarComboBox.SelectedValue
            Catch ex As Exception

            End Try

            Dim da As New SqlDataAdapter
            Dim dt As New DataTable

            da = New SqlDataAdapter("Select * from orders where  deliverydate = '" & DeliveryDateDateTimePicker.Value.Year & DeliveryDateDateTimePicker.Value.Month & DeliveryDateDateTimePicker.Value.Day & "' and deliverycar = N'" & DelivaryCarComboBox.Text & "'  ", SQlManeCon)
            dt.Clear()
            da.Fill(dt)
            If dt.Rows.Count > 0 Then
                Try
                    Label2.Text = dt.Compute("SUM(OrderInTon)", String.Empty)
                Catch ex As Exception
                    Label2.Text = 0
                End Try
            Else
                Label2.Text = 0
            End If


            Dim MaxCCDA As New SqlDataAdapter
            Dim MaxCCDT As New DataTable

            Try
                MaxCCDA = New SqlDataAdapter("Select * from Cars where PlatNB = N'" & DelivaryCarComboBox.Text & "' ", SQlManeCon)
                MaxCCDT.Clear()
                MaxCCDA.Fill(MaxCCDT)
                Label9.Text = MaxCCDT.Rows(0).Item("CarCapacity")

            Catch ex As Exception

            End Try

        Catch ex As Exception
            log_error(Me.Text, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message)
            '   MsgBox("لقد حدث خطأً ما " & ex.Message & "  تم حفظ الخطأ في ملف سجل الاخطاء الموجود في   " & Application.StartupPath & "/Log.txt  ")

        End Try

    End Sub

    Private Sub OrderProductComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles OrderProductComboBox.SelectedIndexChanged
        If OrderProductComboBox.Text.Trim <> Nothing Then
            ProductLineTextBox.Text = OrderProductComboBox.SelectedValue.ToString
        End If
    End Sub
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim ProDate As Date = Now.AddDays(1)
        Dim CHKlinecapacityDA As New SqlDataAdapter
        Dim CHKLineCapacityDT As New DataTable
        Dim linecapDA As New SqlDataAdapter("Select * from ProductionLine where Linename = N'" & ProductLineTextBox.Text & "' ", SQlManeCon)
        Dim linecapDT As New DataTable
        linecapDT.Clear()
        linecapDA.Fill(linecapDT)
        Dim maxcap As Integer = linecapDT.Rows(0).Item("LineCapacity")
        Dim islineOk As Boolean = False
        Do
            CHKlinecapacityDA = New SqlDataAdapter("Select * from Orders where  ProductionDate = '" & ProDate.Year & "-" & ProDate.Month & "-" & ProDate.Day & "' and productLine = N'" & ProductLineTextBox.Text & "'  ", SQlManeCon)
            CHKLineCapacityDT.Clear()
            CHKlinecapacityDA.Fill(CHKLineCapacityDT)
            If CHKLineCapacityDT.Rows.Count = 0 Then
                islineOk = True
            ElseIf CHKLineCapacityDT.Rows.Count > 0 Then
                Dim sum As Double

                sum = Convert.ToDouble(CHKLineCapacityDT.Compute("SUM(OrderInTon)", String.Empty))
                If sum + TextBox1.Text <= maxcap Then
                    islineOk = True
                Else
                    islineOk = False
                    ProDate = ProDate.AddDays(1)
                End If
            End If
        Loop Until islineOk = True

        Dim da As New SqlDataAdapter
        Dim dt As New DataTable
        da = New SqlDataAdapter("Select * from Product Where ProductName =N'" & OrderProductComboBox.Text & "' ", SQlManeCon)
        dt.Clear()
        da.Fill(dt)

        TMPOrederDT.Rows.Add()

        Dim last As Integer = TMPOrederDT.Rows.Count - 1
        '  TMPOrederDT.Rows(last).Item("OrderID") = OrderIDTextBox.Text
        '   TMPOrederDT.Rows(last).Item("OrderClint") = OrderClintComboBox.Text
        TMPOrederDT.Rows(last).Item("OrderProduct") = OrderProductComboBox.Text
        '  TMPOrederDT.Rows(last).Item("OrderProductID") = dt.Rows(0).Item("ProductID")
        TMPOrederDT.Rows(last).Item("Orderquntity") = OrderQuntityTextBox.Text
        TMPOrederDT.Rows(last).Item("ProductLine") = ProductLineTextBox.Text
        '    TMPOrederDT.Rows(last).Item("DeliveryCar") = DelivaryCarComboBox.Text
        ' TMPOrederDT.Rows(last).Item("DeliveryDriver") = DelivaryDriverComboBox.Text
        '  TMPOrederDT.Rows(last).Item("OrderDate") = OrderDateDateTimePicker.Value
        TMPOrederDT.Rows(last).Item("DeliveryDate") = ProDate.AddDays(1)
        '  TMPOrederDT.Rows(last).Item("ProductionDate") = ProDate
        '  TMPOrederDT.Rows(last).Item("IsDeliverd") = CheckState.Unchecked
        TMPOrederDT.Rows(last).Item("OrderInton") = TextBox1.Text

        Label4.Text = TMPOrederDT.Compute("SUM(OrderInton)", String.Empty)

        '  GroupBox2.Enabled = True

        Button2_Click(sender, e)

    End Sub

    Private Sub OrderQuntityTextBox_TextChanged(sender As Object, e As EventArgs) Handles OrderQuntityTextBox.TextChanged
        If OrderQuntityTextBox.Text.Trim <> Nothing Then
            Dim da As New SqlDataAdapter
            Dim dt As New DataTable
            da = New SqlDataAdapter("Select ProductUnitsInTon from  Product Where ProductName =N'" & OrderProductComboBox.Text & "' ", SQlManeCon)
            dt.Clear()
            da.Fill(dt)
            orderInTon = (OrderQuntityTextBox.Text / dt.Rows(0).Item("ProductUnitsInTon"))
            TextBox1.Text = orderInTon
        End If
    End Sub

    Private Sub TextBox2_TextChanged(sender As Object, e As EventArgs) Handles TextBox2.TextChanged
        If TextBox2.Text = "admin1122" Then
            OrderDateDateTimePicker.Enabled = True
            TextBox2.Text = ""

        ElseIf TextBox2.Text = "close" Then
            OrderDateDateTimePicker.Enabled = False
            TextBox2.Visible = False

        End If
    End Sub

    Private Sub admin_Click(sender As Object, e As EventArgs)
        TextBox2.Visible = True

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs)
        max = TMPOrederDT.Compute("Max(DeliveryDate)", String.Empty)
        DeliveryDateDateTimePicker.Value = max
        ProductionDateDateTimePicker.Enabled = True
        DeliveryDateDateTimePicker.Enabled = True
        '  GroupBox1.Enabled = True
        Button4.Enabled = True
        DelivaryCarComboBox_SelectedIndexChanged(sender, e)
    End Sub

    Private Sub DeliveryDateDateTimePicker_ValueChanged(sender As Object, e As EventArgs) Handles DeliveryDateDateTimePicker.ValueChanged
        Try


            If DeliveryDateDateTimePicker.Value < max And DeliveryDateDateTimePicker.Value.Date <> max.Date Then
                MsgBox("لقد تخطيت اقصي حد انتاج للمصنع في هذا اليوم " & max.Date & " ")
            End If
            ProductionDateDateTimePicker.MaxDate = DeliveryDateDateTimePicker.Value
            Try
                ProductionDateDateTimePicker.Value = DeliveryDateDateTimePicker.Value.AddDays(-1)
            Catch ex As Exception

            End Try
        Catch ex As Exception
            log_error(Me.Text, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message)
            '  MsgBox("لقد حدث خطأً ما " & ex.Message & "  تم حفظ الخطأ في ملف سجل الاخطاء الموجود في   " & Application.StartupPath & "/Log.txt  ")

        End Try


    End Sub



    Private Sub Label2_TextChanged(sender As Object, e As EventArgs) Handles Label2.TextChanged, Label4.TextChanged, Label9.TextChanged, Label6.TextChanged

        Try
            Dim load1 As Double
            Dim load2 As Double
            Try
                load1 = Label2.Text
                load2 = Label4.Text
                Label6.Text = load1 + load2

                Dim lb6 As Double
                Dim lb9 As Double

                Try

                    lb6 = Label6.Text
                    lb9 = Label9.Text

                    ArcScaleRangeBarComponent1.Value = (lb6 / lb9) * 100

                    If ArcScaleRangeBarComponent1.Value > 100 Then
                        StateImageIndicatorComponent1.StateIndex = 1
                        GaugeControl1.ColorScheme.Color = Color.Red

                    Else
                        StateImageIndicatorComponent1.StateIndex = 0
                        GaugeControl1.ColorScheme.Color = Color.Green

                    End If
                Catch ex As Exception

                End Try




            Catch ex As Exception
            End Try

        Catch ex As Exception
            log_error(Me.Text, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message)
            ' MsgBox("لقد حدث خطأً ما " & ex.Message & "  تم حفظ الخطء في ملف سجل الاخطاء الموجود في   " & Application.StartupPath & "/Log.txt  ")

        End Try




    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click



        Try
            Dim OHeadDA As New SqlDataAdapter("Select * from OrdersHead ", SQlManeCon)
            Dim OHeadDT As New DataTable
            OHeadDT.Clear()
            OHeadDA.Fill(OHeadDT)
            OHeadDT.Rows.Add()
            Dim H As Integer = OHeadDT.Rows.Count - 1
            OHeadDT.Rows(H).Item(0) = OrderIDTextBox.Text
            Dim saveH As New SqlCommandBuilder(OHeadDA)
            OHeadDA.Update(OHeadDT)
            OHeadDT.AcceptChanges()


            Dim da As New SqlDataAdapter
            Dim dt As New DataTable


            For i As Integer = DataGridView1.Rows.Count - 1 To 0 Step -1
                da = New SqlDataAdapter("Select * from Product Where ProductName =N'" & DataGridView1.Rows(i).DataBoundItem("OrderProduct") & "' ", SQlManeCon)
                dt.Clear()
                da.Fill(dt)

                Load_Orders()
                OrderDT.Rows.Add()
                Dim last As Integer = OrderDT.Rows.Count - 1
                OrderDT.Rows(last).Item("OrderID") = OrderIDTextBox.Text
                OrderDT.Rows(last).Item("OrderClint") = OrderClintComboBox.Text
                OrderDT.Rows(last).Item("OrderProduct") = DataGridView1.Rows(i).DataBoundItem("OrderProduct")
                OrderDT.Rows(last).Item("OrderProductID") = dt.Rows(0).Item("ProductID")
                OrderDT.Rows(last).Item("OrderQuntity") = DataGridView1.Rows(i).DataBoundItem("OrderQuntity")
                OrderDT.Rows(last).Item("ProductLine") = DataGridView1.Rows(i).DataBoundItem("ProductLine")
                'OrderDT.Rows(last).Item("ProductLine") = DataGridView1.Rows(i).DataBoundItem("OrderProductID")
                OrderDT.Rows(last).Item("DeliveryCar") = DelivaryCarComboBox.Text
                OrderDT.Rows(last).Item("DeliveryDriver") = DelivaryDriverComboBox.Text
                OrderDT.Rows(last).Item("OrderDate") = OrderDateDateTimePicker.Value
                OrderDT.Rows(last).Item("DeliveryDate") = DeliveryDateDateTimePicker.Value.Date
                OrderDT.Rows(last).Item("ProductionDate") = ProductionDateDateTimePicker.Value.Date
                OrderDT.Rows(last).Item("IsDeliverd") = CheckState.Unchecked
                OrderDT.Rows(last).Item("OrderInton") = DataGridView1.Rows(i).DataBoundItem("OrderInton")
                OrderDT.Rows(last).Item("CusCode") = OrderClintComboBox.SelectedValue
                OrderDT.Rows(last).Item("OrderGroup") = GroupCompoBox.Text

                Dim save As New SqlCommandBuilder(OrderDA)
                OrderDA.Update(OrderDT)
                OrderDT.AcceptChanges()
            Next
            Log_UserAct(Me.Text, "اضافه", OrderIDTextBox.Text, OrderClintComboBox.Text)

            NewOrder_Load(sender, e)
            MsgBox("تم حفظ الطلبيه")
            Me.Opacity = 100

        Catch ex As Exception


        End Try
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        NewOrder_Load(sender, e)

    End Sub

    Private Sub OrderQuntityTextBox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles OrderQuntityTextBox.KeyPress



        Try
            If Asc(e.KeyChar) <> 13 AndAlso Asc(e.KeyChar) <> 8 AndAlso Not IsNumeric(e.KeyChar) Then
                e.Handled = True
                'MsgBox("هذا الحقل يقبل ارقام فقط")
            Else

            End If


        Catch ex As Exception
            log_error(Me.Text, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message)
            'MsgBox("لقد حدث خطأً ما " & ex.Message & "  تم حفظ الخطأ في ملف سجل الاخطاء الموجود في   " & Application.StartupPath & "/Log.txt  ")

        End Try
    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        Try
            Dim pos As Integer = BindingContext(TMPOrederDT).Position
            TMPOrederDT.Rows(pos).Delete()
            Label4.Text = TMPOrederDT.Compute("SUM(OrderInton)", String.Empty)

        Catch ex As Exception

        End Try

    End Sub

    Private Sub OrderProductComboBox_TextChanged(sender As Object, e As EventArgs) Handles OrderProductComboBox.TextChanged

    End Sub
End Class