﻿Imports System.Data.SqlClient
Imports DevExpress.Utils
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks

Public Class ProductionTable
    Dim ptDA As New SqlDataAdapter("Select * from TimpProTable", SQlManeCon)
    Dim ptDT As New DataTable

    Dim timpproda As New SqlDataAdapter
    Dim timpprodt As New DataTable


    Private Sub ProductionTable_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Me.Visible = False
        Try


            DateTimePicker1.Value = Today
            Load_PLine()
            ComboBox1.DataSource = PLineDT
            ComboBox1.DisplayMember = "LineName"


            ptDT.Clear()
            ptDA.Fill(ptDT)


            GridControl1.BeginUpdate()
            Try
                GridView1.Columns.Clear()
                GridControl1.DataSource = Nothing
                GridControl1.DataSource = ptDT

            Finally
                GridControl1.EndUpdate()

            End Try
            GridView1.Columns("Product").Caption = "الصنف"
            GridView1.Columns("Quntity").Caption = "الكميه"


        Catch ex As Exception
            log_error(Me.Text, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message)
            'MsgBox("لقد حدث خطأً ما " & ex.Message & "  تم حفظ الخطأ في ملف سجل الاخطاء الموجود في   " & Application.StartupPath & "/Log.txt  ")

        End Try
    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        Try


            Load_Product()
            ptDT.Clear()
            For i As Integer = ProductDT.Rows.Count - 1 To 0 Step -1
                timpproda = New SqlDataAdapter("Select * from Orders where ProductLine = N'" & ComboBox1.Text & "' and OrderProduct = N'" & ProductDT.Rows(i).Item("ProductName") & "' and ProductionDate = '" & DateTimePicker1.Value.Year & "-" & DateTimePicker1.Value.Month & "-" & DateTimePicker1.Value.Day & "'  and IsDeliverd ='False' ", SQlManeCon)
                timpprodt.Clear()
                timpproda.Fill(timpprodt)

                If timpprodt.Rows.Count > 0 Then
                    ptDT.Rows.Add()
                    Dim last As Integer = ptDT.Rows.Count - 1
                    ptDT.Rows(last).Item(0) = ProductDT.Rows(i).Item("ProductName")
                    ptDT.Rows(last).Item(1) = timpprodt.Compute("Sum(OrderQuntity)", String.Empty)
                End If
            Next

        Catch ex As Exception

        End Try




        Dim da As New SqlDataAdapter("Select * from Orders where ProductLine = N'" & ComboBox1.Text & "'  and ProductionDate = '" & DateTimePicker1.Value.Year & "-" & DateTimePicker1.Value.Month & "-" & DateTimePicker1.Value.Day & "' and IsDeliverd ='False' ", SQlManeCon)

        Dim dt As New DataTable
        dt.Clear()
        da.Fill(dt)

        '======================
        GridControl2.BeginUpdate()
        'Try
        GridView2.Columns.Clear()
        GridControl2.DataSource = Nothing
        GridControl2.DataSource = dt
        'Finally
        GridControl2.EndUpdate()
        'End Try

        GridView2.Columns("IsDeliverd").Visible = False
        GridView2.Columns("ProductLine").Visible = False
        GridView2.Columns("ProductionDate").Visible = False
        GridView2.Columns("OrderInton").Visible = False
        GridView2.Columns("DeliveryDate").Caption = "موعد الاستلام"
        GridView2.Columns("DeliveryDate").DisplayFormat.FormatType = FormatType.Custom
        GridView2.Columns("DeliveryDate").DisplayFormat.FormatString = "yyyy-MM-dd"

        GridView2.Columns("OrderID").Visible = False
        GridView2.Columns("OrderClint").Caption = "العميل"
        GridView2.Columns("OrderClint").Width = 120
        GridView2.Columns("OrderProduct").Caption = "الصنف"
        GridView2.Columns("OrderQuntity").Caption = "الكميه"
        GridView2.Columns("OrderQuntity").Width = 54
        GridView2.Columns("CusCode").Visible = False
        GridView2.Columns("Id").Visible = False
        GridView2.Columns("InvoiceCode").Visible = False

        GridView2.Columns("DeliveryCar").Caption = "السياره"
        GridView2.Columns("DeliveryDriver").Caption = "السائق"
        GridView2.Columns("OrderDate").Caption = "تاريخ الطلب"
        GridView2.Columns("OrderGroup").Caption = "الفئه"
        GridView2.Columns("OrderDate").DisplayFormat.FormatType = FormatType.Custom
        GridView2.Columns("OrderDate").DisplayFormat.FormatString = "yyyy-MM-dd hh:mm"

        GridView2.Columns(0).OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True
        GridView2.Columns(1).OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True
        GridView2.Columns(2).OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False
        GridView2.Columns(3).OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False
        GridView2.Columns(4).OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False
        GridView2.Columns(7).OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False
        GridView2.Columns(8).OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False
        GridView2.Columns(9).OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False
        GridView2.Columns(10).OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False

    End Sub

    Private Sub SimpleButton2_Click(sender As Object, e As EventArgs) Handles SimpleButton2.Click
        '    Try
        '        GridControl1.ShowPrintPreview()
        '    Catch ex As Exception
        '        log_error(Me.Text, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message)
        '        MsgBox(ex.Message & "log is saved in " & Application.StartupPath & "Log.txt")
        '    End Try


        Try
            Dim composLink As CompositeLink = New CompositeLink(New PrintingSystem())
            AddHandler composLink.CreateMarginalHeaderArea, AddressOf composLink_CreateMarginalHeaderArea
            Dim pcLink1 As PrintableComponentLink = New PrintableComponentLink()
            Dim pcLink2 As PrintableComponentLink = New PrintableComponentLink()
            Dim linkMainReport As Link = New Link()
            AddHandler linkMainReport.CreateDetailArea, AddressOf linkMainReport_CreateDetailArea
            Dim linkGrid1Report As Link = New Link()
            AddHandler linkGrid1Report.CreateDetailArea, AddressOf linkGrid1Report_CreateDetailArea
            Dim linkGrid2Report As Link = New Link()
            AddHandler linkGrid2Report.CreateDetailArea, AddressOf linkGrid2Report_CreateDetailArea

            ' Assign the controls to the printing links.
            pcLink1.Component = Me.GridControl1
            pcLink2.Component = Me.GridControl2


            ' Populate the collection of links in the composite link.
            ' The order of operations corresponds to the document structure.
            composLink.Links.Add(linkGrid1Report)
            composLink.Links.Add(pcLink1)
            composLink.Links.Add(linkMainReport)
            composLink.Links.Add(linkGrid2Report)
            composLink.Links.Add(pcLink2)
            'composLink.PaperKind = Printing.PaperKind.A5
            composLink.PaperKind = My.Settings.PaperSize
            composLink.Landscape = My.Settings.LandScape
            composLink.Margins.Left = My.Settings.MLeft
            composLink.Margins.Right = My.Settings.MRight
            composLink.Margins.Bottom = My.Settings.MBottom
            composLink.Margins.Top = My.Settings.MTop


            ' Create the report and show the preview window.
            composLink.ShowPreviewDialog()



        Catch ex As Exception
            log_error(Me.Text, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message)
            'MsgBox(ex.Message & "log is saved in " & Application.StartupPath & "Log.txt")
        End Try



    End Sub


    ' Inserts a PageInfoBrick into the top margin to display the time.
    Private Sub composLink_CreateMarginalHeaderArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)
        'e.Graph.DrawPageInfo(PageInfo.DateTime, "{0:hhhh:mmmm:ssss}",
        'Color.Black, New RectangleF(0, 0, 200, 50), BorderSide.None)
    End Sub

    ' Creates a text header for the first grid.
    Private Sub linkGrid1Report_CreateDetailArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)

        Try


            Dim tb As TextBrick = New TextBrick()
            tb.Text = "  جدول انتاج  " & ComboBox1.Text & "  بتاريخ  " & DateTimePicker1.Value.Date
            tb.Font = New Font("Times New Roman", 15, FontStyle.Bold)
            tb.Rect = New RectangleF(0, 0, e.Graph.ClientPageSize.Width, 25)
            tb.BorderWidth = 2
            tb.BackColor = Color.LightGray
            tb.HorzAlignment = DevExpress.Utils.HorzAlignment.Center

            tb.VertAlignment = DevExpress.Utils.VertAlignment.Center

            e.Graph.DrawBrick(tb)
        Catch ex As Exception
            log_error(Me.Text, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message)
            'MsgBox(ex.Message & "log is saved in " & Application.StartupPath & "Log.txt")
        End Try
    End Sub

    '    ' Creates an interval between the grids and fills it with color.
    Private Sub linkMainReport_CreateDetailArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)
        'Dim tb As TextBrick = New TextBrick()
        'tb.Rect = New RectangleF(0, 0, e.Graph.ClientPageSize.Width, 50)
        'tb.BackColor = Color.Gray
        'e.Graph.DrawBrick(tb)
    End Sub

    ' Creates a text header for the second grid.
    Private Sub linkGrid2Report_CreateDetailArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)
        'Dim tb As TextBrick = New TextBrick()
        'tb.Text = "Suppliers"
        'tb.Font = New Font("Arial", 15)
        'tb.Rect = New RectangleF(0, 0, 300, 25)
        'tb.BorderWidth = 0
        'tb.BackColor = Color.Transparent
        'tb.HorzAlignment = DevExpress.Utils.HorzAlignment.Near
        'e.Graph.DrawBrick(tb)
    End Sub


    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox1.SelectedIndexChanged

        Try
            SimpleButton1_Click(sender, e)
        Catch ex As Exception

        End Try


    End Sub

    Private Sub DateTimePicker1_ValueChanged(sender As Object, e As EventArgs) Handles DateTimePicker1.ValueChanged
        Try
            SimpleButton1_Click(sender, e)
        Catch ex As Exception
            log_error(Me.Text, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message)
            'MsgBox("لقد حدث خطأً ما " & ex.Message & "  تم حفظ الخطأ في ملف سجل الاخطاء الموجود في   " & Application.StartupPath & "/Log.txt  ")

        End Try



    End Sub


End Class