﻿Imports System.Data.SqlClient
Imports System.Threading
Public Class Form1
    Dim ProccessedItems As Integer = 0
    Dim ManProLocation As Integer

    Private Sub Monitoradd(line As String)
        Invoke(Sub() RichTextBox1.AppendText(line & Environment.NewLine))


    End Sub
    Private Sub ProgressSub(pos As Integer, Max As Integer, title As String)
        Try
            Dim val As Double = pos / Max * 100
            ProgressBar1.Value = val
            Label2.Text = "Loading  Invoice Number " & title.ToString & "...."
            ProgressBar2.Value = ManProLocation + ProgressBar1.Value / 10
            Label1.Text = ProgressBar2.Value & "%"
        Catch ex As Exception
        End Try
    End Sub

    Private Sub ProgressSub2(pos As Integer, Max As Integer, title As String)
        Try
            Dim val As Double = pos / Max * 100
            ProgressBar3.Value = val

        Catch ex As Exception
        End Try
    End Sub
    Private Sub ProgressMain()
        Try
            ProgressBar2.Value += 10
            Label1.Text = ProgressBar2.Value & "%"
            ManProLocation = ProgressBar2.Value

        Catch ex As Exception
        End Try
    End Sub
    Public Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click

        SimpleButton1.Enabled = False

            BackgroundWorker1.RunWorkerAsync()


    End Sub
    Private Delegate Sub StartMoving()
    Private Sub MoveToDoneOrders()

        Invoke(Sub() SimpleButton1.Enabled = False)
        Invoke(Sub() RichTextBox1.Text = "")
        Invoke(Sub() Label3.Text = "Proccess Started")

        Monitoradd("Getting  Orders ")
        Invoke(Sub() Label1.Text = "Exciuting")
        Dim MyOrdersDA As New SqlDataAdapter("Select * from Orders where  IsDeliverd ='True'  and InvoiceCode is not Null ", SQlManeCon)
        Dim MyOrdersDT As New DataTable
        MyOrdersDT.Clear()
        MyOrdersDA.Fill(MyOrdersDT)
        Monitoradd(MyOrdersDT.Rows.Count & " Record Retreved ")
        ' DoneOrders 
        Monitoradd(" Prepairing To Start Proccessing")
        Dim DoneOrdersDA As New SqlDataAdapter("SELECT * FROM DoneOrders where OrderID = 0  ", SQlManeCon)
        Dim DoneOrdersDT As New DataTable
        DoneOrdersDT.Clear()
        DoneOrdersDA.Fill(DoneOrdersDT)

        Dim DODetailsDA As New SqlDataAdapter("SELECT * FROM DoneOrdersDetails where OrderID = 0  ", SQlManeCon)
        Dim DODetailsDT As New DataTable
        DODetailsDT.Clear()
        DODetailsDA.Fill(DODetailsDT)
        ' To Get Order Numbers In a Grid 
        Invoke(Sub() ProgressMain())
        Dim GetSIDA As New SqlDataAdapter
        Dim GetSIDT As New DataTable
        Dim myrow() As Data.DataRow

        For i As Integer = 1 To MyOrdersDT.Rows.Count
            Invoke(Sub() ProgressSub(i, MyOrdersDT.Rows.Count, MyOrdersDT.Rows(i - 1).Item("OrderID")))
            myrow = DoneOrdersDT.Select(" OrderID = '" & MyOrdersDT.Rows(i - 1).Item("OrderID") & "' ")

            If myrow.Count = 0 Then
                GetSIDA = New SqlDataAdapter("SELECT [SL_InvoiceId],[InvoiceCode],[InvoiceDate],[Net],[PurchaseOrderNo] FROM [ERP].[dbo].[SL_Invoice] where InvoiceCode = '" & MyOrdersDT.Rows(i - 1).Item("InvoiceCode") & "' and PurchaseOrderNo =  '" & MyOrdersDT.Rows(i - 1).Item("OrderID") & "'  ", SQLCon)
                GetSIDT.Clear()
                GetSIDA.Fill(GetSIDT)
                If GetSIDT.Rows.Count > 0 Then
                    DoneOrdersDT.Rows.Add()
                    Dim last As Integer = DoneOrdersDT.Rows.Count - 1
                    DoneOrdersDT.Rows(last).Item("OrderID") = MyOrdersDT.Rows(i - 1).Item("OrderID")
                    DoneOrdersDT.Rows(last).Item("OrderClint") = MyOrdersDT.Rows(i - 1).Item("OrderClint")
                    DoneOrdersDT.Rows(last).Item("DeliveryCar") = MyOrdersDT.Rows(i - 1).Item("DeliveryCar")
                    DoneOrdersDT.Rows(last).Item("DeliveryDriver") = MyOrdersDT.Rows(i - 1).Item("DeliveryDriver")
                    DoneOrdersDT.Rows(last).Item("OrderDate") = MyOrdersDT.Rows(i - 1).Item("OrderDate")
                    DoneOrdersDT.Rows(last).Item("DeliveryDate") = MyOrdersDT.Rows(i - 1).Item("DeliveryDate")
                    DoneOrdersDT.Rows(last).Item("TrueDeliveryDate") = GetSIDT.Rows(0).Item("InvoiceDate")
                    DoneOrdersDT.Rows(last).Item("InvoiceCode") = MyOrdersDT.Rows(i - 1).Item("InvoiceCode")
                    DoneOrdersDT.Rows(last).Item("OrderGroup") = MyOrdersDT.Rows(i - 1).Item("OrderGroup")
                    DoneOrdersDT.Rows(last).Item("OrderPrice") = GetSIDT.Rows(0).Item("Net")
                End If

            End If

        Next
        Monitoradd(" Start Proccessing  ")
        Invoke(Sub() ProgressMain())
        For i As Integer = 1 To DoneOrdersDT.Rows.Count
            Invoke(Sub() ProgressSub(i, DoneOrdersDT.Rows.Count, DoneOrdersDT.Rows(i - 1).Item("InvoiceCode")))
            GetSIDA = New SqlDataAdapter("SELECT  SL_Invoice.InvoiceCode ,SL_InvoiceDetail.ItemId,SL_InvoiceDetail.Qty ,SL_InvoiceDetail.TotalSellPrice 
            FROM ( SL_Invoice inner join SL_InvoiceDetail on SL_Invoice.SL_InvoiceId = SL_InvoiceDetail.SL_InvoiceId)
            where InvoiceCode = '" & DoneOrdersDT.Rows(i - 1).Item("InvoiceCode") & "' and PurchaseOrderNo =  '" & DoneOrdersDT.Rows(i - 1).Item("OrderID") & "'   ", SQLCon)
            GetSIDT.Clear()
            GetSIDA.Fill(GetSIDT)
            Monitoradd(" Proccissing Order Number " & DoneOrdersDT.Rows(i - 1).Item("OrderID"))

            For x As Integer = 1 To GetSIDT.Rows.Count

                Invoke(Sub() ProgressSub2(x, GetSIDT.Rows.Count, GetSIDT.Rows(x - 1).Item("InvoiceCode")))
                Dim ProName As String
                Dim TrueProInTon As Double
                Dim OrderedQu As Integer = 0
                '
                Dim da As New SqlDataAdapter("SELECT ProductName,ProductID , ProductUnitsInTon  FROM  Product where ProductID = '" & GetSIDT.Rows(x - 1).Item("ItemId") & "'  ", SQlManeCon)
                Dim dt As New DataTable
                dt.Clear()
                da.Fill(dt)
                If dt.Rows.Count > 0 Then

                    ProName = dt.Rows(0).Item("ProductName")
                    TrueProInTon = (GetSIDT.Rows(x - 1).Item("Qty") / dt.Rows(0).Item("ProductUnitsInTon"))
                    '
                    da = New SqlDataAdapter("SELECT Id , OrderProductID,OrderQuntity ,OrderID  FROM Orders where OrderProductID = '" & GetSIDT.Rows(x - 1).Item("ItemId") & "' and OrderID = '" & DoneOrdersDT.Rows(i - 1).Item("OrderID") & "'  ", SQlManeCon)
                    dt.Clear()
                    da.Fill(dt)
                    If dt.Rows.Count > 0 Then
                        OrderedQu = dt.Rows(0).Item("OrderQuntity")
                        dt.Rows(0).Delete()
                        Dim savedt As New SqlCommandBuilder(da)
                        da.Update(dt)
                        dt.AcceptChanges()
                    End If
                    DODetailsDT.Rows.Add()
                    Dim lastpos As Integer = DODetailsDT.Rows.Count - 1
                    DODetailsDT.Rows(lastpos).Item("OrderID") = DoneOrdersDT.Rows(i - 1).Item("OrderID")
                    DODetailsDT.Rows(lastpos).Item("OrderProduct") = ProName
                    DODetailsDT.Rows(lastpos).Item("OrderProduct_ID") = GetSIDT.Rows(x - 1).Item("ItemId")
                    DODetailsDT.Rows(lastpos).Item("OrderQuntity") = OrderedQu
                    DODetailsDT.Rows(lastpos).Item("TrueOrderQuntity") = GetSIDT.Rows(x - 1).Item("Qty")
                    DODetailsDT.Rows(lastpos).Item("TrueOrderQuntityInTon") = TrueProInTon
                    DODetailsDT.Rows(lastpos).Item("TotalPrice") = GetSIDT.Rows(x - 1).Item("TotalSellPrice")
                End If
            Next

            Dim Leftda As New SqlDataAdapter("SELECT Id, OrderProductID,OrderQuntity ,OrderID ,OrderProduct ,OrderQuntity , OrderInton  FROM Orders where  OrderID = '" & DoneOrdersDT.Rows(i - 1).Item("OrderID") & "'  ", SQlManeCon)
            Dim Leftdt As New DataTable
            Leftdt.Clear()
            Leftda.Fill(Leftdt)
            If Leftdt.Rows.Count > 0 Then

                For y As Integer = 1 To Leftdt.Rows.Count
                    Invoke(Sub() ProgressSub2(y, Leftdt.Rows.Count, Leftdt.Rows(y - 1).Item("OrderID")))
                    DODetailsDT.Rows.Add()
                    Dim lastpos As Integer = DODetailsDT.Rows.Count - 1
                    DODetailsDT.Rows(lastpos).Item("OrderID") = Leftdt.Rows(y - 1).Item("OrderID")
                    DODetailsDT.Rows(lastpos).Item("OrderProduct") = Leftdt.Rows(y - 1).Item("OrderProduct")
                    DODetailsDT.Rows(lastpos).Item("OrderProduct_ID") = Leftdt.Rows(y - 1).Item("OrderProductID")
                    DODetailsDT.Rows(lastpos).Item("OrderQuntity") = Leftdt.Rows(y - 1).Item("OrderQuntity")
                    DODetailsDT.Rows(lastpos).Item("TrueOrderQuntity") = 0
                    DODetailsDT.Rows(lastpos).Item("TrueOrderQuntityInTon") = Leftdt.Rows(y - 1).Item("OrderInton")
                    DODetailsDT.Rows(lastpos).Item("TotalPrice") = 0
                Next
                For y As Integer = 1 To Leftdt.Rows.Count
                    Leftdt.Rows(y - 1).Delete()
                Next




                Dim savedt As New SqlCommandBuilder(Leftda)
                Leftda.Update(Leftdt)
                Leftdt.AcceptChanges()

            End If

        Next
        Invoke(Sub() ProgressMain())
        Monitoradd("Saving.........")
        Dim SaveDoneOrdersDT As New SqlCommandBuilder(DoneOrdersDA)
        DoneOrdersDA.Update(DoneOrdersDT)
        DoneOrdersDT.AcceptChanges()
        Invoke(Sub() ProgressMain())
        Dim SaveDODetailsDT As New SqlCommandBuilder(DODetailsDA)
        DODetailsDA.Update(DODetailsDT)
        DODetailsDT.AcceptChanges()
        Invoke(Sub() ProgressMain())


        Invoke(Sub() SimpleButton1.Enabled = True)
        Invoke(Sub() Label1.Text = "Finshed")
        Invoke(Sub() ProgressBar2.Value = 100)
        Invoke(Sub() Label2.Text = "Finshed")
        Invoke(Sub() ProccessedItems = DoneOrdersDT.Rows.Count)



    End Sub

    Private Sub BackgroundWorker1_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        MoveToDoneOrders()

    End Sub

    Private Sub BackgroundWorker1_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker1.RunWorkerCompleted
        Label3.Text = "Operation Completed "
        Monitoradd(" Total Proccessed Orders  " & ProccessedItems)
        Me.Close()

    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        SimpleButton1_Click(sender, e)
    End Sub
End Class