﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class HomeScreen
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(HomeScreen))
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.RibbonControl1 = New DevExpress.XtraBars.Ribbon.RibbonControl()
        Me.PrintSettingButton = New DevExpress.XtraBars.BarButtonItem()
        Me.UsersMangemntButton = New DevExpress.XtraBars.BarButtonItem()
        Me.OrdersButton = New DevExpress.XtraBars.BarButtonItem()
        Me.NewOrdersButton = New DevExpress.XtraBars.BarButtonItem()
        Me.CarsTableButton = New DevExpress.XtraBars.BarButtonItem()
        Me.ProLineTableButton = New DevExpress.XtraBars.BarButtonItem()
        Me.DoneOrdersButton = New DevExpress.XtraBars.BarButtonItem()
        Me.ClintTakeButton = New DevExpress.XtraBars.BarButtonItem()
        Me.UserHistoryButton = New DevExpress.XtraBars.BarButtonItem()
        Me.CarsButton = New DevExpress.XtraBars.BarButtonItem()
        Me.DriversButton = New DevExpress.XtraBars.BarButtonItem()
        Me.ProductionLineButton = New DevExpress.XtraBars.BarButtonItem()
        Me.ProductsButton = New DevExpress.XtraBars.BarButtonItem()
        Me.OrdersGroupButton = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem1 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem2 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem3 = New DevExpress.XtraBars.BarButtonItem()
        Me.OrdersPage = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup3 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPageGroup4 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPageGroup5 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPageGroup11 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.SettingPage = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup1 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPageGroup2 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPageGroup12 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.ReportsPage = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup6 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.UsersPage = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup7 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.DataPage = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup8 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPageGroup9 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPageGroup10 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.XtraTabbedMdiManager1 = New DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(Me.components)
        Me.BackUpTimer = New System.Windows.Forms.Timer(Me.components)
        CType(Me.RibbonControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XtraTabbedMdiManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button1.Location = New System.Drawing.Point(43, 522)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(201, 25)
        Me.Button1.TabIndex = 4
        Me.Button1.Text = "التجقق من فواتير جديده "
        Me.Button1.UseVisualStyleBackColor = True
        Me.Button1.Visible = False
        '
        'Timer1
        '
        Me.Timer1.Interval = 6000000
        '
        'TextBox1
        '
        Me.TextBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TextBox1.BackColor = System.Drawing.SystemColors.MenuHighlight
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox1.Location = New System.Drawing.Point(759, 532)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(100, 15)
        Me.TextBox1.TabIndex = 8
        Me.TextBox1.Visible = False
        '
        'RibbonControl1
        '
        Me.RibbonControl1.AutoSaveLayoutToXml = True
        Me.RibbonControl1.AutoSaveLayoutToXmlPath = "HomeRebo"
        Me.RibbonControl1.ExpandCollapseItem.Id = 0
        Me.RibbonControl1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.RibbonControl1.ExpandCollapseItem, Me.PrintSettingButton, Me.UsersMangemntButton, Me.OrdersButton, Me.NewOrdersButton, Me.CarsTableButton, Me.ProLineTableButton, Me.DoneOrdersButton, Me.ClintTakeButton, Me.UserHistoryButton, Me.CarsButton, Me.DriversButton, Me.ProductionLineButton, Me.ProductsButton, Me.OrdersGroupButton, Me.BarButtonItem1, Me.BarButtonItem2, Me.BarButtonItem3})
        Me.RibbonControl1.Location = New System.Drawing.Point(0, 0)
        Me.RibbonControl1.MaxItemId = 19
        Me.RibbonControl1.Name = "RibbonControl1"
        Me.RibbonControl1.OptionsTouch.ShowTouchUISelectorInQAT = True
        Me.RibbonControl1.OptionsTouch.ShowTouchUISelectorVisibilityItemInQATMenu = True
        Me.RibbonControl1.Pages.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPage() {Me.OrdersPage, Me.SettingPage, Me.ReportsPage, Me.UsersPage, Me.DataPage})
        Me.RibbonControl1.PopupShowMode = DevExpress.XtraBars.PopupShowMode.Classic
        Me.RibbonControl1.QuickToolbarItemLinks.Add(Me.NewOrdersButton)
        Me.RibbonControl1.QuickToolbarItemLinks.Add(Me.OrdersButton)
        Me.RibbonControl1.QuickToolbarItemLinks.Add(Me.ClintTakeButton)
        Me.RibbonControl1.QuickToolbarItemLinks.Add(Me.BarButtonItem2)
        Me.RibbonControl1.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.MacOffice
        Me.RibbonControl1.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.[False]
        Me.RibbonControl1.ShowDisplayOptionsMenuButton = DevExpress.Utils.DefaultBoolean.[False]
        Me.RibbonControl1.ShowExpandCollapseButton = DevExpress.Utils.DefaultBoolean.[True]
        Me.RibbonControl1.ShowItemCaptionsInPageHeader = True
        Me.RibbonControl1.ShowItemCaptionsInQAT = True
        Me.RibbonControl1.ShowPageHeadersInFormCaption = DevExpress.Utils.DefaultBoolean.[True]
        Me.RibbonControl1.Size = New System.Drawing.Size(871, 133)
        Me.RibbonControl1.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Below
        '
        'PrintSettingButton
        '
        Me.PrintSettingButton.Caption = "اعدادات الطباعة"
        Me.PrintSettingButton.Id = 2
        Me.PrintSettingButton.Name = "PrintSettingButton"
        '
        'UsersMangemntButton
        '
        Me.UsersMangemntButton.Caption = "اداره المستخدمين"
        Me.UsersMangemntButton.Id = 3
        Me.UsersMangemntButton.Name = "UsersMangemntButton"
        '
        'OrdersButton
        '
        Me.OrdersButton.Caption = "الطلبيات"
        Me.OrdersButton.Id = 4
        Me.OrdersButton.Name = "OrdersButton"
        '
        'NewOrdersButton
        '
        Me.NewOrdersButton.Caption = "طلبيه جديده"
        Me.NewOrdersButton.Id = 5
        Me.NewOrdersButton.Name = "NewOrdersButton"
        '
        'CarsTableButton
        '
        Me.CarsTableButton.Caption = "جدول حموله السيارات "
        Me.CarsTableButton.Id = 6
        Me.CarsTableButton.Name = "CarsTableButton"
        '
        'ProLineTableButton
        '
        Me.ProLineTableButton.Caption = "جدول مخطط الانتاج"
        Me.ProLineTableButton.Id = 7
        Me.ProLineTableButton.Name = "ProLineTableButton"
        '
        'DoneOrdersButton
        '
        Me.DoneOrdersButton.Caption = "الطلبيات المنصرفة"
        Me.DoneOrdersButton.Id = 8
        Me.DoneOrdersButton.Name = "DoneOrdersButton"
        '
        'ClintTakeButton
        '
        Me.ClintTakeButton.Caption = "مسحوبات العميل خلال فتره"
        Me.ClintTakeButton.Id = 9
        Me.ClintTakeButton.Name = "ClintTakeButton"
        '
        'UserHistoryButton
        '
        Me.UserHistoryButton.Caption = "سجل المستخدمين"
        Me.UserHistoryButton.Id = 10
        Me.UserHistoryButton.Name = "UserHistoryButton"
        '
        'CarsButton
        '
        Me.CarsButton.Caption = "السيارات"
        Me.CarsButton.Id = 11
        Me.CarsButton.Name = "CarsButton"
        '
        'DriversButton
        '
        Me.DriversButton.Caption = "السائقون"
        Me.DriversButton.Id = 12
        Me.DriversButton.Name = "DriversButton"
        '
        'ProductionLineButton
        '
        Me.ProductionLineButton.Caption = "خطوط الانتاج"
        Me.ProductionLineButton.Id = 13
        Me.ProductionLineButton.Name = "ProductionLineButton"
        '
        'ProductsButton
        '
        Me.ProductsButton.Caption = "الاصناف"
        Me.ProductsButton.Id = 14
        Me.ProductsButton.Name = "ProductsButton"
        '
        'OrdersGroupButton
        '
        Me.OrdersGroupButton.Caption = "فئات الطلبيات"
        Me.OrdersGroupButton.Id = 15
        Me.OrdersGroupButton.Name = "OrdersGroupButton"
        '
        'BarButtonItem1
        '
        Me.BarButtonItem1.Caption = "BarButtonItem1"
        Me.BarButtonItem1.Id = 16
        Me.BarButtonItem1.Name = "BarButtonItem1"
        '
        'BarButtonItem2
        '
        Me.BarButtonItem2.Caption = "فاتوره مبيعات جديده "
        Me.BarButtonItem2.Id = 17
        Me.BarButtonItem2.Name = "BarButtonItem2"
        '
        'BarButtonItem3
        '
        Me.BarButtonItem3.Caption = "اعدادات النسخ الاحتياطي"
        Me.BarButtonItem3.Id = 18
        Me.BarButtonItem3.Name = "BarButtonItem3"
        '
        'OrdersPage
        '
        Me.OrdersPage.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup3, Me.RibbonPageGroup4, Me.RibbonPageGroup5, Me.RibbonPageGroup11})
        Me.OrdersPage.Name = "OrdersPage"
        Me.OrdersPage.Text = "الطلبيات"
        '
        'RibbonPageGroup3
        '
        Me.RibbonPageGroup3.ItemLinks.Add(Me.OrdersButton)
        Me.RibbonPageGroup3.ItemLinks.Add(Me.NewOrdersButton)
        Me.RibbonPageGroup3.Name = "RibbonPageGroup3"
        Me.RibbonPageGroup3.Text = "الطلبيات الحالية"
        '
        'RibbonPageGroup4
        '
        Me.RibbonPageGroup4.ItemLinks.Add(Me.CarsTableButton)
        Me.RibbonPageGroup4.ItemLinks.Add(Me.ProLineTableButton)
        Me.RibbonPageGroup4.Name = "RibbonPageGroup4"
        Me.RibbonPageGroup4.Text = "الجداول"
        '
        'RibbonPageGroup5
        '
        Me.RibbonPageGroup5.ItemLinks.Add(Me.DoneOrdersButton)
        Me.RibbonPageGroup5.Name = "RibbonPageGroup5"
        Me.RibbonPageGroup5.Text = "الطلبيات المنصرفة"
        '
        'RibbonPageGroup11
        '
        Me.RibbonPageGroup11.ItemLinks.Add(Me.BarButtonItem2)
        Me.RibbonPageGroup11.Name = "RibbonPageGroup11"
        Me.RibbonPageGroup11.Text = "الفواتير"
        '
        'SettingPage
        '
        Me.SettingPage.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup1, Me.RibbonPageGroup2, Me.RibbonPageGroup12})
        Me.SettingPage.Name = "SettingPage"
        Me.SettingPage.Text = "الاعدادات"
        '
        'RibbonPageGroup1
        '
        Me.RibbonPageGroup1.ItemLinks.Add(Me.PrintSettingButton)
        Me.RibbonPageGroup1.Name = "RibbonPageGroup1"
        Me.RibbonPageGroup1.Text = "الطباعة"
        '
        'RibbonPageGroup2
        '
        Me.RibbonPageGroup2.ItemLinks.Add(Me.UsersMangemntButton)
        Me.RibbonPageGroup2.Name = "RibbonPageGroup2"
        Me.RibbonPageGroup2.Text = "المستخدمين"
        '
        'RibbonPageGroup12
        '
        Me.RibbonPageGroup12.ItemLinks.Add(Me.BarButtonItem3)
        Me.RibbonPageGroup12.Name = "RibbonPageGroup12"
        Me.RibbonPageGroup12.Text = "النسخ الاحتياطي"
        '
        'ReportsPage
        '
        Me.ReportsPage.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup6})
        Me.ReportsPage.Name = "ReportsPage"
        Me.ReportsPage.Text = "التقارير"
        '
        'RibbonPageGroup6
        '
        Me.RibbonPageGroup6.ItemLinks.Add(Me.ClintTakeButton)
        Me.RibbonPageGroup6.Name = "RibbonPageGroup6"
        Me.RibbonPageGroup6.Text = "العملاء"
        '
        'UsersPage
        '
        Me.UsersPage.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup7})
        Me.UsersPage.Name = "UsersPage"
        Me.UsersPage.Text = "المستخدمين"
        '
        'RibbonPageGroup7
        '
        Me.RibbonPageGroup7.ItemLinks.Add(Me.UserHistoryButton)
        Me.RibbonPageGroup7.Name = "RibbonPageGroup7"
        Me.RibbonPageGroup7.Text = "سجل المستخدمين"
        '
        'DataPage
        '
        Me.DataPage.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup8, Me.RibbonPageGroup9, Me.RibbonPageGroup10})
        Me.DataPage.Name = "DataPage"
        Me.DataPage.Text = "البيانات "
        '
        'RibbonPageGroup8
        '
        Me.RibbonPageGroup8.ItemLinks.Add(Me.CarsButton)
        Me.RibbonPageGroup8.ItemLinks.Add(Me.DriversButton)
        Me.RibbonPageGroup8.Name = "RibbonPageGroup8"
        Me.RibbonPageGroup8.Text = "النقل"
        '
        'RibbonPageGroup9
        '
        Me.RibbonPageGroup9.ItemLinks.Add(Me.ProductionLineButton)
        Me.RibbonPageGroup9.ItemLinks.Add(Me.ProductsButton)
        Me.RibbonPageGroup9.Name = "RibbonPageGroup9"
        Me.RibbonPageGroup9.Text = "الانتاج"
        '
        'RibbonPageGroup10
        '
        Me.RibbonPageGroup10.ItemLinks.Add(Me.OrdersGroupButton)
        Me.RibbonPageGroup10.Name = "RibbonPageGroup10"
        Me.RibbonPageGroup10.Text = "الطلبيات"
        '
        'XtraTabbedMdiManager1
        '
        Me.XtraTabbedMdiManager1.Appearance.BackColor = System.Drawing.Color.White
        Me.XtraTabbedMdiManager1.Appearance.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.XtraTabbedMdiManager1.Appearance.Options.UseBackColor = True
        Me.XtraTabbedMdiManager1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D
        Me.XtraTabbedMdiManager1.BorderStylePage = DevExpress.XtraEditors.Controls.BorderStyles.Style3D
        Me.XtraTabbedMdiManager1.ClosePageButtonShowMode = DevExpress.XtraTab.ClosePageButtonShowMode.InActiveTabPageHeader
        Me.XtraTabbedMdiManager1.FloatOnDoubleClick = DevExpress.Utils.DefaultBoolean.[True]
        Me.XtraTabbedMdiManager1.MdiParent = Me
        '
        'BackUpTimer
        '
        Me.BackUpTimer.Interval = 10000
        '
        'HomeScreen
        '
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(871, 568)
        Me.Controls.Add(Me.RibbonControl1)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Button1)
        Me.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.Name = "HomeScreen"
        Me.Opacity = 0R
        Me.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Text = "الشاشة الرئيسية"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.RibbonControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XtraTabbedMdiManager1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button1 As Button
    Friend WithEvents Timer1 As Timer

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents RibbonControl1 As DevExpress.XtraBars.Ribbon.RibbonControl
    Friend WithEvents PrintSettingButton As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents UsersMangemntButton As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents OrdersButton As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents NewOrdersButton As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents CarsTableButton As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ProLineTableButton As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents DoneOrdersButton As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ClintTakeButton As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents UserHistoryButton As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents CarsButton As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents DriversButton As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ProductionLineButton As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ProductsButton As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents OrdersGroupButton As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents SettingPage As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPageGroup1 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents RibbonPageGroup2 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents OrdersPage As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPageGroup3 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents RibbonPageGroup4 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents RibbonPageGroup5 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents ReportsPage As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPageGroup6 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents UsersPage As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPageGroup7 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents DataPage As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPageGroup8 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents RibbonPageGroup9 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents RibbonPageGroup10 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents XtraTabbedMdiManager1 As DevExpress.XtraTabbedMdi.XtraTabbedMdiManager
    Friend WithEvents BarButtonItem1 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem2 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup11 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents BackUpTimer As Timer
    Friend WithEvents BarButtonItem3 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup12 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
End Class
